import React from 'react';
import PropTypes from 'prop-types';
import {reduxForm, Field} from 'redux-form';

const SetAttributesFormContent = props => {
	const {
		pristine,
		handleSubmit,
		invalid,
		submitting,
		attributesAll,
		fieldName
	} = props;
	
	return (
		<form onSubmit={handleSubmit}>
			<div className="form-group">
		    <label htmlFor="attributeSelect">
		    	Select attributes
	    	</label>
		    <Field
		    	className="form-control"
		    	name={fieldName}
		    	id="attributeSelect"
		    	component="select"
		    	multiple={(fieldName === 'tags')}
		    	type={(fieldName === 'tags') ?
		    		'select-multiple' : 'select'}
	    	>
	    		{attributesAll.map(attr => (<option value={attr._id} key={attr._id}>
	    			{attr.title}
	    		</option>))}
		    </Field>
		  </div>
		  <button
		  	type="submit"
		  	className="btn btn-primary"
		  	disabled={submitting || pristine || invalid}
	  	>
		  	Submit
	  	</button>
		</form>
	);
};

SetAttributesFormContent.propTypes = {
	attributesAll: PropTypes.array.isRequired,
	fieldName: PropTypes.string.isRequired
};

export default reduxForm({
  form: 'setAttributesForm',
  enableReinitialize: true,
  onSubmitSuccess: (result, dispatch, props) => {
    props.reset();
  }
})(SetAttributesFormContent);