import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

class Pagination extends React.Component {

	getComponentId() {
		const {componentId} = this.props;
		return (typeof componentId !== 'undefined')
			? `${componentId}/` : '';
	};

	getPagesButtons() {
		const {pages, route} = this.props;
		const id = this.getComponentId();
		return pages.map(page => (
			<li
				className={`page-item shadow${(page.active) ? ' active' : ''}`}
				key={page.number}
				aria-current={(page.active) ? 'page' : ''}
			>
	    	<Link className="page-link" to={`${route}${id}${page.number}`}>{page.number}</Link>
	    	{(page.active) && (<span className="sr-only">(current)</span>)}
	    </li>
    ));
	};

	getNextPage() {
		const {currentPage, pages} = this.props;
		return (currentPage < pages.length) ? (currentPage+1) : (currentPage);
	};

	getPreviousPage() {
		const {currentPage} = this.props;
		return (currentPage-1 > 0) ? (currentPage-1) : (currentPage);
	};

	render() {
		const id = this.getComponentId();
		const {route} = this.props;
		return (
			<nav aria-label="...">
			  <ul className="pagination justify-content-center">
			    <li className="page-item shadow">
			      <Link
			      	className="page-link"
			      	aria-label="Previous"
			      	to={`${route}${id}${this.getPreviousPage()}`}
		      	>
			      	<span aria-hidden="true">&laquo;</span>
			      </Link>
			    </li>
			    {this.getPagesButtons()}
			    <li className="page-item shadow">
			      <Link
			      	className="page-link"
			      	aria-label="Next"
			      	to={`${route}${id}${this.getNextPage()}`}
		      	>
			      	<span aria-hidden="true">&raquo;</span>
			      </Link>
			    </li>
			  </ul>
			</nav>
		);
	};
};

Pagination.propTypes = {
	pages: PropTypes.array.isRequired,
	currentPage: PropTypes.number.isRequired,
	route: PropTypes.string.isRequired,
	componentId: PropTypes.string
};

export default Pagination;