import axios from 'axios';
import {
	FETCH_LATEST_ARTICLES,
	FETCH_LATEST_AUTHORS,
	FETCH_CATEGORIES,
	FETCH_TAGS
} from './Types';

export const fetchLatestArticles = max => async dispatch => {
	try {
		const res = await axios.get(`/api/articles/latest/${max}`);

		dispatch({
			type: FETCH_LATEST_ARTICLES,
			payload: res.data
		});
	} catch (err) {
		console.log(err.message);
	}
};

export const fetchLatestAuthors = max => async dispatch => {
	try {
		const res = await axios.get(`/api/users/latest/${max}`);

		dispatch({
			type: FETCH_LATEST_AUTHORS,
			payload: res.data
		});
	} catch (err) {
		console.log(err.message);
	}
};

export const fetchCategories = () => async dispatch => {
	try {
		const res = await axios.get('/api/categories/all');

		dispatch({
			type: FETCH_CATEGORIES,
			payload: res.data
		});
	} catch (err) {
		console.log(err.message);
	}
};

export const fetchTags = () => async dispatch => {
	try {
		const res = await axios.get('/api/tags/all');

		dispatch({
			type: FETCH_TAGS,
			payload: res.data
		});
	} catch (err) {
		console.log(err.message);
	}
};