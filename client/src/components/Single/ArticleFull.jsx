import React from 'react';
import PropTypes from 'prop-types';

class ArticleFull extends React.Component {
	render() {
		let {
			image,
			title,
			content,
			last_update,
			viewed,
			user
		} = this.props.article;

		const defaultImage = `${window.location.origin}/no_image.jpg`;
		image = image ? image : defaultImage;
		//image = image ? `${window.location.origin}/uploads/${image}` : defaultImage;

		return (
			<div className="card mb-3 shadow">
				<a href="#1" className="p-1">
			  	<img src={image} className="card-img-top" alt="" />
				</a>
			  <div className="card-body">
			    <h5 className="card-title">{title}</h5>
			    <p className="card-text">
						{content}
			    </p>
			  </div>
			  <div className="card-footer text-muted d-flex justify-content-between align-items-center">
			  	<small>
			  		<a href="#1" className="card-link mr-2">
			  			By {user && user.name}
			  		</a>
			  		<span>{last_update}</span>
			  	</small>
			    <span>
			    	<i className="fa fa-eye" aria-hidden="true"></i>
			    	{' '}
			    	<span>{viewed}</span>
			  	</span>
			  </div>
			</div>
		);
	};
};

ArticleFull.propTypes = {
	article: PropTypes.object.isRequired
};

export default ArticleFull;