import {
	UPLOAD_FILE,
	SET_FILE,
	CLEAR_UPLOAD_DATA
} from '../actions/Types';

const initialState = {
	file: {},
	receivedData: {}
};

export const fileUploadReducer = function(state = initialState, action) {
	const {type, payload} = action;

	switch (type) {
		case SET_FILE:
			return {
				...state,
				file: payload
			};
		case UPLOAD_FILE:
			return {
				...state,
				receivedData: payload
			};
		case CLEAR_UPLOAD_DATA:
			return {
				...state,
				file: payload,
				receivedData: payload
			};
		default: return state;
	}
};