import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import Latest from './Latest';
import Categories from './Categories';
import Authors from './Authors';
import Tags from './Tags';
import {
	fetchLatestArticles,
	fetchLatestAuthors,
	fetchCategories,
	fetchTags
} from '../../actions/SidebarActions';

class Sidebar extends React.Component {

	constructor(props) {
		super(props);
		this.max = 4;
	};

	componentDidMount() {
		this.props.fetchLatestArticles(this.max);
		this.props.fetchLatestAuthors(this.max);
		this.props.fetchCategories();
		this.props.fetchTags();
	};

	render() {
		const {
			latestArticles,
			categories,
			authors,
			tags
		} = this.props;

		return (
			<div className="col-md-5 col-lg-4 sidebar">
				<Latest latest={latestArticles}/>
				<Categories categories={categories}/>
				<Authors authors={authors}/>
				<Tags tags={tags}/>
			</div>
		);
	};
};

const mapStateToProps = store => {
  const {latestArticles, categories, authors, tags} = store.sidebar;
  return {latestArticles, categories, authors, tags};
};

const mapDispatchToProps = dispatch => ({
  fetchLatestArticles: max => dispatch(fetchLatestArticles(max)),
  fetchLatestAuthors: max => dispatch(fetchLatestAuthors(max)),
  fetchCategories: () => dispatch(fetchCategories()),
  fetchTags: () => dispatch(fetchTags())
});

Sidebar.propTypes = {
	latestArticles: PropTypes.array.isRequired,
	categories: PropTypes.array.isRequired,
	authors: PropTypes.array.isRequired,
	tags: PropTypes.array.isRequired,
	fetchLatestArticles: PropTypes.func.isRequired,
	fetchLatestAuthors: PropTypes.func.isRequired,
	fetchCategories: PropTypes.func.isRequired,
	fetchTags: PropTypes.func.isRequired
};

export default connect(
	mapStateToProps,
  mapDispatchToProps
)(Sidebar);