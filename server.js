require('express-async-errors');
const debugStartup = require('debug')('app:startup');
const config = require('config');
const express = require('express');
const app = express();
const path = require('path');
require('./startup/routes')(app);
require('./startup/db')();

process.on('uncaughtException', ex => {
	console.log('WE GOT AN UNCAUGHT EXCEPTION');
	process.exit(1);
});
process.on('unhandledRejection', ex => {
	console.log('WE GOT AN UNHANDLED REJECTION');
	process.exit(1);
});

if (!config.get('jwtPrivateKey') ||
	!config.get('cloudinary_cloud_name') ||
	!config.get('cloudinary_api_key') ||
	!config.get('cloudinary_api_secret') ||
	!config.get('db_password')) {
		console.error('FATAL ERROR: some of environment valiables is not defined!');
		process.exit(1);
}

if (process.env.NODE_ENV === 'production') {
	app.use(express.static('client/build'));

	app.get('*', (req, res) => {
		res.sendFile(path.resolve(
			__dirname, 'client', 'build', 'index.html'));
	});
}

const port = process.env.PORT || 5000;

const server = app.listen(port, () =>
	debugStartup(`Listening on port ${port}...`));

module.exports = server;