const pagination = require('../helpers/pagination');
const admin = require('../middleware/admin');
const auth = require('../middleware/auth');
const validateInput = require('../middleware/validateInput');
const validateObjectId = require('../middleware/validateObjectId');
const {Category, validateCategory} = require('../models/Category');
const {Article} = require('../models/Article');
const express = require('express');
const router = express.Router();

router.get('/all', async (req, res) => {
	const categories = await Category.find();

	res.send(categories);
});

router.get('/page/:page/size/:pageSize', async (req, res) => {
	let {page, pageSize} = req.params;

	const paginate = await pagination(
		Category, page, pageSize);

	const {beginIndex, pages} = paginate;
	pageSize = paginate.pageSize;

	const categories = await Category
		.find()
		.skip(beginIndex)
		.limit(pageSize)
		.sort({title: 1});

	res.send({categories, pages});
});

router.get('/:id', validateObjectId, async (req, res) => {
	const category = await Category.findById(req.params.id);
	if (!category)
		return res.status(404).send('Cannot find the category with given ID');

	res.send(category);
});

router.post('/', [auth, admin, validateInput(validateCategory)],
	async (req, res) => {
		const {title} = req.body;
		const category = new Category({title});
		await category.save();

		res.send(category);
});

router.put('/:id', [auth, admin, validateObjectId,
	validateInput(validateCategory)], async (req, res) => {
		const {title} = req.body;
		const category = await Category.findByIdAndUpdate(
			req.params.id, {title}, {new: true});
		if (!category)
			return res.status(404).send('Cannot find category with given ID');

		await Article.updateMany(
			{'category._id': category._id},
			{'category.title': title}
		);

		res.send(category);
});

router.delete('/:id', [auth, admin, validateObjectId],
	async (req, res) => {
		const category = await Category.findByIdAndRemove(req.params.id);
		if (!category)
			return res.status(404).send('Cannot find category with given ID');

		await Article.updateMany(
			{'category._id': category._id},
			{category: null}
		);

		res.send(category);
});

module.exports = router;