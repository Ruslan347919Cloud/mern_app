import {
	CREATE_ITEM,
	FIND_ITEM,
	UPDATE_ITEM,
	CLEAR_FORM
} from '../../../actions/Types';

const initialState = {
	currentTagId: '',
	currentTagTitle: '',
	newTagData: {}
};

export const tagFormReducer = function(state = initialState, action) {
	const {type, payload} = action;

	switch (type) {
		case FIND_ITEM:
			return {
				...state,
				currentTagId: payload._id,
				currentTagTitle: payload.title
			};
		case CREATE_ITEM:
			return {
				...state,
				newTagData: payload
			};
		case UPDATE_ITEM:
			return {
				...state,
				newTagData: payload
			};
		case CLEAR_FORM:
			return {
				...state,
				currentTagId: payload.defaultValue,
				currentTagTitle: payload.defaultValue,
				newTagData: payload.defaultItemData
			};
		default: return state;
	}
};