const config = require('config');
const multer = require('multer');
const {v4: uuidv4} = require('uuid');
const path = require('path');
const cloudinary = require('cloudinary').v2;

const cloud_name = config.get('cloudinary_cloud_name');
const api_key = config.get('cloudinary_api_key');
const api_secret = config.get('cloudinary_api_secret');
const uploads = config.get('uploads');

const uploadsAbsolutePath = `${path.dirname(
  require.main.filename)}${uploads}`;

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, uploadsAbsolutePath);
  },
  filename: (req, file, cb) => {
    const fileName = file
      .originalname
      .toLowerCase()
      .split(' ')
      .join('-');
    cb(null, `${uuidv4()}-${fileName}`);
  }
});

const fileFilter = (req, file, cb) => {
  if (file.mimetype === 'image/gif'
    || file.mimetype === 'image/png'
    || file.mimetype === 'image/jpg'
    || file.mimetype === 'image/jpeg') {
      cb(null, true);
  } else {
    cb(null, false);
    return cb(new Error('Only .png, .jpg, .gif and .jpeg format allowed!'));
  }
};

const upload = multer({
  storage,
  limits: {fileSize: 1280 * 1280},
  fileFilter
});

cloudinary.config({
  cloud_name,
  api_key,
  api_secret
});

exports.deleteImageFromCloudinary = async imageLink => {
  const parsedLink = imageLink.split('/');
  const oldImageName = parsedLink[parsedLink.length-1];
  const public_id = oldImageName.split('.')[0];
  await cloudinary.uploader.destroy(public_id);
};

exports.cloudinary = cloudinary;
exports.imageUpload = upload.single('image');
exports.uploadsAbsolutePath = uploadsAbsolutePath;