import React, {Fragment} from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import RelatedArticles from '../../partial/view/RelatedArticles';

class SingleTag extends React.Component {

	deleteTag = event => {
		event.preventDefault();

		const {_id} = this.props.item;

		this.props.deleteItem(_id, 'tags');
	}

	render() {
		const {item, junctionItems} = this.props;

		return (
			<Fragment>
				<h2>{item.title}</h2>
				<div className="list-group mb-3 shadow">
					<div className="list-group-item d-flex justify-content-between border">
				    <span>Id</span>
				    <span>{item._id}</span>
				  </div>
					<div className="list-group-item d-flex justify-content-between border">
				    <span>Title</span>
				    <span>{item.title}</span>
				  </div>
				</div>
				<Link className="btn btn-primary btn mr-2" to={`/admin/tag/form/${item._id}`}>
					Update tag info
				</Link>
				<button className="btn btn-danger btn mr-2" type="button" onClick={this.deleteTag}>
					Delete tag
				</button>
				<RelatedArticles articles={junctionItems} />
			</Fragment>
		);
	}
}

SingleTag.propTypes = {
	item: PropTypes.object.isRequired,
	junctionItems: PropTypes.array.isRequired,
	deleteItem: PropTypes.func.isRequired
};

export default SingleTag;