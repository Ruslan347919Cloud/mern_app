const pagination = require('../helpers/pagination');
const auth = require('../middleware/auth');
const validateObjectId = require('../middleware/validateObjectId');
const validateInput = require('../middleware/validateInput');
const {Article} = require('../models/Article');
const {Comment, validateComment, validateCommentUpdate}
	= require('../models/Comment');
const express = require('express');
const router = express.Router();

router.get('/page/:page/size/:pageSize', async (req, res) => {
	let {page, pageSize} = req.params;

	const paginate = await pagination(
		Comment, page, pageSize);

	const {beginIndex, pages} = paginate;
	pageSize = paginate.pageSize;

	const comments = await Comment
		.find()
		.skip(beginIndex)
		.limit(pageSize)
		.sort({_id: -1});

	res.send({comments, pages});
});

router.get('/article/:id/page/:page/size/:pageSize',
	validateObjectId, async (req, res) => {
		const articleId = req.params.id;

		const article = await Article.findById(articleId);
		if (!article)
			return res.status(404).send('Cannot find article with given ID');

		let {page, pageSize} = req.params;
		const searchParam = {article: articleId};
		
		const paginate = await pagination(Comment,
			page, pageSize, searchParam);

		const {beginIndex, pages} = paginate;
		pageSize = paginate.pageSize;

		const comments = await Comment
			.find(searchParam)
			.populate('user', '_id name photo')
			.skip(beginIndex)
			.limit(pageSize)
			.sort({_id: -1});

		res.send({comments, pages});
});

router.get('/:id', validateObjectId, async (req, res) => {
	const comment = await Comment
		.findById(req.params.id)
		.populate('user article', '_id name title photo');
		
	if (!comment)
		return res.status(404).send('Cannot find the comment with given id');

	res.send(comment);
});

router.get('/search/:keyword', async (req, res) => {
	const keyword = new RegExp('.*'+req.params.keyword+'.*', 'i');
	const limit = 10;

	const comments = await Comment
		.find({text: keyword})
		.populate('user article', 'title name')
		.limit(limit);

	res.send(comments);
});

router.post('/article/:id', [auth, validateObjectId,
	validateInput(validateComment)], async (req, res) => {
		const {text} = req.body;
		const article = await Article.findById(req.params.id);
		if (!article)
			return res.status(404).send('Cannot find the article with given ID');

		const comment = new Comment({
			text,
			user: req.user._id,
			article: article._id
		});

		article.comments.unshift(comment);
		await comment.save();
		await article.save();

		res.send(comment);
});

router.put('/:id', [auth, validateObjectId,
	validateInput(validateCommentUpdate)], async (req, res) => {
		let comment = await Comment.findById(req.params.id);
		if (!comment)
			return res.status(404).send('Cannot find the comment with given ID');
		if (!comment.checkAuthorization(req))
			return res.status(401).send('User is not authorized');

		const {text, visible} = req.body;
		const commentFields = {};

		if (visible) {
			if (!req.user.isAdmin)
				return res.status(403).send('Operation is forbidden.');
			else commentFields.visible = !comment.visible || false;
		}

		if (text)
			commentFields.text = text;

		comment.set(commentFields);

		const article = await Article.findById(comment.article);

		const updateIndex = article.comments
			.findIndex(item => String(item._id) === String(comment._id));
		article.comments[updateIndex].set(commentFields);

		await comment.save();
		await article.save();

		res.send(comment);
});

router.delete('/:id', [auth, validateObjectId], async (req, res) => {
	const comment = await Comment.findById(req.params.id);
	if (!comment)
		return res.status(404).send('Cannot find the comment with given ID');
	if (!comment.checkAuthorization(req))
		return res.status(401).send('User is not authorized');

	const article = await Article.findById(comment.article);

	const removeIndex = article.comments
		.findIndex(item => String(item._id) === String(comment._id));
	article.comments.splice(removeIndex, 1);

	await comment.delete();
	await article.save();

	res.send(comment);
});

module.exports = router;