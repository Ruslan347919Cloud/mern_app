import {
	CREATE_ITEM,
	FIND_ITEM,
	UPDATE_ITEM,
	CLEAR_FORM
} from '../../../actions/Types';

const initialState = {
	currentArticleId: '',
	currentArticleTitle: '',
	currentArticleDescription: '',
	currentArticleContent: '',
	newArticleData: {}
};

export const articleFormReducer = function(state = initialState, action) {
	switch (action.type) {
		case FIND_ITEM:
			return {
				...state,
				currentArticleId: action.payload._id,
				currentArticleTitle: action.payload.title,
				currentArticleDescription: action.payload.description,
				currentArticleContent: action.payload.content
			};
		case CREATE_ITEM:
			return {
				...state,
				newArticleData: action.payload
			};
		case UPDATE_ITEM:
			return {
				...state,
				newArticleData: action.payload
			};
		case CLEAR_FORM:
			return {
				...state,
				currentArticleId: action.payload.defaultValue,
				currentArticleTitle: action.payload.defaultValue,
				currentArticleDescription: action.payload.defaultValue,
				currentArticleContent: action.payload.defaultValue,
				newArticleData: action.payload.defaultItemData
			};
		default: return state;
	}
};