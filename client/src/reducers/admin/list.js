import {
	FETCH_ITEMS,
	DELETE_ITEM,
	CHANGE_ITEM_STATUS
} from '../../actions/Types';

const initialState = {
	items: [],
	pages: [],
	deletedItemId: '',
	changedItem: {}
};

export const listReducer = function(state = initialState, action) {
	const {type, payload} = action;

	switch (type) {
		case FETCH_ITEMS:
			return {
				...state,
				items: payload.items,
				pages: payload.pages,
				deletedItemId: ''
			};
		case DELETE_ITEM:
			return {
				...state,
				deletedItemId: payload
			};
		case CHANGE_ITEM_STATUS:
			return {
				...state,
				changedItem: payload
			};
		default: return state;
	}
};