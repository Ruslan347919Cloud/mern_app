import React, {Fragment} from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import RelatedArticles from '../../partial/view/RelatedArticles';

class SingleUser extends React.Component {

	deleteUser = event => {
		event.preventDefault();
		const {_id} = this.props.user;
		this.props.deleteUser(_id);
	};

	render() {
		const {articles} = this.props;
		let {
			_id,
			photo,
			name,
			email
		} = this.props.user;
		const defaultImage = `${window.location.origin}/no_avatar.png`;
		//photo = photo ? `${window.location.origin}/uploads/${photo}` : defaultImage;
		photo = photo ? photo : defaultImage;

		return (
			<Fragment>
				<h2>{name}</h2>
				<div className="list-group mb-3 shadow">
					<div className="list-group-item d-flex justify-content-between border">
				    <span>Photo</span>
				    <img src={photo} alt="" className="rounded" />
				  </div>
					<div className="list-group-item d-flex justify-content-between border">
				    <span>Id</span>
				    <span>{_id}</span>
				  </div>
				  <div className="list-group-item d-flex justify-content-between border">
				    <span>Name</span>
				    <span>{name}</span>
				  </div>
				  <div className="list-group-item d-flex justify-content-between border">
				    <span>Email</span>
				    <span>{email}</span>
				  </div>
				</div>
				<Link className="btn btn-primary btn mr-2 mb-1" to={'/admin/article/form'}>
					Create article
				</Link>
				<Link className="btn btn-primary btn mr-2 mb-1" to={`/admin/user/form/${_id}`}>
					Update user info
				</Link>
				<Link className="btn btn-primary btn mr-2" to={`/upload/users/user/${_id}`}>
					Upload user photo
				</Link>
				<button className="btn btn-danger btn mr-2" type="button" onClick={this.deleteUser}>
					Delete user
				</button>
				<RelatedArticles articles={articles} />
			</Fragment>
		);
	};
};

SingleUser.propTypes = {
	user: PropTypes.object.isRequired,
	articles: PropTypes.array.isRequired,
	deleteUser: PropTypes.func.isRequired
};

export default SingleUser;