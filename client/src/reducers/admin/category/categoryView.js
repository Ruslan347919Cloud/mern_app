import {
	FETCH_SINGLE_CATEGORY,
	FETCH_ARTICLES_BY_CATEGORY
} from '../../../actions/Types';

const initialState = {
	category: {},
	articles: [],
	pages: []
};

export const categoryViewReducer = function(state = initialState, action) {
	const {type, payload} = action;

	switch (type) {
		case FETCH_SINGLE_CATEGORY:
			return {
				...state,
				category: payload
			};
		case FETCH_ARTICLES_BY_CATEGORY:
			return {
				...state,
				...payload
			};
		default: return state;
	}
};