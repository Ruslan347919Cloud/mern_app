module.exports = async (Model, page, pageSize,
	searchParam = {}) => {
		page = Number(page);
		pageSize = Number(pageSize);

		const documentsTotal = await Model
			.countDocuments(searchParam);
			
		const pagesTotal = Math
			.ceil(documentsTotal/pageSize);
		
		if (page > pagesTotal) page = pagesTotal;
		if (page < 1) page = 1;

		const beginIndex = (page - 1) * pageSize;

		const pages = [];
		for (let i = 0; i < pagesTotal; i++) {
			pages.push({
				number: i+1,
				active: (page === i+1)
			});
		}

		return {pageSize, beginIndex, pages};
};