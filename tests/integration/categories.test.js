const {User} = require('../../models/User');
const {Article} = require('../../models/Article');
const {Category} = require('../../models/Category');
const request = require('supertest');
const mongoose = require('mongoose');

let server;

describe('/api/categories', () => {
	beforeEach(() => {server = require('../../server');});
	afterEach(async () => {
		await Category.deleteMany({});
		await server.close();
	});

	describe('GET /all', () => {
		const exec = async () => {
			return await request(server)
				.get('/api/categories/all');
		};

		beforeEach(async () => {
			await Category.collection.insertMany([
				{title: 'category1'},
				{title: 'category2'},
				{title: 'category3'}
			]);
		});

		it('should return all categories', async () => {
			const res = await exec();

			expect(res.status).toBe(200);
			expect(res.body.length).toBe(3);
			expect(res.body.some(category => category.title === 'category1'))
				.toBeTruthy();
			expect(res.body.some(category => category.title === 'category2'))
				.toBeTruthy();
			expect(res.body.some(category => category.title === 'category3'))
				.toBeTruthy();
		});

	});

	describe('GET /page/:page/size/:pageSize', () => {
		let page, pageSize;

		const exec = async () => {
			return await request(server)
				.get(`/api/categories/page/${page}/size/${pageSize}`);
		};

		beforeEach(async () => {
			await Category.collection.insertMany([
				{title: 'category1'},
				{title: 'category2'},
				{title: 'category3'},
				{title: 'category4'},
				{title: 'category5'},
				{title: 'category6'},
				{title: 'category7'},
				{title: 'category8'}
			]);
			page = 2;
			pageSize = 3;
		});

		it('should return all categories for current page', async () => {
			const res = await exec();

			const {categories} = res.body;

			expect(res.status).toBe(200);
			expect(categories.length).toBe(3);
			expect(categories.some(category => category.title === 'category4'))
				.toBeTruthy();
			expect(categories.some(category => category.title === 'category5'))
				.toBeTruthy();
			expect(categories.some(category => category.title === 'category6'))
				.toBeTruthy();
		});

		it('should return first page if given page is less than 1', async () => {
			page = 0;

			const res = await exec();

			const {categories} = res.body;

			expect(res.status).toBe(200);
			expect(categories.length).toBe(3);
			expect(categories.some(category => category.title === 'category1'))
				.toBeTruthy();
			expect(categories.some(category => category.title === 'category2'))
				.toBeTruthy();
			expect(categories.some(category => category.title === 'category3'))
				.toBeTruthy();
		});

		it('should return last page if given page is more then max page', async () => {
			page = 4;

			const res = await exec();

			const {categories} = res.body;

			expect(res.status).toBe(200);
			expect(categories.length).toBe(2);
			expect(categories.some(category => category.title === 'category7'))
				.toBeTruthy();
			expect(categories.some(category => category.title === 'category8'))
				.toBeTruthy();
		});

		it('should return information about pages', async () => {
			const res = await exec();

			const {pages} = res.body;

			expect(res.status).toBe(200);
			expect(pages.length).toBe(3);
			expect(pages.some(page => {
				page.number === 1 && page.active === false
			}));
			expect(pages.some(page => {
				page.number === 2 && page.active === true
			}));
			expect(pages.some(page => {
				page.number === 3 && page.active === false
			}));
		});
	});

	describe('GET /:id', () => {
		let categoryId;
		const exec = async () => await request(server)
			.get(`/api/categories/${categoryId}`);

		it('should return a single category if valid ID is passed', async () => {
			const category = new Category({title: 'aaaaa'});
			await category.save();

			categoryId = category._id;

			const res = await exec();

			expect(res.status).toBe(200);
			expect(res.body).toHaveProperty('title', category.title);
		});

		it('should return 404 if invalid ID is passed', async () => {
			categoryId = '1';

			const res = await exec();

			expect(res.status).toBe(404);
		});

		it('should return 404 if no category with the given ID exists', async () => {
			categoryId = mongoose.Types.ObjectId();

			const res = await exec();

			expect(res.status).toBe(404);
		});
	});

	describe('POST /', () => {
		let token;
		let title;

		const exec = async () => {
			return await request(server)
				.post('/api/categories')
				.set('x-auth-token', token)
				.send({title});
		}

		beforeEach(async () => {
			title = '12345';
			token = new User({isAdmin: true}).generateAuthToken();
		});

		it('should return 401 if client is not logged in', async () => {
			token = '';
			
			const res = await exec();

			expect(res.status).toBe(401);
		});

		it('should return 403 if user is not admin', async () => {
			token = new User({isAdmin: false}).generateAuthToken();
			
			const res = await exec();

			expect(res.status).toBe(403);
		});

		it('should return 400 if title is less than 5 characters', async () => {
			title = '1234';

			const res = await exec();

			expect(res.status).toBe(400);
		});

		it('should return 400 if title is more than 50 characters', async () => {
			title = new Array(52).join('a');

			const res = await exec();

			expect(res.status).toBe(400);
		});

		it('should save the category if it is valid', async () => {
			const res = await exec();

			const category = await Category.findById(res.body._id);
			
			expect(category).not.toBeNull();
		});

		it('should return the category if it is valid', async () => {
			const res = await exec();

			expect(res.body).toHaveProperty('_id');
			expect(res.body).toHaveProperty('title', '12345');
		});
	});

	describe('PUT /:id', () => {
		let token, title, category, categoryId,
			article1, article2;

		const exec = async () => {
			return await request(server)
				.put(`/api/categories/${categoryId}`)
				.set('x-auth-token', token)
				.send({title});
		};

		beforeEach(async () => {
			token = new User({isAdmin: true}).generateAuthToken();
			category = new Category({title: '12345'});
			await category.save();
			categoryId = category._id;
			article1 = new Article({
				_id: mongoose.Types.ObjectId(),
				title: '12345',
				content: '12345',
				user: mongoose.Types.ObjectId(),
				tags: [],
				category: {
					_id: category._id,
					title: category.title
				}
			});
			article2 = new Article({
				_id: mongoose.Types.ObjectId(),
				title: '54321',
				content: '54321',
				user: mongoose.Types.ObjectId(),
				tags: [],
				category: {
					_id: category._id,
					title: category.title
				}
			});
			await article1.save();
			await article2.save();
			title = '54321';
		});

		afterEach(async () => {
			await Article.deleteMany({});
		});

		it('should return 401 if client is not logged in', async () => {
			token = '';
			
			const res = await exec();

			expect(res.status).toBe(401);
		});

		it('should return 403 if user is not admin', async () => {
			token = new User({isAdmin: false}).generateAuthToken();
			
			const res = await exec();

			expect(res.status).toBe(403);
		});

		it('should return 404 if invalid id is passed', async () => {
			categoryId = 1;

			const res = await exec();

			expect(res.status).toBe(404);
		});

		it('should return 404 if no category with the given id exists', async () => {
			categoryId = mongoose.Types.ObjectId();

			const res = await exec();

			expect(res.status).toBe(404);
		});

		it('should return 400 if title is less than 5 characters', async () => {
			title = '1234';

			const res = await exec();

			expect(res.status).toBe(400);
		});

		it('should return 400 if title is more than 50 characters', async () => {
			title = new Array(52).join('a');

			const res = await exec();

			expect(res.status).toBe(400);
		});

		it('should update category in db and if id is valid', async () => {
			const res = await exec();

			category = await Category.findById(res.body._id);

			expect(res.status).toBe(200);
			expect(category.title).toBe('54321');
		});

		it('should update category title in related articles', async () => {
			const res = await exec();

			const articles = await Article
				.find({'category._id': categoryId});

			expect(articles.length).toBe(2);
			expect(articles[0].category.title).toBe('54321');
			expect(articles[1].category.title).toBe('54321');
		});

		it('should return the category if it is valid', async () => {
			const res = await exec();

			expect(res.body).toHaveProperty('_id', category._id.toHexString());
			expect(res.body).toHaveProperty('title', '54321');
		});
	});

	describe('DELETE /:id', () => {
		let token, category, categoryId;

		const exec = async () => {
			return await request(server)
				.delete(`/api/categories/${categoryId}`)
				.set('x-auth-token', token);
		};

		beforeEach(async () => {
			token = new User({isAdmin: true}).generateAuthToken();

			category = new Category({title: '12345'});
			await category.save();
			categoryId = category._id;

			await Article.collection.insertMany([
				{title: 'article1', content: '12345', category: {_id: categoryId}},
				{title: 'article2', content: '12345', category: {_id: categoryId}},
				{title: 'article3', content: '12345', category: {_id: categoryId}}
			]);
		});

		afterEach(async () => {
			await Article.deleteMany({});
		});

		it('should return 401 if client is not logged in', async () => {
			token = '';

			const res = await exec();

			expect(res.status).toBe(401); 
		});

		it('should return 403 if user is not admin', async () => {
			token = new User({isAdmin: false}).generateAuthToken();
			
			const res = await exec();

			expect(res.status).toBe(403);
		});

		it('should return 404 if invalid id is passed', async () => {
			categoryId = '1';

			const res = await exec();

			expect(res.status).toBe(404);
		});

		it('should return 404 if no category with the given id exists', async () => {
			categoryId = mongoose.Types.ObjectId();

			const res = await exec();

			expect(res.status).toBe(404);
		});

		it('should delete the category from db if id is valid', async () => {
			const res = await exec();

			category = await Category.findById(res.body._id);

			expect(res.status).toBe(200);
			expect(category).toBeNull();
		});

		it('should return the removed category', async () => {
      const res = await exec();

      expect(res.body).toHaveProperty('_id', category._id.toHexString());
      expect(res.body).toHaveProperty('title', category.title);
    });

    it('should remove deleted category from articles', async () => {
      const res = await exec();

      const articles = await Article.find({'category._id': categoryId});

    	expect(res.status).toBe(200);
      expect(articles.length).toBe(0);
    });
	});
});