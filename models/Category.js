const mongoose = require('mongoose');
const Joi = require('@hapi/joi');

const validateCategory = category => {
	const schema = Joi.object({
		title: Joi.string().min(5).max(50).required()
	});
	return schema.validate(category);
}

const categorySchema = new mongoose.Schema({
	title: {
		type: String,
		required: true,
		minlength: 5,
		maxlength: 50,
		trim: true
	},
	articles: [{
		type: mongoose.Schema.Types.ObjectId,
    ref: 'Article'
	}]
});

categorySchema.methods.hasArticle = function(articleId) {
	return this.articles.some(item =>
		String(item) === String(articleId));
};

categorySchema.methods.addArticle = async function(articleId) {
	if (!this.hasArticle(articleId)) {
		this.articles.push(articleId);
		await this.save();
	}
};

categorySchema.methods.removeArticle = async function(articleId) {
	const removeIndex = this.articles
		.findIndex(item => item === articleId);
	this.articles.splice(removeIndex, 1);
	await this.save();
};

const Category = mongoose.model('Category', categorySchema);

exports.Category = Category;
exports.categorySchema = categorySchema;
exports.validateCategory = validateCategory;