const mongoose = require('mongoose');

module.exports = (req, res, next) => {
	const idIsValid = mongoose.Types.ObjectId.isValid(req.params.id);
	if (!idIsValid)
		return res.status(404).send('Invalid ID.');
	
	next();
};