import React, {Fragment} from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

const RelatedArticles = props => {
	let {articles} = props;
	let articlesList = (typeof articles !== 'undefined')
		? (articles.map(article => (
			<Link className="list-group-item list-group-item-action d-flex justify-content-between border"
				to={`/admin/article/view/${article._id}`} key={article._id}
			>
		    <span>{article.title}</span>
		    <span>Last update: {article.last_update}</span>
		  </Link>
		))) : null;

	return (
		<Fragment>
			<h3 className="mt-4">
				{(articles.length > 0) ? 'Related articles' : 'No articles'}
			</h3>
			<div className="list-group mb-3 shadow">
				{articlesList}
			</div>
		</Fragment>
	);
};

RelatedArticles.propTypes = {
	articles: PropTypes.array.isRequired
};

export default RelatedArticles;