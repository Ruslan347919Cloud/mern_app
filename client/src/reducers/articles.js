import {FETCH_ARTICLES} from '../actions/Types';

const initialState = {
	articles: [],
	pages: []
};

export const articlesReducer = function(state = initialState, action) {
	switch (action.type) {
		case FETCH_ARTICLES:
			return {
				...state,
				...action.payload
			};
		default: return state;
	}
};