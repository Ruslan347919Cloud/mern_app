import React, {Fragment} from 'react';
import {NavLink} from 'react-router-dom';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {Redirect} from 'react-router-dom';
import {logout} from '../../actions/AuthActions';

class HeaderCms extends React.Component {
	render() {
		const {
			auth: {isAuthenticated, loading, user},
			logout
		} = this.props;

		const adminLinks = (
			<Fragment>
				<li className="nav-item">
	        <NavLink className="nav-link text-white" to="/admin/article/index">
	        	Article
	        </NavLink>
	      </li>
	      <li className="nav-item">
	        <NavLink className="nav-link text-white" to="/admin/category/index">
	        	Category
	        </NavLink>
	      </li>
	      <li className="nav-item">
	        <NavLink className="nav-link text-white" to="/admin/tag/index">
	        	Tag
	        </NavLink>
	      </li>
	      <li className="nav-item">
	        <NavLink className="nav-link text-white" to="/admin/comment/index">
	        	Comment
	        </NavLink>
	      </li>
	      <li className="nav-item">
	        <NavLink className="nav-link text-white" to="/admin/user/index">
	        	User
	        </NavLink>
	      </li>
			</Fragment>
		);

		if (!loading && !isAuthenticated)
			return <Redirect to={'/'}/>;
		else return (
			<header className="container-fluid mb-3 shadow">
				<nav className="navbar navbar-expand-sm border navbar-light bg-primary row">
				  <NavLink className="navbar-brand" to="/">
				  	My News Site
				  </NavLink>
				  <button
				  	className="navbar-toggler"
				  	type="button"
				  	data-toggle="collapse"
				  	data-target="#navbarMenu"
				  	aria-controls="navbarMenu"
				  	aria-expanded="false"
				  	aria-label="Toggle navigation"
			  	>
				    <i className="navbar-toggler-icon"></i>
				  </button>
				  <div className="collapse navbar-collapse" id="navbarMenu">
				    <ul className="navbar-nav mr-auto">
				      <li className="nav-item active">
				        <NavLink className="nav-link text-white" to="/">
				        	Home
				        </NavLink>
				      </li>
				      {!loading && user.isAdmin && adminLinks}
				      <li className="nav-item">
				        <NavLink
				        	className="nav-link text-white"
				        	to={`/admin/user/view/${user && user._id}`}
			        	>
				        	{user && user.name} profile
			        	</NavLink>
				      </li>
				      <li className="nav-item">
				        <NavLink
				        	className="nav-link text-white"
				        	to="#1"
				        	onClick={logout}
			        	>
			        		<i className="fas fa-sign-out-alt"></i>
			        		{' '}Logout
				        </NavLink>
				      </li>
				    </ul>
				  </div>
				</nav>
			</header>
		);
	};
};

HeaderCms.propTypes = {
	logout: PropTypes.func.isRequired,
	auth: PropTypes.object.isRequired
};

const mapStateToProps = store => ({
	auth: store.auth
});

export default connect(mapStateToProps, {logout})(HeaderCms);