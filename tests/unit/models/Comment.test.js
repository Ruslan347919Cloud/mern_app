const {Comment} = require('../../../models/Comment');
const mongoose = require('mongoose');

describe('comment.checkAuthorization', () => {
	let req;
	let comment;

	beforeEach(() => {
		req = {
			user: {
				_id: new mongoose.Types.ObjectId().toHexString(),
				isAdmin: false
			}
		};

		comment = new Comment({
			text: 'aaaaa',
			user: new mongoose.Types.ObjectId().toHexString(),
			article: new mongoose.Types.ObjectId().toHexString(),
			_id: new mongoose.Types.ObjectId().toHexString()
		});
	});

	it('should return false if user is not admin and not own this comment', () => {
		const result = comment.checkAuthorization(req);

		expect(result).toBeFalsy();
	});

	it('should return true if user is not admin but own this comment', () => {
		comment.user = req.user._id;

		const result = comment.checkAuthorization(req);

		expect(result).toBeTruthy();
	});

	it('should return true if user is admin but not own this comment', () => {
		req.user.isAdmin = true;

		const result = comment.checkAuthorization(req);

		expect(result).toBeTruthy();
	});

	it('should return true if user is admin and own this comment', () => {
		comment.user = req.user._id;
		req.user.isAdmin = true;

		const result = comment.checkAuthorization(req);

		expect(result).toBeTruthy();
	});
});