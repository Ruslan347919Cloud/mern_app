const bcrypt = require('bcrypt');
const {User} = require('../../models/User');
const {Article} = require('../../models/Article');
const {Comment} = require('../../models/Comment');
const {Category} = require('../../models/Category');
const {Tag} = require('../../models/Tag');
const request = require('supertest');
const mongoose = require('mongoose');

let server;

describe('/api/users', () => {
	beforeEach(() => {server = require('../../server');});
	afterEach(async () => {
		await User.deleteMany({});
		await server.close();
	});

	describe('GET /page/:page/size/:pageSize', () => {
		let page, pageSize;

		const exec = async () => {
			return await request(server)
				.get(`/api/users/page/${page}/size/${pageSize}`);
		};

		beforeEach(async () => {
			await User.collection.insertMany([
				{name: 'user1', email: 'user1@mail.com', password: '12345'},
				{name: 'user2', email: 'user2@mail.com', password: '12345'},
				{name: 'user3', email: 'user3@mail.com', password: '12345'},
				{name: 'user4', email: 'user4@mail.com', password: '12345'},
				{name: 'user5', email: 'user5@mail.com', password: '12345'},
				{name: 'user6', email: 'user6@mail.com', password: '12345'},
				{name: 'user7', email: 'user7@mail.com', password: '12345'},
				{name: 'user8', email: 'user8@mail.com', password: '12345'}
			]);
			page = 2;
			pageSize = 3;
		});

		it('should return all users for current page', async () => {
			const res = await exec();

			const {users} = res.body;

			expect(res.status).toBe(200);
			expect(users.length).toBe(3);
			expect(users.some(user => user.name === 'user4'))
				.toBeTruthy();
			expect(users.some(user => user.name === 'user5'))
				.toBeTruthy();
			expect(users.some(user => user.name === 'user6'))
				.toBeTruthy();
		});

		it('should return first page if given page is less than 1', async () => {
			page = 0;

			const res = await exec();

			const {users} = res.body;

			expect(res.status).toBe(200);
			expect(users.length).toBe(3);
			expect(users.some(user => user.name === 'user1'))
				.toBeTruthy();
			expect(users.some(user => user.name === 'user2'))
				.toBeTruthy();
			expect(users.some(user => user.name === 'user3'))
				.toBeTruthy();
		});

		it('should return last page if given page is more then max page', async () => {
			page = 4;

			const res = await exec();

			const {users} = res.body;

			expect(res.status).toBe(200);
			expect(users.length).toBe(2);
			expect(users.some(user => user.name === 'user7'))
				.toBeTruthy();
			expect(users.some(user => user.name === 'user8'))
				.toBeTruthy();
		});

		it('should return information about pages', async () => {
			const res = await exec();

			const {pages} = res.body;

			expect(res.status).toBe(200);
			expect(pages.length).toBe(3);
			expect(pages.some(page => {
				page.number === 1 && page.active === false
			}));
			expect(pages.some(page => {
				page.number === 2 && page.active === true
			}));
			expect(pages.some(page => {
				page.number === 3 && page.active === false
			}));
		});
	});

	describe('GET /me', () => {
		let user, token;

		const exec = async () => {
			return await request(server)
				.get('/api/users/me')
				.set('x-auth-token', token);
		};

		beforeEach(async () => {
			user = new User({
				_id: mongoose.Types.ObjectId(),
				name: '12345',
				email: 'user1@mail.com',
				password: '12345'
			});
			await user.save();

			token = user.generateAuthToken();
		});

		it('should return 401 if client is not logged in', async () => {
			token = '';

			const res = await exec();

			expect(res.status).toBe(401); 
		});

		it('should return current user', async () => {
			const res = await exec();

			expect(res.status).toBe(200);
			expect(res.body)
				.toHaveProperty('_id', user._id.toHexString());
		});
	});

	describe('GET /:id', () => {
		let user, userId;

		const exec = async () => {
			return await request(server)
				.get(`/api/users/${userId}`);
		};

		beforeEach(async () => {
			user = new User({
				_id: mongoose.Types.ObjectId(),
				name: '12345',
				email: 'user1@mail.com',
				password: '12345'
			});
			await user.save();

			userId = user._id;
		});

		it('should return 404 if ID is invalid', async () => {
			userId = '1';

			const res = await exec();

			expect(res.status).toBe(404);
		});

		it('should return 404 if user with given ID is not exists', async () => {
			userId = mongoose.Types.ObjectId();

			const res = await exec();

			expect(res.status).toBe(404);
		});

		it('should return user if ID is valid', async () => {
			const res = await exec();

			expect(res.status).toBe(200);
			expect(res.body)
				.toHaveProperty('_id', user._id.toHexString());
		});
	});

	describe('GET /latest/:limit', () => {
		let limit;

		const exec = async () => await request(server)
			.get(`/api/users/latest/${limit}`);

		beforeEach(async () => {
			await User.collection.insertMany([
				{name: 'user01', email: 'user1@mail.com', password: '12345'},
				{name: 'user02', email: 'user2@mail.com', password: '12345'},
				{name: 'user03', email: 'user3@mail.com', password: '12345'},
				{name: 'user04', email: 'user4@mail.com', password: '12345'}
			]);

			limit = 3;
		});

		it('should return 400 if invalid data is passed', async () => {
			limit = 'aaa';

			const res = await exec();
			
			expect(res.status).toBe(400);
		});

		it('should return required number of users', async () => {
			const res = await exec();
			
			expect(res.status).toBe(200);
			expect(res.body.length).toBe(3);
			expect(res.body.some(user => user.name === 'user04'))
				.toBeTruthy();
			expect(res.body.some(user => user.name === 'user03'))
				.toBeTruthy();
			expect(res.body.some(user => user.name === 'user02'))
				.toBeTruthy();
		});
	});

	describe('GET /search/:keyword', () => {
		let keyword;

		const exec = async () => {
			return await request(server)
				.get(`/api/users/search/${keyword}`);
		};

		beforeEach(async () => {
			await User.collection.insertMany([
				{name: 'userKeyword01', email: 'user1@mail.com'},
				{name: 'user02', email: 'user2@mail.com'},
				{name: 'userKeyWord03', email: 'user3@mail.com'}
			]);

			keyword = 'keyword';
		});

		it('should return users with keywords', async () => {
			const res = await exec();

			expect(res.status).toBe(200);
			expect(res.body.length).toBe(2);
			expect(res.body.some(user =>
				user.name === 'userKeyword01')).toBeTruthy();
			expect(res.body.some(user =>
				user.name === 'userKeyWord03')).toBeTruthy();
		});
	});

	describe('POST /', () => {
		let name, email, password, user, salt;

		const exec = async () => {
			return await request(server)
				.post('/api/users')
				.send({name, email, password});
		};

		beforeEach(() => {
			name = '54321';
			email = 'user2@mail.com';
			password = '54321';
		});

		it('should return 400 if user is already exist', async () => {
			user = new User({
				_id: mongoose.Types.ObjectId(),
				name: '12345',
				email: 'user1@mail.com',
				password: '12345'
			});
			salt = await bcrypt.genSalt(10);
			user.password = await bcrypt.hash(user.password, salt);

			await user.save();
			email = user.email;

			const res = await exec();

			expect(res.status).toBe(400);
		});

		it('should return 400 if name is less than 5 characters', async () => {
			name = '1234';

			const res = await exec();

			expect(res.status).toBe(400);
		});

		it('should return 400 if email is invalid', async () => {
			email = '1';

			const res = await exec();

			expect(res.status).toBe(400);
		});

		it('should return 400 if password is less than 5 characters', async () => {
			password = '1234';

			const res = await exec();

			expect(res.status).toBe(400);
		});

		it('should return 400 if name is more than 50 characters', async () => {
			name = new Array(52).join('a');

			const res = await exec();

			expect(res.status).toBe(400);
		});

		it('should return 400 if password is more than 1250 characters', async () => {
			password = new Array(1252).join('a');

			const res = await exec();

			expect(res.status).toBe(400);
		});

		it('should create a new user if given data is valid', async () => {
			const res = await exec();

			user = await User.findById(res.body._id);

			const passwordIsValid = await bcrypt.compare(
				password, user.password);

			expect(res.status).toBe(200);
			expect(user).toHaveProperty('name', name);
			expect(user).toHaveProperty('email', email);
			expect(passwordIsValid).toBeTruthy();
		});

		it('should return created user and token if given data is valid', async () => {
			const res = await exec();

			user = await User.findById(res.body._id);
			token = user.generateAuthToken();

			expect(res.status).toBe(200);
			expect(res.header['x-auth-token']).toEqual(token);
			expect(res.body).toHaveProperty('_id');
			expect(res.body).toHaveProperty('name', name);
			expect(res.body).toHaveProperty('email', email);
		});
	});

	describe('PUT /', () => {
		let name, email, password, token, user, salt;

		const exec = async () => {
			return await request(server)
				.put('/api/users')
				.set('x-auth-token', token)
				.send({name, email, password});
		};

		beforeEach(async () => {
			user = new User({
				_id: mongoose.Types.ObjectId(),
				name: '12345',
				email: 'user1@mail.com',
				password: '12345'
			});
			salt = await bcrypt.genSalt(10);
			user.password = await bcrypt.hash(user.password, salt);
			await user.save();

			token = user.generateAuthToken();

			name = '54321';
			email = 'user2@mail.com';
			password = '54321';
		});

		it('should return 401 if client is not logged in', async () => {
			token = '';
			
			const res = await exec();

			expect(res.status).toBe(401);
		});

		it('should return 400 if name is less than 5 characters', async () => {
			name = '1234';

			const res = await exec();

			expect(res.status).toBe(400);
		});

		it('should return 400 if email is invalid', async () => {
			email = '1';

			const res = await exec();

			expect(res.status).toBe(400);
		});

		it('should return 400 if password is less than 5 characters', async () => {
			password = '1234';

			const res = await exec();

			expect(res.status).toBe(400);
		});

		it('should return 400 if name is more than 50 characters', async () => {
			name = new Array(52).join('a');

			const res = await exec();

			expect(res.status).toBe(400);
		});

		it('should return 400 if password is more than 1250 characters', async () => {
			password = new Array(1252).join('a');

			const res = await exec();

			expect(res.status).toBe(400);
		});

		it('should allow to update only name', async () => {
			email = password = null;

			const res = await exec();

			user = await User.findById(user._id);

			const passwordIsValid = await bcrypt.compare(
				'12345', user.password);

			expect(res.status).toBe(200);
			expect(user).toHaveProperty('name', name);
			expect(user).toHaveProperty('email', 'user1@mail.com');
			expect(passwordIsValid).toBeTruthy();
		});

		it('should allow to update only email', async () => {
			name = password = null;

			const res = await exec();

			user = await User.findById(user._id);

			const passwordIsValid = await bcrypt.compare(
				'12345', user.password);

			expect(res.status).toBe(200);
			expect(user).toHaveProperty('name', '12345');
			expect(user).toHaveProperty('email', email);
			expect(passwordIsValid).toBeTruthy();
		});

		it('should allow to update only password', async () => {
			email = name = null;

			const res = await exec();

			user = await User.findById(user._id);

			const passwordIsValid = await bcrypt.compare(
				password, user.password);

			expect(res.status).toBe(200);
			expect(user).toHaveProperty('name', '12345');
			expect(user).toHaveProperty('email', 'user1@mail.com');
			expect(passwordIsValid).toBeTruthy();
		});

		it('should update current user if given data is valid', async () => {
			const res = await exec();

			user = await User.findById(user._id);

			const passwordIsValid = await bcrypt.compare(
				password, user.password);

			expect(res.status).toBe(200);
			expect(user).toHaveProperty('name', name);
			expect(user).toHaveProperty('email', email);
			expect(passwordIsValid).toBeTruthy();
		});

		it('should return updated user if given data is valid', async () => {
			const res = await exec();

			expect(res.status).toBe(200);
			expect(res.body).toHaveProperty('_id', user._id.toHexString());
			expect(res.body).toHaveProperty('name', name);
			expect(res.body).toHaveProperty('email', email);
		});
	});

	describe('DELETE /:id', () => {	
		let token, user, userId, salt, article1, article1Id,
			article2Id, article2, article3, comment1, comment2,
				comment3, category1, category2, tag01, tag02;

		const exec = async () => {
			return await request(server)
				.delete(`/api/users/${userId}`)
				.set('x-auth-token', token);
		};

		beforeEach(async () => {
			user = new User({
				_id: mongoose.Types.ObjectId(),
				name: '12345',
				email: 'user1@mail.com',
				password: '12345'
			});
			salt = await bcrypt.genSalt(10);
			user.password = await bcrypt.hash(user.password, salt);
			await user.save();

			token = user.generateAuthToken();
			userId = user._id.toHexString();

			article1Id = mongoose.Types.ObjectId();
			article2Id = mongoose.Types.ObjectId();

			category1 = new Category({
				_id: mongoose.Types.ObjectId(),
				title: 'category1',
				articles: [article1Id]
			});
			category2 = new Category({
				_id: mongoose.Types.ObjectId(),
				title: 'category2',
				articles: [article2Id]
			});
			await category1.save();
			await category2.save();

			tag01 = new Tag({
				_id: mongoose.Types.ObjectId(),
				title: 'tag01',
				articles: [article1Id]
			});
			tag02 = new Tag({
				_id: mongoose.Types.ObjectId(),
				title: 'tag02',
				articles: [article2Id]
			});
			await tag01.save();
			await tag02.save();

			article1 = new Article({
				_id: article1Id,
				title: '12345',
				content: '12345',
				user: {
					_id: userId,
					name: user.name
				},
				category: {
					_id: category1._id,
					title: category1.title
				},
				tags: [{
					_id: tag01._id,
					title: tag01.title
				}]
			});
			article2 = new Article({
				_id: article2Id,
				title: '54321',
				content: '54321',
				user: {
					_id: userId,
					name: user.name
				},
				category: {
					_id: category2._id,
					title: category2.title
				},
				tags: [{
					_id: tag02._id,
					title: tag02.title
				}]
			});
			article3 = new Article({
				_id: mongoose.Types.ObjectId(),
				title: '98765',
				content: '98765',
				user: {
					_id: mongoose.Types.ObjectId(),
					name: 'random'
				}
			});
			await article1.save();
			await article2.save();
			await article3.save();

			comment1 = new Comment({
				_id: mongoose.Types.ObjectId(),
				text: 'comment1',
				user: mongoose.Types.ObjectId(),
				article: article1._id
			});
			comment2 = new Comment({
				_id: mongoose.Types.ObjectId(),
				text: 'comment2',
				user: mongoose.Types.ObjectId(),
				article: article2._id
			});
			comment3 = new Comment({
				_id: mongoose.Types.ObjectId(),
				text: 'comment3',
				user: user._id,
				article: article3._id
			});
			await comment1.save();
			await comment2.save();
			await comment3.save();
		});
		afterEach(async () => {
			await Article.deleteMany({});
			await Comment.deleteMany({});
			await Category.deleteMany({});
			await Tag.deleteMany({});
		});

		it('should return 401 if client is not logged in', async () => {
			token = '';
			
			const res = await exec();

			expect(res.status).toBe(401);
		});

		it('should return 404 if ID is invalid', async () => {
			userId = '1';

			const res = await exec();

			expect(res.status).toBe(404);
		});

		it('should return 404 if user with given ID is not exists', async () => {
			userId = mongoose.Types.ObjectId();

			const res = await exec();

			expect(res.status).toBe(404);
		});

		it('should return 401 if user is not authorized', async () => {
			token = new User().generateAuthToken();
			
			const res = await exec();

			expect(res.status).toBe(401);
		});

		it('should delete given user if user is authorized', async () => {
			const res = await exec();

			user = await User.findById(userId);

			expect(res.status).toBe(200);
			expect(res.body).toHaveProperty('_id', userId);
			expect(user).toBeNull();
		});

		it('should delete given user if current user is admin', async () => {
			token = new User({isAdmin: true}).generateAuthToken();

			const res = await exec();

			user = await User.findById(userId);

			expect(res.status).toBe(200);
			expect(res.body).toHaveProperty('_id', userId);
			expect(user).toBeNull();
		});

		it('should delete all user articles with comments', async () => {
			const res = await exec();

			const articles = await Article
				.find({'user._id': user._id.toHexString()});
			
			const comments1 = await Comment
				.find({article: article1._id});
			const comments2 = await Comment
				.find({article: article2._id});
			
			expect(res.status).toBe(200);
			expect(articles.length).toBe(0);
			expect(comments1.length).toBe(0);
			expect(comments2.length).toBe(0);
		});

		it('should remove deleted articles from categories', async () => {
			const res = await exec();

			const firstArticleCategory = await Category
				.findOne({articles: article1Id});
			const secondArticleCategory = await Category
				.findOne({articles: article2Id});

			expect(res.status).toBe(200);
			expect(firstArticleCategory).toBeNull();
			expect(secondArticleCategory).toBeNull();
		});

		it('should remove deleted articles from tags', async () => {
			const res = await exec();

			const firstArticleTags = await Tag
				.find({articles: article1Id});
			const secondArticleTags = await Tag
				.find({articles: article2Id});

			expect(res.status).toBe(200);
			expect(firstArticleTags.length).toBe(0);
			expect(secondArticleTags.length).toBe(0);
		});

		it('should delete all user comments', async () => {
			const res = await exec();

			const comments = await Comment
				.find({user: user._id});
			
			expect(res.status).toBe(200);
			expect(comments.length).toBe(0);
		});
	});
});