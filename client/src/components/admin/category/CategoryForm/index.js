import {connect} from 'react-redux';
import {compose} from 'redux';
import CategoryFormContent from './CategoryFormContent';
import {withForm} from '../../../hoc/withForm';
import {
  findItem,
  createItem,
  updateItem,
  clearForm
} from '../../../../actions/admin/FormActions';

const mapStateToProps = store => {
  const {
  	currentCategoryId,
  	currentCategoryTitle,
  	newCategoryData
  } = store.categoryForm;
  
  return {
    route: 'categories',
    itemName: 'category',
  	updatingItemId: currentCategoryId,
    newItemData: newCategoryData,
    initialValues: {
    	title: currentCategoryTitle
    }
  }
};

const mapDispatchToProps = dispatch => ({
	findItem: (id, route) => dispatch(findItem(id, route)),
  createItem: (data, route) => dispatch(createItem(data, route)),
  updateItem: (id, data, route) =>
    dispatch(updateItem(id, data, route)),
  clearForm: () => dispatch(clearForm())
});

export default compose(
	connect(mapStateToProps, mapDispatchToProps),
	withForm
)(CategoryFormContent);