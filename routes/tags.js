const pagination = require('../helpers/pagination');
const admin = require('../middleware/admin');
const auth = require('../middleware/auth');
const validateInput = require('../middleware/validateInput');
const validateObjectId = require('../middleware/validateObjectId');
const {Tag, validateTag} = require('../models/Tag');
const {Article} = require('../models/Article');
const express = require('express');
const router = express.Router();

router.get('/all', async (req, res) => {
	const tags = await Tag.find();

	res.send(tags);
});

router.get('/page/:page/size/:pageSize', async (req, res) => {
	let {page, pageSize} = req.params;

	const paginate = await pagination(
		Tag, page, pageSize);

	const {beginIndex, pages} = paginate;
	pageSize = paginate.pageSize;

	const tags = await Tag
		.find()
		.skip(beginIndex)
		.limit(pageSize)
		.sort({title: 1});

	res.send({tags, pages});
});

router.get('/:id', validateObjectId, async (req, res) => {
	const tag = await Tag.findById(req.params.id);
	if (!tag)
		return res.status(404).send('Cannot find the tag with given ID');

	res.send(tag);
});

router.post('/', [auth, admin, validateInput(validateTag)],
	async (req, res) => {
		const {title} = req.body;
		const tag = new Tag({title});
		await tag.save();

		res.send(tag);
});

router.put('/:id', [auth, admin, validateObjectId,
	validateInput(validateTag)], async (req, res) => {
		const {title} = req.body;
		const tag = await Tag.findByIdAndUpdate(
			req.params.id, {title}, {new: true});
		if (!tag)
			return res.status(404).send('Cannot find tag with given ID');

		await Article.updateMany(
			{tags: {
				$elemMatch: {_id: tag._id}
			}},
			{$set: {
				'tags.$.title': title
			}}
		);
		
		res.send(tag);
});

router.delete('/:id', [auth, admin, validateObjectId],
	async (req, res) => {
		const tag = await Tag.findByIdAndRemove(req.params.id);
		if (!tag)
			return res.status(404).send('Cannot find tag with given ID');

		await Article.updateMany(
			{tags: {
				$elemMatch: {_id: tag._id}
			}},
			{$pull: {
				tags: {_id: tag._id}
			}}
		);

		res.send(tag);
});

module.exports = router;