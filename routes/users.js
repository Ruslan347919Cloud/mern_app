const config = require('config');
const fs = require('fs');
const pagination = require('../helpers/pagination');
const auth = require('../middleware/auth');
const bcrypt = require('bcrypt');
const _ = require('lodash');
const {
	imageUpload,
	uploadsAbsolutePath,
	cloudinary,
	deleteImageFromCloudinary
} = require('../helpers/imageUpload');
const validateInput = require('../middleware/validateInput');
const validateObjectId = require('../middleware/validateObjectId');
const {
	User,
	validateUser,
	validateUserUpdate
} = require('../models/User');
const {Article} = require('../models/Article');
const {Comment} = require('../models/Comment');
const {Category} = require('../models/Category');
const {Tag} = require('../models/Tag');
const express = require('express');
const router = express.Router();

router.get('/page/:page/size/:pageSize', async (req, res) => {
	let {page, pageSize} = req.params;

	const paginate = await pagination(
		User, page, pageSize);

	const {beginIndex, pages} = paginate;
	pageSize = paginate.pageSize;

	const users = await User
		.find()
		.skip(beginIndex)
		.limit(pageSize)
		.sort({_id: 1})
		.select('-password');

	res.send({users, pages});
});

router.get('/me', auth, async (req, res) => {
	const user = await User.findById(
		req.user._id).select('-password');
	
	res.send(user);
});

router.get('/:id', validateObjectId, async (req, res) => {
	const user = await User
		.findById(req.params.id)
		.select('-password');
	if (!user)
		return res.status(404).send('Cannot find user with given ID.');

	res.send(user);
});

router.get('/latest/:limit', async (req, res) => {
	const limit = Number(req.params.limit);
	if (isNaN(limit))
		return res.status(400).send('Invalid value.');

	const users = await User
		.find()
		.limit(limit)
		.sort({_id: -1})
		.select('-password');

	res.send(users);
});

router.get('/search/:keyword', async (req, res) => {
	const keyword = new RegExp('.*'+req.params.keyword+'.*', 'i');
	const limit = 10;

	const users = await User
		.find({name: keyword})
		.select('-password')
		.limit(limit);

	res.send(users);
});

router.post('/', validateInput(validateUser),
	async (req, res) => {
		const {name, email, password} = req.body;
		let user = await User.findOne({email});
		if (user)
			return res.status(400).send('This user is already exists.');

		user = new User({name, email, password});

		const salt = await bcrypt.genSalt(10);
		user.password = await bcrypt.hash(user.password, salt);

		await user.save();

		const token = user.generateAuthToken();

		res.header('x-auth-token', token)
			.send(_.pick(user, ['_id', 'name', 'email']));
});

router.post('/uploadImage/:id', [auth, imageUpload],
	async (req, res) => {
		const uploaded = await cloudinary.uploader
			.upload(req.file.path, options = {use_filename: true});

		if (!uploaded)
			res.status(400).send('Cannon upload image to cloud');

	  const oldUserData = await User.findByIdAndUpdate(
			req.params.id, {photo: uploaded.url}, {new: false});

	  if (!oldUserData)
			return res.status(404).send('Cannot update current user');

		if (oldUserData.photo)
			await deleteImageFromCloudinary(oldUserData.photo);

		const {filename} = req.file;
		const tempImage = `${uploadsAbsolutePath}/${filename}`;
		
		await fs.stat(tempImage, async (err, stats) => {
	    if (err) {
        console.log('file not found');
	    } else {
        await fs.unlink(tempImage, err => {
				  if (err) throw err;
				  console.log(`file ${filename} was deleted`);
				});
	    }
		});

		res.send(_.pick(oldUserData, ['_id', 'name', 'email']));
});

router.put('/', [auth, validateInput(validateUserUpdate)],
	async (req, res) => {
		const {name, email, password} = req.body;
		const userFields = {};
		if (name)
			userFields.name = name;
		if (email)
			userFields.email = email;
		if (password) {
			const salt = await bcrypt.genSalt(10);
			userFields.password = await bcrypt.hash(password, salt);
		}

		const user = await User.findByIdAndUpdate(
			req.user._id, userFields, {new: true});
		if (!user)
			return res.status(404).send('Cannot update current user');

		res.send(_.pick(user, ['_id', 'name', 'email']));
});

router.delete('/:id', [auth, validateObjectId], async (req, res) => {
	const {id} = req.params;

	let user = await User.findById(id);
	if (!user)
		return res.status(404).send('Cannot find user with given ID');

	if (!user.checkAuthorization(req))
		return res.status(401).send('User is not authorized');

	user = await User.findByIdAndRemove(id);

	if (user.photo)
		await deleteImageFromCloudinary(user.photo);

	const articles = await Article
		.find({'user._id': user._id.toHexString()});

	articles.forEach(async article => {
		await Comment
			.deleteMany({'article': article._id.toHexString()});

		if (article.image)
			await deleteImageFromCloudinary(article.image);

		await Category.updateOne(
			{articles: article._id},
			{$pull: {articles: {$in: [article._id]}}}
		);

		await Tag.updateMany(
			{articles: article._id},
			{$pull: {articles: {$in: [article._id]}}}
		);
	});

	await Article
		.deleteMany({'user._id': user._id.toHexString()});

	await Comment
		.deleteMany({user: user._id.toHexString()});

	res.send(_.pick(user, ['_id', 'name', 'email']));
});

module.exports = router;