import {
	FETCH_SINGLE_ARTICLE,
	FETCH_COMMENTS,
	CHANGE_ARTICLE_STATUS
} from '../../../actions/Types';

const initialState = {
	article: {},
	comments: [],
	pages: [],
	status: null
};

export const articleViewReducer = function(state = initialState, action) {
	switch (action.type) {
		case FETCH_SINGLE_ARTICLE:
			return {
				...state,
				article: action.payload
			};
		case FETCH_COMMENTS:
			return {
				...state,
				...action.payload
			};
		case CHANGE_ARTICLE_STATUS:
			return {
				...state,
				status: action.payload.visible
			};
		default: return state;
	}
};