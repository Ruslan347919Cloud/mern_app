import React from 'react';
import PropTypes from 'prop-types';
import {Redirect} from 'react-router-dom';
import {connect} from 'react-redux';
import SetAttributesFormContent from './SetAttributesFormContent';
import {
	getArticleAttributes,
	fetchAttributes,
	clearAttributesData,
	setAttribute
} from '../../../../actions/admin/Article/SetAttributesActions';

class SetAttributes extends React.Component {
	componentDidMount() {
		const {attribute, id} = this.props.match.params;

		this.props.fetchAttributes(attribute);
		this.props.getArticleAttributes(id);
	};

	componentWillUnmount() {
		this.props.clearAttributesData();
	};

	getValues() {
		const {attribute} = this.props.match.params;
		const {category, tags} = this.props;
		let fieldName, values;

		switch(attribute) {
			case 'categories':
				fieldName = 'category';
				values = category && category._id;
			break;
			case 'tags':
				fieldName = 'tags';
				values = tags.map(tag => tag._id);
			break;
			default: return null;
		}
		
		return {
			initialValues: {[fieldName]: values},
			fieldName
		};
	};

	submitHandler = formData => {
		const {id} = this.props.match.params;

		this.props.setAttribute(id, formData);
	};

	formIsSubmitted() {
		const {receivedData} = this.props;

		return ((Object.keys(receivedData).length !== 0)
			&& (typeof receivedData._id !== 'undefined'));
	};

	render() {
		const {attributesAll, receivedData} = this.props;
		const {initialValues, fieldName} = this.getValues();

		if (this.formIsSubmitted())
			return (<Redirect to={`/admin/article/view/${receivedData._id}`}/>);
		else return (
			<div className="col-md-12 articles">
				<SetAttributesFormContent
					onSubmit={this.submitHandler}
					attributesAll={attributesAll}
					fieldName={fieldName}
					initialValues={initialValues}
				/>
			</div>
		);
	};
};

SetAttributes.propTypes = {
	attributesAll: PropTypes.array.isRequired,
	category: PropTypes.object,
	tags: PropTypes.array.isRequired,
	receivedData: PropTypes.object.isRequired,
	getArticleAttributes: PropTypes.func.isRequired,
	fetchAttributes: PropTypes.func.isRequired,
	setAttribute: PropTypes.func.isRequired,
	clearAttributesData: PropTypes.func.isRequired
};

const mapStateToProps = store => {
  const {
  	category,
  	tags,
  	attributesAll,
  	receivedData
  } = store.setAttributes;

  return {category, tags, attributesAll, receivedData};
};

const mapDispatchToProps = dispatch => ({
	getArticleAttributes: articleId =>
		dispatch(getArticleAttributes(articleId)),
	fetchAttributes: attribute =>
		dispatch(fetchAttributes(attribute)),
	setAttribute: (id, data) =>
		dispatch(setAttribute(id, data)),
	clearAttributesData: () => dispatch(clearAttributesData())
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(SetAttributes);