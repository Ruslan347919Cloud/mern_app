import {connect} from 'react-redux';
import {compose} from 'redux';
import SingleComment from './SingleComment';
import {withView} from '../../../hoc/withView';
import {
  fetchSingleComment,
  changeCommentStatus
} from '../../../../actions/CommentActions';
import {
  deleteItem
} from '../../../../actions/admin/ListActions';

const mapStateToProps = store => {
  const {comment, status} = store.commentView;
  const {deletedItemId} = store.list;
  return {
    itemName: 'comment',
    item: comment,
    deletedItemId,
    status
  }
};

const mapDispatchToProps = dispatch => ({
  fetchSingleItem: (id, page, limit) => dispatch(
    fetchSingleComment(id, page, limit)),
  deleteItem: (id, route) => dispatch(deleteItem(id, route)),
  changeStatus: (id) => dispatch(changeCommentStatus(id))
});

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  withView
)(SingleComment);