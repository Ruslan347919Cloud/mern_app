import axios from 'axios';
import {setAlert} from './AlertActions';
import {
	FETCH_SINGLE_ARTICLE,
	FETCH_COMMENTS,
	CHANGE_ARTICLE_STATUS
} from './Types';

export const fetchSingleArticle = articleId => async dispatch => {
	try {
		const res = await axios.get(`/api/articles/${articleId}`);
		
		dispatch({
			type: FETCH_SINGLE_ARTICLE,
			payload: res.data
		});
	} catch (err) {
		const {data: error, status, statusText} = err.response;
		
		console.log(statusText, status);
		console.log(error);
		
		if (error)
			dispatch(setAlert(error, 'danger'));
	}
};

export const fetchComments = (articleId, page, size) => async dispatch => {
	try {
		const res = await axios.get(
			`/api/comments/article/${articleId}/page/${page}/size/${size}`);
		
		dispatch({
			type: FETCH_COMMENTS,
			payload: res.data
		});
	} catch (err) {
		const {data: error, status, statusText} = err.response;

		console.log(statusText, status);
		console.log(error);
	}
};

export const changeArticleStatus = id => async dispatch => {
	const config = {
		headers: {
			'Content-Type': 'application/json'
		}
	};

	const body = JSON.stringify({visible: true});

	try {
		const res = await axios.put(`/api/articles/${id}`, body, config);
		
		dispatch({
			type: CHANGE_ARTICLE_STATUS,
			payload: res.data
		});

	} catch (err) {
		const {data: error, status, statusText} = err.response;
		
		console.log(statusText, status);
		console.log(error);
		
		if (error)
			dispatch(setAlert(error, 'danger'));
	}
};