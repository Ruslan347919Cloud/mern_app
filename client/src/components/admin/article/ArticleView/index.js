import {connect} from 'react-redux';
import {compose} from 'redux';
import SingleArticle from './SingleArticle';
import {withView} from '../../../hoc/withView';
import {
  fetchSingleArticle,
  fetchComments,
  changeArticleStatus
} from '../../../../actions/SingleArticleActions';
import {
  deleteItem
} from '../../../../actions/admin/ListActions';

const mapStateToProps = store => {
  const {article, comments, pages, status} = store.articleView;
  const {deletedItemId} = store.list;

  return {
    itemName: 'article',
    item: article,
    junctionItems: comments,
    pages: pages,
    deletedItemId: deletedItemId,
    status: status
  };
};

const mapDispatchToProps = dispatch => ({
  fetchSingleItem: id => dispatch(fetchSingleArticle(id)),
  fetchJunctionItems: (id, page, size) =>
    dispatch(fetchComments(id, page, size)),
  deleteItem: (id, route) => dispatch(
    deleteItem(id, route)),
  changeStatus: id => dispatch(changeArticleStatus(id))
});

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  withView
)(SingleArticle);