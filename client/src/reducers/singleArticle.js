import {
	FETCH_SINGLE_ARTICLE,
	FETCH_COMMENTS
} from '../actions/Types';

const initialState = {
	article: {},
	comments: [],
	pages: []
};

export const singleArticleReducer = function(state = initialState, action) {
	const {type, payload} = action;

	switch (type) {
		case FETCH_SINGLE_ARTICLE:
			return {
				...state,
				article: payload
			};
		case FETCH_COMMENTS:
			return {
				...state,
				...payload
			};
		default: return state;
	}
};