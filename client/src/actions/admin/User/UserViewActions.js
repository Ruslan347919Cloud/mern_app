import axios from 'axios';
import {setAlert} from '../../AlertActions';
import {logout} from '../../AuthActions';
import {
	FETCH_USER,
	FETCH_USER_ARTICLES,
	DELETE_USER,
	CLEAR_USER_DATA
} from '../../Types';

export const fetchUser = userId => async dispatch => {
	try {
		const res = await axios.get(`/api/users/${userId}`);
		
		dispatch({
			type: FETCH_USER,
			payload: res.data
		});
	} catch (err) {
		const {data: error, status, statusText} = err.response;
		
		console.log(statusText, status);
		console.log(error);
		
		if (error)
			dispatch(setAlert(error, 'danger'));
	}
};

export const fetchUserArticles = (userId, page, pageSize) => async dispatch => {
	try {
		const res = await axios.get(
			`/api/articles/author/${userId}/page/${page}/size/${pageSize}`);
		
		dispatch({
			type: FETCH_USER_ARTICLES,
			payload: res.data
		});
	} catch (err) {
		const {data: error, status, statusText} = err.response;
		
		console.log(statusText, status);
		console.log(error);
	}
};

export const deleteUser = userId => async dispatch => {
	try {
		const res = await axios.delete(`/api/users/${userId}`);
		
		dispatch({
			type: DELETE_USER,
			payload: res.data
		});

		dispatch(logout());
	} catch (err) {
		const {data: error, status, statusText} = err.response;
		
		console.log(statusText, status);
		console.log(error);
		
		if (error)
			dispatch(setAlert(error, 'danger'));
	}
};

export const clearUserData = () => ({
	type: CLEAR_USER_DATA,
	payload: {
		user: {},
		articles: [],
		pages: [],
		deleted: false
	}
});