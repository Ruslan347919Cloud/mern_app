const validateInput = require('../middleware/validateInput');
const bcrypt = require('bcrypt');
const {User} = require('../models/User');
const Joi = require('@hapi/joi');
const express = require('express');
const router = express.Router();

const validate = user => {
	const schema = Joi.object({
		email: Joi.string().min(5).max(255).required().email(),
		password: Joi.string().min(5).max(1250).required()
	});
	return schema.validate(user);
};

router.post('/', validateInput(validate), async (req, res) => {
	const {email, password} = req.body;
	const user = await User.findOne({email});
	if (!user)
		return res.status(400).send('Invalid email or password');

	const passwordIsValid = await bcrypt.compare(
		password, user.password);
	if (!passwordIsValid)
		return res.status(400).send('Invalid email or password');

	const token = user.generateAuthToken();
	res.send(token);
});

module.exports = router;