import axios from 'axios';
import {setAlert} from './AlertActions';
import {SORT_BY} from './Types';

export const sortBy = (model, id, page, size) => async dispatch => {
	
	try {
		const res = await axios.get(
			`/api/articles/${model}/${id}/page/${page}/size/${size}`);

		dispatch({
			type: SORT_BY,
			payload: res.data
		});
	} catch (err) {
		const {data: error, status, statusText} = err.response;
		
		console.log(statusText, status);
		console.log(error);
		
		if (error)
			dispatch(setAlert(error, 'danger'));
	}
};