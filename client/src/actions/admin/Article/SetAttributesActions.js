import axios from 'axios';
import {setAlert} from '../../AlertActions';
import {
	GET_ARTICLE_ATTRIBUTES,
	FETCH_ATTRIBUTES,
	CLEAR_ATTRIBUTES_DATA,
	SET_ATTRIBUTE
} from '../../Types';

export const getArticleAttributes = articleId => async dispatch => {
	try {
		const res = await axios.get(`/api/articles/${articleId}`);
		
		dispatch({
			type: GET_ARTICLE_ATTRIBUTES,
			payload: res.data
		});
	} catch (err) {
		const {data: error, status, statusText} = err.response;
		
		console.log(statusText, status);
		console.log(error);
		
		if (error)
			dispatch(setAlert(error, 'danger'));
	}
};

export const fetchAttributes = attribute => async dispatch => {
	try {
		const res = await axios.get(`/api/${attribute}/all`);
		
		dispatch({
			type: FETCH_ATTRIBUTES,
			payload: res.data
		});

	} catch (err) {
		const {data: error, status, statusText} = err.response;
		
		console.log(statusText, status);
		console.log(error);
		
		if (error)
			dispatch(setAlert(error, 'danger'));
	}
};

export const setAttribute = (id, data) => async dispatch => {
	const config = {
		headers: {
			'Content-Type': 'application/json'
		}
	};

	const body = JSON.stringify(data);

	try {
		const res = await axios.put(`/api/articles/${id}`, body, config);
		
		dispatch({
			type: SET_ATTRIBUTE,
			payload: res.data
		});

	} catch (err) {
		const {data: error, status, statusText} = err.response;
		
		console.log(statusText, status);
		console.log(error);
		
		if (error)
			dispatch(setAlert(error, 'danger'));
	}
};

export const clearAttributesData = () => ({
	type: CLEAR_ATTRIBUTES_DATA,
	payload: {
		attributesAll: [],
		category: {},
		tags: [],
		receivedData: {}
	}
});