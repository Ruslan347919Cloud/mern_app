import React, {Fragment} from 'react';
import {NavLink} from 'react-router-dom';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {logout} from '../../actions/AuthActions';
import SearchForm from './SearchForm';

class Header extends React.Component {
	getCategories() {
		let {categories} = this.props;

		categories = categories.map((category, index) => (
			<Fragment key={category._id}>
				<NavLink
					className="dropdown-item"
					to={`/sort/category/${category._id}`}
				>
					{category.title}
				</NavLink>
        {(categories.length !== index+1 ) &&
        	(<div className="dropdown-divider"></div>)}
      </Fragment>
		));

		return (
			<div className="dropdown-menu" aria-labelledby="navbarDropdown">
        {categories}
      </div>
		);
	};

	render() {
		const {
			auth: {isAuthenticated, loading, user},
			logout
		} = this.props;

		const authLinks = (
			<Fragment>
				<li className="nav-item">
	        <NavLink
	        	className="nav-link text-white"
	        	to={`/admin/user/view/${user && user._id}`}
        	>
	        	{user && user.name} profile
        	</NavLink>
	      </li>
	      {user && user.isAdmin && <li className="nav-item">
	        <NavLink
	        	className="nav-link text-white"
	        	to={'/admin'}
        	>
	        	Admin
        	</NavLink>
	      </li>}
				<li className="nav-item">
	        <NavLink
	        	className="nav-link text-white"
	        	to="#1"
	        	onClick={logout}
        	>
        		<i className="fas fa-sign-out-alt"></i>
        		{' '}Logout
	        </NavLink>
	      </li>
      </Fragment>
		);

		const guestLinks = (
			<Fragment>
				<li className="nav-item">
	        <NavLink className="nav-link text-white" to="/login">Login</NavLink>
	      </li>
	      <li className="nav-item">
	        <NavLink className="nav-link text-white" to="/register">Register</NavLink>
	      </li>
			</Fragment>
		);

		return (
			<header className="container-fluid mb-3 shadow">
				<nav className="navbar navbar-expand-sm border navbar-light bg-primary row">
				  <NavLink className="navbar-brand" to="/">
				  	My News Site
			  	</NavLink>
				  <button
				  	className="navbar-toggler"
				  	type="button"
				  	data-toggle="collapse"
				  	data-target="#navbarMenu"
				  	aria-controls="navbarMenu"
				  	aria-expanded="false"
				  	aria-label="Toggle navigation"
			  	>
				    <i className="navbar-toggler-icon"></i>
				  </button>
				  <div className="collapse navbar-collapse" id="navbarMenu">
				    <ul className="navbar-nav mr-auto">
				      <li className="nav-item active">
				        <NavLink className="nav-link text-white" to="/">
				        	Home
			        	</NavLink>
				      </li>
				      <li className="nav-item dropdown">
				        <a className="nav-link text-white dropdown-toggle"
				        	href="#1"
				        	id="navbarDropdown"
				        	role="button"
				        	data-toggle="dropdown"
				        	aria-haspopup="true"
				        	aria-expanded="false"
			        	>
				          Categories
				        </a>
				        {this.getCategories()}
				      </li>
				      {!loading && (<Fragment>
				      	{isAuthenticated ? authLinks : guestLinks}
			      	</Fragment>)}
				    </ul>
				    <SearchForm />
				  </div>
				</nav>
			</header>
		);
	};
};

Header.propTypes = {
	logout: PropTypes.func.isRequired,
	auth: PropTypes.object.isRequired,
	categories: PropTypes.array
};

const mapStateToProps = store => ({
	auth: store.auth,
	categories: store.sidebar.categories
});

export default connect(mapStateToProps, {logout})(Header);