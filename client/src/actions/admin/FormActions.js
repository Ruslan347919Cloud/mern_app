import axios from 'axios';
import {setAlert} from '../AlertActions';

import {
	CREATE_ITEM,
	FIND_ITEM,
	UPDATE_ITEM,
	CLEAR_FORM
} from '../Types';

export const findItem = (id, route) => async dispatch => {
	try {
		const res = await axios.get(`/api/${route}/${id}`);
		
		dispatch({
			type: FIND_ITEM,
			payload: res.data
		});

	} catch (err) {
		const {data: error, status, statusText} = err.response;
		
		console.log(statusText, status);
		console.log(error);
		
		if (error)
			dispatch(setAlert(error, 'danger'));
	}
};

export const updateItem = (id, data, route) => async dispatch => {
	const config = {
		headers: {
			'Content-Type': 'application/json'
		}
	};

	const body = JSON.stringify(data);

	try {
		const res = await axios.put(`/api/${route}/${id}`, body, config);
		
		dispatch({
			type: UPDATE_ITEM,
			payload: res.data
		});

	} catch (err) {
		const {data: error, status, statusText} = err.response;
		
		console.log(statusText, status);
		console.log(error);
		
		if (error)
			dispatch(setAlert(error, 'danger'));
	}
};

export const createItem = (data, route) => async dispatch => {
	const config = {
		headers: {
			'Content-Type': 'application/json'
		}
	};

	const body = JSON.stringify(data);

	try {
		const res = await axios.post(`/api/${route}`, body, config);
		
		dispatch({
			type: CREATE_ITEM,
			payload: res.data
		});

	} catch (err) {
		const {data: error, status, statusText} = err.response;
		
		console.log(statusText, status);
		console.log(error);
		
		if (error)
			dispatch(setAlert(error, 'danger'));
	}
};

export const clearForm = function() {
	return {
		type: CLEAR_FORM,
		payload: {
			defaultValue: '',
			defaultItemData: {}
		}
	};
};