import axios from 'axios';
import {setAlert} from './AlertActions';
import {
	FETCH_SINGLE_TAG,
	FETCH_ARTICLES_BY_TAG
} from './Types';

export const fetchSingleTag = tagId => async dispatch => {
	try {
		const res = await axios.get(`/api/tags/${tagId}`);
		
		dispatch({
			type: FETCH_SINGLE_TAG,
			payload: res.data
		});
	} catch (err) {
		const {data: error, status, statusText} = err.response;
		
		console.log(statusText, status);
		console.log(error);
		
		if (error)
			dispatch(setAlert(error, 'danger'));
	}
};

export const fetchArticlesByTag = (tagId, page, size) => async dispatch => {
	try {
		const res = await axios.get(
			`/api/articles/tag/${tagId}/page/${page}/size/${size}`);
		
		dispatch({
			type: FETCH_ARTICLES_BY_TAG,
			payload: res.data
		});
	} catch (err) {
		const {data: error, status, statusText} = err.response;

		console.log(statusText, status);
		console.log(error);
	}
};