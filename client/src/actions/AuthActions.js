import axios from 'axios';
import {setAlert} from './AlertActions';
import setAuthToken from '../utils/setAuthToken';
import {
	REGISTER_SUCCESS,
	REGISTER_FAIL,
	USER_LOADED,
	AUTH_ERROR,
	LOGIN_SUCCESS,
	LOGIN_FAIL,
	LOGOUT
} from './Types';

export const loadUser = () => async dispatch => {
	if (localStorage.token)
		setAuthToken(localStorage.token);

	try {
		const res = await axios.get('/api/users/me');

		dispatch({
			type: USER_LOADED,
			payload: res.data
		});
	} catch (err) {
		console.log(err.message);
		dispatch({
			type: AUTH_ERROR
		});
	}
};

export const register = ({name, email, password}) => async dispatch => {
	const config = {
		headers: {
			'Content-Type': 'application/json'
		}
	};

	const body = JSON.stringify({name, email, password});
	try {
		const res = await axios.post('/api/users', body, config);

		dispatch({
			type: REGISTER_SUCCESS,
			payload: {
				token: res.headers['x-auth-token'],
				user: res.data
			}
		});

		dispatch(loadUser());
	} catch (err) {
		const error = err.response.data;
		if (error)
			dispatch(setAlert(error, 'danger'));
		
		dispatch({
			type: REGISTER_FAIL
		});
	};
};

export const login = (email, password) => async dispatch => {
	const config = {
		headers: {
			'Content-Type': 'application/json'
		}
	};

	const body = JSON.stringify({email, password});
	try {
		const res = await axios.post('/api/auth', body, config);

		dispatch({
			type: LOGIN_SUCCESS,
			payload: res.data
		});

		dispatch(loadUser());
	} catch (err) {
		const error = err.response.data;
		if (error)
			dispatch(setAlert(error, 'danger'));
		
		dispatch({
			type: LOGIN_FAIL
		});
	};
};

export const logout = () => dispatch => {
	dispatch({type: LOGOUT});
};
