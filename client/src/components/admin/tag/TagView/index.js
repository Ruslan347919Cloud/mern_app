import {connect} from 'react-redux';
import {compose} from 'redux';
import SingleTag from './SingleTag';
import {withView} from '../../../hoc/withView';
import {
  fetchSingleTag,
  fetchArticlesByTag
} from '../../../../actions/TagActions';
import {
  deleteItem
} from '../../../../actions/admin/ListActions';

const mapStateToProps = store => {
  const {tag, articles, pages} = store.tagView;
  const {deletedItemId} = store.list;

  return {
    itemName: 'tag',
    item: tag,
    junctionItems: articles,
    pages,
    deletedItemId
  }
};

const mapDispatchToProps = dispatch => ({
  fetchSingleItem: id => dispatch(fetchSingleTag(id)),
  fetchJunctionItems: (id, page, size) =>
    dispatch(fetchArticlesByTag(id, page, size)),
  deleteItem: (id, route) => dispatch(deleteItem(id, route))
});

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  withView
)(SingleTag);