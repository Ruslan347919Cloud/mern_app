import React from 'react';
import PropTypes from 'prop-types';
import {Redirect} from 'react-router-dom';

export const withForm = Component => {

	class FormComponent extends React.Component {

		componentDidMount() {
			const {id} = this.props.match.params;
			const {findItem, route} = this.props;

			if (typeof id !== 'undefined')
				findItem(id, route);
		};

		componentWillUnmount() {
			this.props.clearForm();
		};

		submitHandler = formData => {
			const {
				route,
				updatingItemId,
				createItem,
				updateItem
			} = this.props;
			
			if (!updatingItemId)
				createItem(formData, route);
			else
				updateItem(updatingItemId, formData, route);
		};

		formIsSubmitted() {
			const {newItemData} = this.props;

			return ((Object.keys(newItemData).length !== 0)
				&& (typeof newItemData._id !== 'undefined'));
		};

		render() {
			const {itemName, newItemData, initialValues} = this.props;
			
			if (this.formIsSubmitted())
				return <Redirect to={`/admin/${itemName}/view/${newItemData._id}`}/>;
			else return (
				<div className="col-md-7 col-lg-8 articles">
					<Component
						onSubmit={this.submitHandler}
						initialValues={initialValues}
					/>
				</div>
			);
		};
	};

	FormComponent.propTypes = {
		route: PropTypes.string.isRequired,
		itemName: PropTypes.string.isRequired,
		match: PropTypes.object,
		updatingItemId: PropTypes.string.isRequired,
		newItemData: PropTypes.object.isRequired,
		createItem: PropTypes.func.isRequired,
		updateItem: PropTypes.func.isRequired,
		findItem: PropTypes.func.isRequired,
		clearForm: PropTypes.func.isRequired
	};

	return FormComponent;
};