import React from 'react';
import {reduxForm, Field} from 'redux-form';
import {
	required,
	maxLength,
	minLength
} from '../../../../utils/validators';
import {TextArea} from '../../../../common/FormControl/';

const maxLength500 = maxLength(500);
const minLength4 = minLength(4);

const CommentFormContent = props => {
	const {handleSubmit, pristine, invalid, submitting} = props;
	return (
		<form className="my-4" onSubmit={handleSubmit}>
			<div className="form-group">
				<label htmlFor="commentAdmin">Text</label>
		    <Field
		    	name="text"
		    	className="form-control"
		    	id="commentAdmin"
		    	placeholder="Leave the comment"
		    	rows="10"
		    	component={TextArea}
		    	validate={[required, minLength4, maxLength500]}
	    	/>
		  </div>
		  <button
		  	type="submit"
		  	className="btn btn-primary"
		  	disabled={submitting || pristine || invalid}
	  	>
		  	Submit
	  	</button>
		</form>
	);
}

export default reduxForm({
  form: 'commentForm',
  enableReinitialize: true,
  onSubmitSuccess: (result, dispatch, props) => {
    props.reset();
  }
})(CommentFormContent);