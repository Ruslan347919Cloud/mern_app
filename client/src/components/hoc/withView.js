import React from 'react';
import PropTypes from 'prop-types';
import {Redirect} from 'react-router-dom';
import Pagination from '../Pagination/';

export const withView = Component => {

	class ViewComponent extends React.Component {

		constructor(props) {
			super(props);
			this.pageSize = 3;
		};

		componentDidMount() {
			const {id} = this.props.match.params;
			const {fetchSingleItem, fetchJunctionItems} = this.props;

			fetchSingleItem(id);
			if (fetchJunctionItems)
				fetchJunctionItems(id, this.getCurrentPage(), this.pageSize);
		};

		componentDidUpdate(prevProps) {
			const {
				fetchSingleItem,
				fetchJunctionItems,
				status
			} = this.props;
			const {status: statusPrev} = prevProps;
			const {id, page} = this.props.match.params;
			const {id: idPrev, page: pagePrev} = prevProps.match.params;

			if (id !== idPrev) {
				fetchSingleItem(id);
				if (fetchJunctionItems)
					fetchJunctionItems(id, this.getCurrentPage(), this.pageSize);
			}

			if (typeof status !== 'undefined' && status !== statusPrev)
				fetchSingleItem(id);

			if (fetchJunctionItems && (page !== pagePrev))
				fetchJunctionItems(id, this.getCurrentPage(), this.pageSize);
		};

		getCurrentPage() {
			const {page} = this.props.match.params;

			return (typeof page !== 'undefined') ? Number(page) : 1;
		};

		render() {
			const {
				itemName,
				item,
				pages,
				deletedItemId,
				deleteItem,
				junctionItems,
				changeStatus,
				status
			} = this.props;
			const {id} = this.props.match.params;

			if (deletedItemId !== '')
				return <Redirect to={`/admin/${itemName}/index/`}/>;
			return (
				<div className="col-md-12 articles">
					<Component
						item={item}
						deleteItem={deleteItem}
						junctionItems={junctionItems}
						changeStatus={changeStatus}
						status={status}
					/>
					{(pages) && (pages.length > 1) && (<Pagination
						route={`/admin/${itemName}/view/`}
						pages={pages}
						componentId={id}
						currentPage={this.getCurrentPage()}
					/>)}
				</div>
			);
		};
	};

	ViewComponent.propTypes = {
		match: PropTypes.object.isRequired,
		item: PropTypes.object.isRequired,
		itemName: PropTypes.string.isRequired,
		junctionItems: PropTypes.array,
		pages: PropTypes.array,
		deletedItemId: PropTypes.string.isRequired,
		fetchSingleItem: PropTypes.func.isRequired,
		fetchJunctionItems: PropTypes.func,
		deleteItem: PropTypes.func.isRequired,
		changeStatus: PropTypes.func,
		status: PropTypes.bool
	};

	return ViewComponent;
};