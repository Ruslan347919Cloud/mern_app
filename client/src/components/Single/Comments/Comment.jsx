import React from 'react';
import PropTypes from 'prop-types';

class Comment extends React.Component {
	render() {
		let {
			text,
			put_date,
			user: {
				name,
				photo
			}
		} = this.props.comment;
		const defaultImage = `${window.location.origin}/no_avatar.png`;
		photo = photo ? photo : defaultImage;
		//photo = photo ? `${window.location.origin}/uploads/${photo}` : defaultImage;
		
		return (
			<li className="media bg-light rounded shadow my-2 p-1 comment">
		    <a href="#1" className="comment__image align-self-center mr-3">
		    	<img src={photo} className="img-fluid rounded" alt="" />
		    </a>
		    <div className="media-body">
		      <h5 className="my-1">
		      	<a href="#1">{name}</a>
		      </h5>
		      <p>
		      	{text}
		      </p>
		      <small>Added: {put_date}</small>
		    </div>
		  </li>
		);
	};
};

Comment.propTypes = {
	comment: PropTypes.object.isRequired
};

export default Comment;