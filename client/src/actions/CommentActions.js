import axios from 'axios';
import {setAlert} from './AlertActions';
import {
	FETCH_SINGLE_COMMENT,
	CHANGE_COMMENT_STATUS,
	DELETE_COMMENT,
	CREATE_COMMENT,
	UPDATE_COMMENT,
	CLEAR_COMMENT_FORM
} from './Types';

export const createComment = (id, data) => async dispatch => {
	const config = {
		headers: {
			'Content-Type': 'application/json'
		}
	};

	const body = JSON.stringify(data);

	try {
		const res = await axios.post(
			`/api/comments/article/${id}`, body, config);
		
		dispatch({
			type: CREATE_COMMENT,
			payload: res.data
		});

	} catch (err) {
		const {data: error, status, statusText} = err.response;
		
		console.log(statusText, status);
		console.log(error);
		
		if (error)
			dispatch(setAlert(error, 'danger'));
	}
};

export const fetchSingleComment = commentId => async dispatch => {
	try {
		const res = await axios.get(`/api/comments/${commentId}`);
		
		dispatch({
			type: FETCH_SINGLE_COMMENT,
			payload: res.data
		});
	} catch (err) {
		const {data: error, status, statusText} = err.response;
		
		console.log(statusText, status);
		console.log(error);
		
		if (error)
			dispatch(setAlert(error, 'danger'));
	}
};

export const changeCommentStatus = id => async dispatch => {
	const config = {
		headers: {
			'Content-Type': 'application/json'
		}
	};

	const body = JSON.stringify({visible: true});

	try {
		const res = await axios.put(`/api/comments/${id}`, body, config);
		
		dispatch({
			type: CHANGE_COMMENT_STATUS,
			payload: res.data.visible
		});

	} catch (err) {
		const {data: error, status, statusText} = err.response;
		
		console.log(statusText, status);
		console.log(error);
		
		if (error)
			dispatch(setAlert(error, 'danger'));
	}
};

export const updateComment = (id, data) => async dispatch => {
	const config = {
		headers: {
			'Content-Type': 'application/json'
		}
	};

	const body = JSON.stringify(data);

	try {
		const res = await axios.put(`/api/comments/${id}`, body, config);
		
		dispatch({
			type: UPDATE_COMMENT,
			payload: res.data
		});

	} catch (err) {
		const {data: error, status, statusText} = err.response;
		
		console.log(statusText, status);
		console.log(error);
		
		if (error)
			dispatch(setAlert(error, 'danger'));
	}
};

export const deleteComment = id => async dispatch => {
	try {
		const res = await axios.delete(`/api/comments/${id}`);
		
		dispatch({
			type: DELETE_COMMENT,
			payload: res.data._id
		});
	} catch (err) {
		const {data: error, status, statusText} = err.response;
		
		console.log(statusText, status);
		console.log(error);
		
		if (error)
			dispatch(setAlert(error, 'danger'));
	}
};

export const clearCommentForm = function() {
	return {
		type: CLEAR_COMMENT_FORM,
		payload: {
			defaultValue: '',
			defaultData: {}
		}
	};
};