import React, {Fragment} from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import RelatedComments from '../../partial/view/RelatedComments';

class SingleArticle extends React.Component {

	deleteArticle = event => {
		event.preventDefault();

		const {_id} = this.props.item;

		this.props.deleteItem(_id, 'articles');
	};

	changeVisibleStatus = event => {
		event.preventDefault();

		const {_id} = this.props.item;
		const {changeStatus} = this.props;

		changeStatus(_id);
	};

	getTags() {
		const {tags} = this.props.item;

		return tags && tags.map(tag =>
			<span className="ml-2" key={tag._id}>{tag.title}</span>);
	};

	render() {
		const {item, junctionItems} = this.props;
		let {image} = this.props.item;
		const defaultImage = `${window.location.origin}/no_image.jpg`;
		image = image ? image : defaultImage;
		//image = image ? `${window.location.origin}/uploads/${image}` : defaultImage;

		return (
			<Fragment>
				<h2>{item.title}</h2>
				<div className="list-group mb-3 shadow">
					<div className="list-group-item d-flex border">
				    <img src={image}
				    	alt="" className="rounded"
			    	/>
				  </div>
					<div className="list-group-item border">
					   {item.content}
				  </div>
					<div className="list-group-item d-flex justify-content-between border">
				    <span>Id</span>
				    <span>{item._id}</span>
				  </div>
					<div className="list-group-item d-flex justify-content-between border">
				    <span>Title</span>
				    <span>{item.title}</span>
				  </div>
				  <div className="list-group-item d-flex justify-content-between border">
				    <span>Description</span>
				    <span>{item.description}</span>
				  </div>
				  <div className="list-group-item d-flex justify-content-between border">
				    <span>Author</span>
				    <span>{item.user && item.user.name}</span>
				  </div>
				  <div className="list-group-item d-flex justify-content-between border">
				    <span>Category</span>
				    <span>{item.category && item.category.title}</span>
				  </div>
				  <div className="list-group-item d-flex justify-content-between border">
				    <span>Tags</span>
				    <span>{this.getTags()}</span>
				  </div>
				  <div className="list-group-item d-flex justify-content-between border">
				    <span>Last update</span>
				    <span>{item.last_update}</span>
				  </div>
				  <div className="list-group-item d-flex justify-content-between border">
				    <span>Viewed</span>
				    <span>{item.viewed}</span>
				  </div>
				  <div className="list-group-item d-flex justify-content-between border">
				    <span>Visible</span>
				    <span>{String(item.visible)}</span>
				  </div>
				</div>
				<Link className="btn btn-primary btn mr-2 mb-1" to={`/admin/article/form/${item._id}`}>
					Update article
				</Link>
				<Link className="btn btn-primary btn mr-2 mb-1" to={`/upload/articles/article/${item._id}`}>
					Set image
				</Link>
				<Link className="btn btn-primary btn mr-2 mb-1" to={
					`/admin/article/set/categories/${item._id}`
				}>
					Set category
				</Link>
				<Link className="btn btn-primary btn mr-2" to={
					`/admin/article/set/tags/${item._id}`
				}>
					Set tags
				</Link>
				<button className="btn btn-primary btn mr-2" type="button" onClick={this.changeVisibleStatus}>
					Change status
				</button>
				<button className="btn btn-danger btn mr-2" type="button" onClick={this.deleteArticle}>
					Delete article
				</button>
				<RelatedComments comments={junctionItems}/>
			</Fragment>
		);
	};
};

SingleArticle.propTypes = {
	item: PropTypes.object.isRequired,
	junctionItems: PropTypes.array.isRequired,
	deleteItem: PropTypes.func.isRequired,
	changeStatus: PropTypes.func.isRequired
};

export default SingleArticle;