import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import Article from './Article';
import Pagination from '../Pagination/';
import {fetchArticles} from '../../actions/ArticlesActions';

class Articles extends React.Component {
	constructor(props) {
		super(props);
		this.pageSize = 3;
	};

	componentDidMount() {
		const page = this.getCurrentPage();
		this.props.fetchArticles(page, this.pageSize);
	};

	componentDidUpdate(prevProps, prevState, prevContext) {
		const page = this.getCurrentPage();
		const pagePrev = (typeof prevProps.match !== 'undefined')
			? (Number(prevProps.match.params.page)) : 1;
		if (page !== pagePrev)
			this.props.fetchArticles(page, this.pageSize);
	};

	getArticlesList() {
		const {articles} = this.props;
		return articles.map(article => <Article
			key={article._id}
			article={article}
		/>);
	};

	getCurrentPage() {
		return (typeof this.props.match !== 'undefined')
			? Number(this.props.match.params.page) : 1;
	};

	render() {
		const {pages} = this.props;
		return (
			<div className="col-md-7 col-lg-8 articles">
				{this.getArticlesList()}
				{(pages.length > 1) && (<Pagination
					pages={pages}
					currentPage={this.getCurrentPage()}
					route={'/articles/'}
				/>)}
			</div>
		);
	};
};

Articles.propTypes = {
	match: PropTypes.object,
	articles: PropTypes.array.isRequired,
	pages: PropTypes.array.isRequired,
	fetchArticles: PropTypes.func.isRequired
};

const mapStateToProps = store => {
  const {articles, pages} = store.articles;
  return {articles, pages};
};

const mapDispatchToProps = dispatch => ({
  fetchArticles: (page, size) => dispatch(fetchArticles(page, size))
});

export default connect(
	mapStateToProps,
  mapDispatchToProps
)(Articles);