import React from 'react';
import {reduxForm, Field} from 'redux-form';
import {
	required,
	maxLength,
	minLength
} from '../../../../utils/validators';
import {TextArea} from '../../../../common/FormControl/';

const maxLength500 = maxLength(500);
const minLength4 = minLength(4);

const CommentFormForUsersContent = props => {
	const {handleSubmit, pristine, invalid, submitting} = props;
	return (
		<form className="my-4" onSubmit={handleSubmit}>
			<div className="form-group">
		    <Field
		    	name="text"
		    	className="form-control"
		    	id="comment"
		    	placeholder="Leave the comment"
		    	rows="10"
		    	component={TextArea}
		    	validate={[required, minLength4, maxLength500]}
	    	/>
		  </div>
		  <button
		  	type="submit"
		  	className="btn btn-primary"
		  	disabled={submitting || pristine || invalid}
	  	>
		  	Submit
	  	</button>
		</form>
	);
}

export default reduxForm({
  form: 'commentFormForUsers',
  onSubmitSuccess: (result, dispatch, props) => {
    props.reset();
  }
})(CommentFormForUsersContent);