import {connect} from 'react-redux';
import {compose} from 'redux';
import ArticleFormContent from './ArticleFormContent';
import {withForm} from '../../../hoc/withForm';
import {
  findItem,
  createItem,
  updateItem,
  clearForm
} from '../../../../actions/admin/FormActions';

const mapStateToProps = store => {
  const {
    currentArticleId,
    currentArticleTitle,
    currentArticleDescription,
    currentArticleContent,
    newArticleData
  } = store.articleForm;
  
  return {
    itemName: 'article',
    route: 'articles',
    updatingItemId: currentArticleId,
    newItemData: newArticleData,
    initialValues: {
      title: currentArticleTitle,
      description: currentArticleDescription,
      content: currentArticleContent
    }
  };
};

const mapDispatchToProps = dispatch => ({
  findItem:
    (id, route) => dispatch(findItem(id, route)),
  createItem:
    (data, route) => dispatch(createItem(data, route)),
  updateItem:
    (id, data, route) => dispatch(updateItem(id, data, route)),
  clearForm: () => dispatch(clearForm())
});

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  withForm
)(ArticleFormContent);