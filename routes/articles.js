const fs = require('fs');
const config = require('config');
const {
	imageUpload,
	uploadsAbsolutePath,
	cloudinary,
	deleteImageFromCloudinary
} = require('../helpers/imageUpload');
const pagination = require('../helpers/pagination');
const getCurrentDate = require('../helpers/getCurrentDate');
const mongoose = require('mongoose');
const auth = require('../middleware/auth');
const validateObjectId = require('../middleware/validateObjectId');
const validateInput = require('../middleware/validateInput');
const {
	Article,
	validateArticle,
	validateArticleUpdate
} = require('../models/Article');
const {Comment} = require('../models/Comment');
const {Category} = require('../models/Category');
const {Tag} = require('../models/Tag');
const {User} = require('../models/User');
const express = require('express');
const router = express.Router();

router.get('/page/:page/size/:pageSize', async (req, res) => {
	let {page, pageSize} = req.params;

	const paginate = await pagination(
		Article, page, pageSize);

	const {beginIndex, pages} = paginate;
	pageSize = paginate.pageSize;

	const articles = await Article
		.find()
		.skip(beginIndex)
		.limit(pageSize)
		.sort({_id: -1});

	res.send({articles, pages});
});

router.get('/author/:id/page/:page/size/:pageSize',
	validateObjectId, async (req, res) => {
		const userId = req.params.id;

		const user = await User.findById(userId);
		if (!user)
			return res.status(404).send('Cannot find user with given ID');

		const searchParam = {'user._id': userId};
		let {page, pageSize} = req.params;

		const paginate = await pagination(
			Article, page, pageSize, searchParam);

		const {beginIndex, pages} = paginate;
		pageSize = paginate.pageSize;

		const articles = await Article
			.find(searchParam)
			.skip(beginIndex)
			.limit(pageSize)
			.sort({_id: -1});

		res.send({articles, pages});
});

router.get('/category/:id/page/:page/size/:pageSize',
	validateObjectId, async (req, res) => {
		const categoryId = req.params.id;

		const category = await Category
			.findById(categoryId)
			.select('articles -_id');
			
		if (!category)
			return res.status(404).send('Cannot find category with the given ID.');

		const {articles: articlesIds} = category;
		const searchParam = {_id: {$in: articlesIds}};
		let {page, pageSize} = req.params;

		const paginate = await pagination(Article, page,
			pageSize, searchParam);

		const {beginIndex, pages} = paginate;
		pageSize = paginate.pageSize;

		const articles = await Article
			.find(searchParam)
			.skip(beginIndex)
			.limit(pageSize)
			.sort({_id: -1});
		
		res.send({articles, pages});
});

router.get('/tag/:id/page/:page/size/:pageSize',
	validateObjectId, async (req, res) => {
		const tagId = req.params.id;

		const tag = await Tag
			.findById(tagId)
			.select('articles -_id');

		if (!tag)
			return res.status(404).send('Cannot find tag with the given ID.');

		const {articles: articlesIds} = tag;
		const searchParam = {_id: {$in: articlesIds}};
		let {page, pageSize} = req.params;

		const paginate = await pagination(Article, page,
			pageSize, searchParam);

		const {beginIndex, pages} = paginate;
		pageSize = paginate.pageSize;

		const articles = await Article
			.find(searchParam)
			.skip(beginIndex)
			.limit(pageSize)
			.sort({_id: -1});
		
		res.send({articles, pages});
});

router.get('/:id', validateObjectId, async (req, res) => {
	const article = await Article
		.findById(req.params.id)
		.populate('comments.user', 'name photo');
		
	if (!article)
		return res.status(404).send('Cannot find the article with given ID');

	if (article.canIncreaseViews(req))
		await article.updateOne({$inc: {viewed: 1}});

	res.send(article);
});

router.get('/latest/:limit', async (req, res) => {
	const limit = Number(req.params.limit);
	if (isNaN(limit))
		return res.status(400).send('Invalid value.');

	const articles = await Article
		.find()
		.limit(limit)
		.sort({_id: -1});

	res.send(articles);
});

router.get('/search/:keyword', async (req, res) => {
	const keyword = new RegExp('.*'+req.params.keyword+'.*', 'i');
	const limit = 10;

	const articles = await Article
		.find()
		.or([
			{content: keyword},
			{title: keyword},
			{description: keyword}
		])
		.limit(limit);

	res.send(articles);
});

router.post('/', [auth, validateInput(validateArticle)],
	async (req, res) => {
		const {title, description, content} = req.body;

		const user = await User
			.findById(req.user._id)
			.select('name');

		const article = new Article({
			title,
			description,
			content,
			user: {
				_id: req.user._id,
				name: user.name
			}
		});
		await article.save();
		await user.updateOne({$inc: {articlesCount: 1}});

		res.send(article);
});

router.post('/uploadImage/:id', [auth, imageUpload],
	async (req, res) => {
		const uploaded = await cloudinary.uploader
			.upload(req.file.path, options = {use_filename: true});

		if (!uploaded)
			res.status(400).send('Cannon upload image to cloud');

		const oldArticleData = await Article.findByIdAndUpdate(
			req.params.id, {image: uploaded.url}, {new: false});

	  if (!oldArticleData)
			return res.status(404).send('Cannot update article');

		if (oldArticleData.image)
			await deleteImageFromCloudinary(oldArticleData.image);

		const {filename} = req.file;
		const tempImage = `${uploadsAbsolutePath}/${filename}`;
		
		await fs.stat(tempImage, async (err, stats) => {
	    if (err) {
        console.log('file not found');
	    } else {
        await fs.unlink(tempImage, err => {
				  if (err) throw err;
				  console.log(`file ${filename} was deleted`);
				});
	    }
		});

		res.send(oldArticleData);
});

router.put('/:id', [auth, validateObjectId,
	validateInput(validateArticleUpdate)], async (req, res) => {
		let article = await Article.findById(req.params.id);
		if (!article)
			return res.status(404).send('Cannot find the article with given ID');

		if (!article.checkAuthorization(req))
			return res.status(401).send('User is not authorized');

		const {title, description, content,
			category, tags, visible} = req.body;

		const articleFields = {};
		if (title)
			articleFields.title = title;
		if (description)
			articleFields.description = description;
		if (content)
			articleFields.content = content;
		if (category) {
			let newCategory = await article.setCategory(category);
			if (newCategory.status !== 200)
				return res.status(newCategory.status).send(newCategory.result);

			articleFields.category = newCategory.result;
		}
		if (tags && tags.length > 0) {
			let newTags = await article.setTags(tags);
			if (newTags.status !== 200)
				return res.status(newTags.status).send(newTags.result);

			articleFields.tags = newTags.result;
		}
		if (visible) {
			if (!req.user.isAdmin)
				return res.status(403).send('Operation is forbidden.');
			else articleFields.visible = !article.visible || false;
		}

		articleFields.last_update = getCurrentDate();

		article.set(articleFields);
		await article.save();

		res.send(article);
});

router.delete('/:id', [auth, validateObjectId], async (req, res) => {
	const article = await Article.findById(req.params.id);
	if (!article)
		return res.status(404).send('Cannot find the article with given ID');

	if (!article.checkAuthorization(req))
		return res.status(401).send('User is not authorized');

	const user = await User.findById(article.user._id);
	await user.updateOne({$inc: {articlesCount: -1}});

	await Comment.deleteMany({article: article._id});
	await article.delete();

	if (article.image)
		await deleteImageFromCloudinary(article.image);

	await Category.updateOne(
		{articles: article._id},
		{$pull: {articles: {$in: [article._id]}}}
	);

	await Tag.updateMany(
		{articles: article._id},
		{$pull: {articles: {$in: [article._id]}}}
	);

	res.send(article);
});

module.exports = router;