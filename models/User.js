const config = require('config');
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');
const Joi = require('@hapi/joi');

const validateUser = user => {
	const schema = Joi.object({
		name: Joi.string().min(5).max(50).required(),
		email: Joi.string().min(5).max(255).required().email(),
		password: Joi.string().min(5).max(1250).required()
	});
	return schema.validate(user);
};

const validateUserUpdate = user => {
	const schema = Joi.object({
		name: Joi.string().min(5).max(50).allow(null),
		email: Joi.string().min(5).max(255).email().allow(null),
		password: Joi.string().min(5).max(1250).allow(null),
		photo: Joi.object().allow(null)
	});
	return schema.validate(user);
};

const userSchema = new mongoose.Schema({
	name: {
		type: String,
		required: true,
		minlength: 5,
		maxlength: 50,
		trim: true
	},
	email: {
		type: String,
		required: true,
		minlength: 5,
		maxlength: 255,
		unique: true
	},
	password: {
		type: String,
		required: true,
		minlength: 5,
		maxlength: 1250
	},
	photo: {
		type: String,
		default: null
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	articlesCount: {
		type: Number,
		default: 0
	}
});

userSchema.methods.checkAuthorization = function(req) {
	return (!req.user.isAdmin && this._id.toHexString()
		!== req.user._id) ? false : true;
};

userSchema.methods.generateAuthToken = function() {
	const payload = {_id: this._id, isAdmin: this.isAdmin};

	return jwt.sign(payload, config.get('jwtPrivateKey'),
		{expiresIn: 3600000});
};

const User = new mongoose.model('User', userSchema);

exports.User = User;
exports.validateUser = validateUser;
exports.validateUserUpdate = validateUserUpdate;