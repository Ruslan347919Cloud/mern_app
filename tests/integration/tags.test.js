const {User} = require('../../models/User');
const {Article} = require('../../models/Article');
const {Tag} = require('../../models/Tag');
const request = require('supertest');
const mongoose = require('mongoose');

let server;

describe('/api/tags', () => {
	beforeEach(() => {server = require('../../server');});
	afterEach(async () => {
		await server.close();
		await Tag.deleteMany({});
	});

	describe('GET /all', () => {
		const exec = async () => {
			return await request(server)
				.get('/api/tags/all');
		};

		beforeEach(async () => {
			await Tag.collection.insertMany([
				{title: 'tag01'},
				{title: 'tag02'},
				{title: 'tag03'}
			]);
		});

		it('should return all tags', async () => {
			const res = await exec();

			expect(res.status).toBe(200);
			expect(res.body.length).toBe(3);
			expect(res.body.some(tag => tag.title === 'tag01'))
				.toBeTruthy();
			expect(res.body.some(tag => tag.title === 'tag02'))
				.toBeTruthy();
			expect(res.body.some(tag => tag.title === 'tag03'))
				.toBeTruthy();
		});

	});

	describe('GET /page/:page/size/:pageSize', () => {
		let page, pageSize;

		const exec = async () => {
			return await request(server)
				.get(`/api/tags/page/${page}/size/${pageSize}`);
		};

		beforeEach(async () => {
			await Tag.collection.insertMany([
				{title: 'tag1'},
				{title: 'tag2'},
				{title: 'tag3'},
				{title: 'tag4'},
				{title: 'tag5'},
				{title: 'tag6'},
				{title: 'tag7'},
				{title: 'tag8'}
			]);
			page = 2;
			pageSize = 3;
		});

		it('should return all tags for current page', async () => {
			const res = await exec();

			const {tags} = res.body;

			expect(res.status).toBe(200);
			expect(tags.length).toBe(3);
			expect(tags.some(tag => tag.title === 'tag4'))
				.toBeTruthy();
			expect(tags.some(tag => tag.title === 'tag5'))
				.toBeTruthy();
			expect(tags.some(tag => tag.title === 'tag6'))
				.toBeTruthy();
		});

		it('should return first page if given page is less than 1', async () => {
			page = 0;

			const res = await exec();

			const {tags} = res.body;

			expect(res.status).toBe(200);
			expect(tags.length).toBe(3);
			expect(tags.some(tag => tag.title === 'tag1'))
				.toBeTruthy();
			expect(tags.some(tag => tag.title === 'tag2'))
				.toBeTruthy();
			expect(tags.some(tag => tag.title === 'tag3'))
				.toBeTruthy();
		});

		it('should return last page if given page is more then max page', async () => {
			page = 4;

			const res = await exec();

			const {tags} = res.body;

			expect(res.status).toBe(200);
			expect(tags.length).toBe(2);
			expect(tags.some(tag => tag.title === 'tag7'))
				.toBeTruthy();
			expect(tags.some(tag => tag.title === 'tag8'))
				.toBeTruthy();
		});

		it('should return information about pages', async () => {
			const res = await exec();

			const {pages} = res.body;

			expect(res.status).toBe(200);
			expect(pages.length).toBe(3);
			expect(pages.some(page => {
				page.number === 1 && page.active === false
			}));
			expect(pages.some(page => {
				page.number === 2 && page.active === true
			}));
			expect(pages.some(page => {
				page.number === 3 && page.active === false
			}));
		});
	});

	describe('GET /:id', () => {
		let tagId;
		const exec = async () => await request(server)
			.get(`/api/tags/${tagId}`);

		it('should return a single tag if valid ID is passed', async () => {
			const tag = new Tag({title: 'aaaaa'});
			await tag.save();

			tagId = tag._id;

			const res = await exec();

			expect(res.status).toBe(200);
			expect(res.body).toHaveProperty('title', tag.title);
		});

		it('should return 404 if invalid ID is passed', async () => {
			tagId = '1';

			const res = await exec();

			expect(res.status).toBe(404);
		});

		it('should return 404 if no tag with the given ID exists', async () => {
			tagId = mongoose.Types.ObjectId();

			const res = await exec();

			expect(res.status).toBe(404);
		});
	});

	describe('POST /', () => {
		let token;
		let title;

		const exec = async () => {
			return await request(server)
				.post('/api/tags')
				.set('x-auth-token', token)
				.send({title});
		}

		beforeEach(async () => {
			title = '12345';
			token = new User({isAdmin: true}).generateAuthToken();
		});

		it('should return 401 if client is not logged in', async () => {
			token = '';
			
			const res = await exec();

			expect(res.status).toBe(401);
		});

		it('should return 403 if user is not admin', async () => {
			token = new User({isAdmin: false}).generateAuthToken();
			
			const res = await exec();

			expect(res.status).toBe(403);
		});

		it('should return 400 if title is less than 5 characters', async () => {
			title = '1234';

			const res = await exec();

			expect(res.status).toBe(400);
		});

		it('should return 400 if title is more than 50 characters', async () => {
			title = new Array(52).join('a');

			const res = await exec();

			expect(res.status).toBe(400);
		});

		it('should save the tag if it is valid', async () => {
			const res = await exec();

			const tag = await Tag.findById(res.body._id);
			
			expect(tag).not.toBeNull();
		});

		it('should return the tag if it is valid', async () => {
			const res = await exec();

			expect(res.body).toHaveProperty('_id');
			expect(res.body).toHaveProperty('title', '12345');
		});
	});

	describe('PUT /:id', () => {
		let token, title, tag, tagId, article1, article2, oldTag;

		const exec = async () => {
			return await request(server)
				.put(`/api/tags/${tagId}`)
				.set('x-auth-token', token)
				.send({title});
		};

		beforeEach(async () => {
			token = new User({isAdmin: true}).generateAuthToken();
			tag = new Tag({title: '12345'});
			oldTag = new Tag({title: 'old tag'});
			await tag.save();
			await oldTag.save();
			tagId = tag._id;
			article1 = new Article({
				_id: mongoose.Types.ObjectId(),
				title: '12345',
				content: '12345',
				user: mongoose.Types.ObjectId(),
				tags: [
					{
						_id: tag._id,
						title: tag.title
					}, {
						_id: oldTag._id,
						title: oldTag.title
					}
				],
				category: null
			});
			article2 = new Article({
				_id: mongoose.Types.ObjectId(),
				title: '54321',
				content: '54321',
				user: mongoose.Types.ObjectId(),
				tags: [
					{
						_id: tag._id,
						title: tag.title
					}, {
						_id: oldTag._id,
						title: oldTag.title
					}
				],
				category: null
			});
			await article1.save();
			await article2.save();
			title = '54321';
		});

		afterEach(async () => {
			await Article.deleteMany({});
		});

		it('should return 401 if client is not logged in', async () => {
			token = '';
			
			const res = await exec();

			expect(res.status).toBe(401);
		});

		it('should return 403 if user is not admin', async () => {
			token = new User({isAdmin: false}).generateAuthToken();
			
			const res = await exec();

			expect(res.status).toBe(403);
		});

		it('should return 404 if invalid id is passed', async () => {
			tagId = 1;

			const res = await exec();

			expect(res.status).toBe(404);
		});

		it('should return 404 if no tag with the given id exists', async () => {
			tagId = mongoose.Types.ObjectId();

			const res = await exec();

			expect(res.status).toBe(404);
		});

		it('should return 400 if title is less than 5 characters', async () => {
			title = '1234';

			const res = await exec();

			expect(res.status).toBe(400);
		});

		it('should return 400 if title is more than 50 characters', async () => {
			title = new Array(52).join('a');

			const res = await exec();

			expect(res.status).toBe(400);
		});

		it('should update tag in db and if id is valid', async () => {
			const res = await exec();

			tag = await Tag.findById(res.body._id);

			expect(res.status).toBe(200);
			expect(tag.title).toBe('54321');
		});

		it('should update tag title in related articles', async () => {
			const res = await exec();

			const articles = await Article
				.find({tags: {
					$elemMatch: {_id: tag._id}
				}});

			expect(articles.length).toBe(2);
			expect(articles[0].tags)
				.toContainEqual(expect.objectContaining({title: '54321'}));
			expect(articles[0].tags)
				.toContainEqual(expect.objectContaining({title: 'old tag'}));
			expect(articles[1].tags)
				.toContainEqual(expect.objectContaining({title: '54321'}));
			expect(articles[1].tags)
				.toContainEqual(expect.objectContaining({title: 'old tag'}));
		});

		it('should return the tag if it is valid', async () => {
			const res = await exec();

			expect(res.body).toHaveProperty('_id', tag._id.toHexString());
			expect(res.body).toHaveProperty('title', '54321');
		});
	});

	describe('DELETE /:id', () => {
		let token, tag, tagId;

		const exec = async () => {
			return await request(server)
				.delete(`/api/tags/${tagId}`)
				.set('x-auth-token', token);
		};

		beforeEach(async () => {
			token = new User({isAdmin: true}).generateAuthToken();

			tag = new Tag({title: '12345'});
			await tag.save();
			tagId = tag._id;

			await Article.collection.insertMany([
				{title: 'article1', content: '12345', tags: [{_id: tagId}]},
				{title: 'article2', content: '12345', tags: [{_id: tagId}]},
				{title: 'article3', content: '12345', tags: [{_id: tagId}]}
			]);
		});

		afterEach(async () => {
			await Article.deleteMany({});
		});

		it('should return 401 if client is not logged in', async () => {
			token = '';

			const res = await exec();

			expect(res.status).toBe(401); 
		});

		it('should return 403 if user is not admin', async () => {
			token = new User({isAdmin: false}).generateAuthToken();
			
			const res = await exec();

			expect(res.status).toBe(403);
		});

		it('should return 404 if invalid id is passed', async () => {
			tagId = '1';

			const res = await exec();

			expect(res.status).toBe(404);
		});

		it('should return 404 if no tag with the given id exists', async () => {
			tagId = mongoose.Types.ObjectId();

			const res = await exec();

			expect(res.status).toBe(404);
		});

		it('should delete the tag from db if id is valid', async () => {
			const res = await exec();

			tag = await Tag.findById(res.body._id);

			expect(res.status).toBe(200);
			expect(tag).toBeNull();
		});

		it('should return the removed tag', async () => {
      const res = await exec();

      expect(res.body).toHaveProperty('_id', tag._id.toHexString());
      expect(res.body).toHaveProperty('title', tag.title);
    });

    it('should remove deleted tag from articles', async () => {
      const res = await exec();

      const articles = await Article
				.find({tags: {
					$elemMatch: {_id: tagId}
				}});

    	expect(res.status).toBe(200);
      expect(articles.length).toBe(0);
    });
	});

});