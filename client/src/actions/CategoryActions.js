import axios from 'axios';
import {setAlert} from './AlertActions';
import {
	FETCH_SINGLE_CATEGORY,
	FETCH_ARTICLES_BY_CATEGORY
} from './Types';

export const fetchSingleCategory = categoryId => async dispatch => {
	try {
		const res = await axios.get(`/api/categories/${categoryId}`);
		
		dispatch({
			type: FETCH_SINGLE_CATEGORY,
			payload: res.data
		});
	} catch (err) {
		const {data: error, status, statusText} = err.response;
		
		console.log(statusText, status);
		console.log(error);
		
		if (error)
			dispatch(setAlert(error, 'danger'));
	}
};

export const fetchArticlesByCategory = (categoryId, page, size) => async dispatch => {
	try {
		const res = await axios.get(
			`/api/articles/category/${categoryId}/page/${page}/size/${size}`);
		
		dispatch({
			type: FETCH_ARTICLES_BY_CATEGORY,
			payload: res.data
		});
	} catch (err) {
		const {data: error, status, statusText} = err.response;

		console.log(statusText, status);
		console.log(error);
	}
};