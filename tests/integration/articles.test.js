const bcrypt = require('bcrypt');
const {User} = require('../../models/User');
const {Article} = require('../../models/Article');
const {Comment} = require('../../models/Comment');
const {Category} = require('../../models/Category');
const {Tag} = require('../../models/Tag');
const request = require('supertest');
const mongoose = require('mongoose');

let server;

describe('/api/articles', () => {
	beforeEach(() => {server = require('../../server');});
	afterEach(async () => {
		await Article.deleteMany({});
		await server.close();
	});

	describe('GET /page/:page/size/:pageSize', () => {
		let page, pageSize;

		const exec = async () => {
			return await request(server)
				.get(`/api/articles/page/${page}/size/${pageSize}`);
		};

		beforeEach(async () => {
			await Article.collection.insertMany([
				{title: 'article1', content: '12345'},
				{title: 'article2', content: '12345'},
				{title: 'article3', content: '12345'},
				{title: 'article4', content: '12345'},
				{title: 'article5', content: '12345'},
				{title: 'article6', content: '12345'},
				{title: 'article7', content: '12345'},
				{title: 'article8', content: '12345'}
			]);
			page = 2;
			pageSize = 3;
		});

		it('should return all articles for current page', async () => {
			const res = await exec();

			const {articles} = res.body;

			expect(res.status).toBe(200);
			expect(articles.length).toBe(3);
			expect(articles.some(article => article.title === 'article5'))
				.toBeTruthy();
			expect(articles.some(article => article.title === 'article4'))
				.toBeTruthy();
			expect(articles.some(article => article.title === 'article3'))
				.toBeTruthy();
		});

		it('should return first page if given page is less than 1', async () => {
			page = 0;

			const res = await exec();

			const {articles} = res.body;

			expect(res.status).toBe(200);
			expect(articles.length).toBe(3);
			expect(articles.some(article => article.title === 'article8'))
				.toBeTruthy();
			expect(articles.some(article => article.title === 'article7'))
				.toBeTruthy();
			expect(articles.some(article => article.title === 'article6'))
				.toBeTruthy();
		});

		it('should return last page if given page is more then max page', async () => {
			page = 4;

			const res = await exec();

			const {articles} = res.body;

			expect(res.status).toBe(200);
			expect(articles.length).toBe(2);
			expect(articles.some(article => article.title === 'article2'))
				.toBeTruthy();
			expect(articles.some(article => article.title === 'article1'))
				.toBeTruthy();
		});

		it('should return information about pages', async () => {
			const res = await exec();

			const {pages} = res.body;

			expect(res.status).toBe(200);
			expect(pages.length).toBe(3);
			expect(pages.some(page => {
				page.number === 1 && page.active === false
			}));
			expect(pages.some(page => {
				page.number === 2 && page.active === true
			}));
			expect(pages.some(page => {
				page.number === 3 && page.active === false
			}));
		});
	});

	describe('GET /author/:id/page/:page/size/:pageSize', () => {
		let page, pageSize, user, userId;

		const exec = async () => {
			return await request(server)
				.get(`/api/articles/author/${userId}/page/${page}/size/${pageSize}`);
		};

		beforeEach(async () => {
			user = new User({
				_id: mongoose.Types.ObjectId(),
				name: 'user1',
				email: 'user1@mail.com',
				password: '12345'
			});
			await user.save();

			userId = user._id.toHexString();

			await Article.collection.insertMany([
				{title: 'article01', content: '12345', user: {_id: userId}},
				{title: 'article02', content: '12345', user: {_id: mongoose.Types.ObjectId()}},
				{title: 'article03', content: '12345', user: {_id: userId}},
				{title: 'article04', content: '12345', user: {_id: mongoose.Types.ObjectId()}},
				{title: 'article05', content: '12345', user: {_id: userId}},
				{title: 'article06', content: '12345', user: {_id: mongoose.Types.ObjectId()}},
				{title: 'article07', content: '12345', user: {_id: userId}},
				{title: 'article08', content: '12345', user: {_id: mongoose.Types.ObjectId()}},
				{title: 'article09', content: '12345', user: {_id: userId}},
				{title: 'article10', content: '12345', user: {_id: mongoose.Types.ObjectId()}},
				{title: 'article11', content: '12345', user: {_id: userId}},
				{title: 'article12', content: '12345', user: {_id: mongoose.Types.ObjectId()}},
				{title: 'article13', content: '12345', user: {_id: userId}},
				{title: 'article14', content: '12345', user: {_id: mongoose.Types.ObjectId()}},
				{title: 'article15', content: '12345', user: {_id: userId}}
			]);

			page = 2;
			pageSize = 3;
		});

		afterEach(async () => {
			await User.deleteMany({});
		});

		it('should return 404 if invalid ID is passed', async () => {
			userId = '1';

			const res = await exec();

			expect(res.status).toBe(404);
		});

		it('should return 404 if user with given ID does not exists', async () => {
			userId = mongoose.Types.ObjectId();

			const res = await exec();

			expect(res.status).toBe(404);
		});

		it('should return all articles of given user on current page', async () => {
			const res = await exec();

			const {articles} = res.body;

			expect(res.status).toBe(200);
			expect(articles.length).toBe(3);
			expect(articles.some(article => article.title === 'article09'))
				.toBeTruthy();
			expect(articles.some(article => article.title === 'article07'))
				.toBeTruthy();
			expect(articles.some(article => article.title === 'article05'))
				.toBeTruthy();
		});

		it('should return first page if given page is less than 1', async () => {
			page = 0;

			const res = await exec();

			const {articles} = res.body;

			expect(res.status).toBe(200);
			expect(articles.length).toBe(3);
			expect(articles.some(article => article.title === 'article15'))
				.toBeTruthy();
			expect(articles.some(article => article.title === 'article13'))
				.toBeTruthy();
			expect(articles.some(article => article.title === 'article11'))
				.toBeTruthy();
		});

		it('should return last page if given page is more then max page', async () => {
			page = 4;

			const res = await exec();

			const {articles} = res.body;

			expect(res.status).toBe(200);
			expect(articles.length).toBe(2);
			expect(articles.some(article => article.title === 'article03'))
				.toBeTruthy();
			expect(articles.some(article => article.title === 'article01'))
				.toBeTruthy();
		});

		it('should return information about pages', async () => {
			const res = await exec();

			const {pages} = res.body;

			expect(res.status).toBe(200);
			expect(pages.length).toBe(3);
			expect(pages.some(page => {
				page.number === 1 && page.active === false
			}));
			expect(pages.some(page => {
				page.number === 2 && page.active === true
			}));
			expect(pages.some(page => {
				page.number === 3 && page.active === false
			}));
		});
	});

	describe('GET /category/:id/page/:page/size/:pageSize', () => {
		let page, pageSize, category, categoryId;

		const articlesIds = [];
		for (let i = 0; i < 15; i++)
			articlesIds.push(mongoose.Types.ObjectId());

		const exec = async () => {
			return await request(server)
				.get(`/api/articles/category/${categoryId}/page/${page}/size/${pageSize}`);
		};

		beforeEach(async () => {
			category = new Category({
				_id: mongoose.Types.ObjectId(),
				title: 'category1',
				articles: [
					articlesIds[0],
					articlesIds[2],
					articlesIds[4],
					articlesIds[6],
					articlesIds[8],
					articlesIds[10],
					articlesIds[12],
					articlesIds[14]
				]
			});
			await category.save();

			categoryId = category._id;

			await Article.collection.insertMany([
				{_id: articlesIds[0], title: 'article01', category: {_id: categoryId}},
				{_id: articlesIds[1], title: 'article02'},
				{_id: articlesIds[2], title: 'article03', category: {_id: categoryId}},
				{_id: articlesIds[3], title: 'article04'},
				{_id: articlesIds[4], title: 'article05', category: {_id: categoryId}},
				{_id: articlesIds[5], title: 'article06'},
				{_id: articlesIds[6], title: 'article07', category: {_id: categoryId}},
				{_id: articlesIds[7], title: 'article08'},
				{_id: articlesIds[8], title: 'article09', category: {_id: categoryId}},
				{_id: articlesIds[9], title: 'article10'},
				{_id: articlesIds[10], title: 'article11', category: {_id: categoryId}},
				{_id: articlesIds[11], title: 'article12'},
				{_id: articlesIds[12], title: 'article13', category: {_id: categoryId}},
				{_id: articlesIds[13], title: 'article14'},
				{_id: articlesIds[14], title: 'article15', category: {_id: categoryId}},
			]);

			page = 2;
			pageSize = 3;
		});

		afterEach(async () => {
			await Category.deleteMany({});
		});

		it('should return 404 if invalid ID is passed', async () => {
			categoryId = '1';

			const res = await exec();

			expect(res.status).toBe(404);
		});

		it('should return 404 if category with given ID does not exists', async () => {
			categoryId = mongoose.Types.ObjectId();

			const res = await exec();

			expect(res.status).toBe(404);
		});

		it('should return all articles with given category on current page', async () => {
			const res = await exec();

			const {articles} = res.body;

			expect(res.status).toBe(200);
			expect(articles.length).toBe(3);
			expect(articles.some(article => article.title === 'article09'))
				.toBeTruthy();
			expect(articles.some(article => article.title === 'article07'))
				.toBeTruthy();
			expect(articles.some(article => article.title === 'article05'))
				.toBeTruthy();
		});

		it('should return first page if given page is less than 1', async () => {
			page = 0;

			const res = await exec();

			const {articles} = res.body;

			expect(res.status).toBe(200);
			expect(articles.length).toBe(3);
			expect(articles.some(article => article.title === 'article15'))
				.toBeTruthy();
			expect(articles.some(article => article.title === 'article13'))
				.toBeTruthy();
			expect(articles.some(article => article.title === 'article11'))
				.toBeTruthy();
		});

		it('should return last page if given page is more then max page', async () => {
			page = 4;

			const res = await exec();

			const {articles} = res.body;

			expect(res.status).toBe(200);
			expect(articles.length).toBe(2);
			expect(articles.some(article => article.title === 'article03'))
				.toBeTruthy();
			expect(articles.some(article => article.title === 'article01'))
				.toBeTruthy();
		});

		it('should return information about pages', async () => {
			const res = await exec();

			const {pages} = res.body;

			expect(res.status).toBe(200);
			expect(pages.length).toBe(3);
			expect(pages.some(page => {
				page.number === 1 && page.active === false
			}));
			expect(pages.some(page => {
				page.number === 2 && page.active === true
			}));
			expect(pages.some(page => {
				page.number === 3 && page.active === false
			}));
		});
	});

	describe('GET /tag/:id/page/:page/size/:pageSize', () => {
		let page, pageSize, tag, tagId;

		const articlesIds = [];
		for (let i = 0; i < 15; i++)
			articlesIds.push(mongoose.Types.ObjectId());

		const exec = async () => {
			return await request(server)
				.get(`/api/articles/tag/${tagId}/page/${page}/size/${pageSize}`);
		};

		beforeEach(async () => {
			tag = new Tag({
				_id: mongoose.Types.ObjectId(),
				title: 'tag01',
				articles: [
					articlesIds[0],
					articlesIds[2],
					articlesIds[4],
					articlesIds[6],
					articlesIds[8],
					articlesIds[10],
					articlesIds[12],
					articlesIds[14]
				]
			});
			await tag.save();

			tagId = tag._id;

			await Article.collection.insertMany([
				{_id: articlesIds[0], title: 'article01', tags: [{_id: tagId}]},
				{_id: articlesIds[1], title: 'article02'},
				{_id: articlesIds[2], title: 'article03', tags: [{_id: tagId}]},
				{_id: articlesIds[3], title: 'article04'},
				{_id: articlesIds[4], title: 'article05', tags: [{_id: tagId}]},
				{_id: articlesIds[5], title: 'article06'},
				{_id: articlesIds[6], title: 'article07', tags: [{_id: tagId}]},
				{_id: articlesIds[7], title: 'article08'},
				{_id: articlesIds[8], title: 'article09', tags: [{_id: tagId}]},
				{_id: articlesIds[9], title: 'article10'},
				{_id: articlesIds[10], title: 'article11', tags: [{_id: tagId}]},
				{_id: articlesIds[11], title: 'article12'},
				{_id: articlesIds[12], title: 'article13', tags: [{_id: tagId}]},
				{_id: articlesIds[13], title: 'article14'},
				{_id: articlesIds[14], title: 'article15', tags: [{_id: tagId}]},
			]);

			page = 2;
			pageSize = 3;
		});

		afterEach(async () => {
			await Tag.deleteMany({});
		});

		it('should return 404 if invalid ID is passed', async () => {
			tagId = '1';

			const res = await exec();

			expect(res.status).toBe(404);
		});

		it('should return 404 if tag with given ID does not exists', async () => {
			tagId = mongoose.Types.ObjectId();

			const res = await exec();

			expect(res.status).toBe(404);
		});

		it('should return all articles with given tag on current page', async () => {
			const res = await exec();

			const {articles} = res.body;

			expect(res.status).toBe(200);
			expect(articles.length).toBe(3);
			expect(articles.some(article => article.title === 'article09'))
				.toBeTruthy();
			expect(articles.some(article => article.title === 'article07'))
				.toBeTruthy();
			expect(articles.some(article => article.title === 'article05'))
				.toBeTruthy();
		});

		it('should return first page if given page is less than 1', async () => {
			page = 0;

			const res = await exec();

			const {articles} = res.body;

			expect(res.status).toBe(200);
			expect(articles.length).toBe(3);
			expect(articles.some(article => article.title === 'article15'))
				.toBeTruthy();
			expect(articles.some(article => article.title === 'article13'))
				.toBeTruthy();
			expect(articles.some(article => article.title === 'article11'))
				.toBeTruthy();
		});

		it('should return last page if given page is more then max page', async () => {
			page = 4;

			const res = await exec();

			const {articles} = res.body;

			expect(res.status).toBe(200);
			expect(articles.length).toBe(2);
			expect(articles.some(article => article.title === 'article03'))
				.toBeTruthy();
			expect(articles.some(article => article.title === 'article01'))
				.toBeTruthy();
		});

		it('should return information about pages', async () => {
			const res = await exec();

			const {pages} = res.body;

			expect(res.status).toBe(200);
			expect(pages.length).toBe(3);
			expect(pages.some(page => {
				page.number === 1 && page.active === false
			}));
			expect(pages.some(page => {
				page.number === 2 && page.active === true
			}));
			expect(pages.some(page => {
				page.number === 3 && page.active === false
			}));
		});
	});

	describe('GET /:id', () => {
		let article, articleId;

		const exec = async () => await request(server)
			.get(`/api/articles/${articleId}`);

		beforeEach(async () => {
			article = new Article({
				title: '12345',
				content: '12345'
			});
			await article.save();

			articleId = article._id;
		});

		it('should return a single article if valid ID is passed', async () => {
			const res = await exec();

			expect(res.status).toBe(200);
			expect(res.body).toHaveProperty('title', article.title);
		});

		it('should increase the viewed counter if valid ID is passed', async () => {
			const res = await exec();

			article = await Article.findById(articleId);

			expect(res.status).toBe(200);
			expect(article).toHaveProperty('viewed', 1);
		});

		it('should return 404 if invalid ID is passed', async () => {
			articleId = '1';

			const res = await exec();

			expect(res.status).toBe(404);
		});

		it('should return 404 if no article with the given ID exists', async () => {
			articleId = mongoose.Types.ObjectId();

			const res = await exec();

			expect(res.status).toBe(404);
		});
	});

	describe('GET /latest/:limit', () => {
		let limit;

		const exec = async () => await request(server)
			.get(`/api/articles/latest/${limit}`);

		beforeEach(async () => {
			await Article.collection.insertMany([
				{title: 'article01', content: '12345'},
				{title: 'article02', content: '12345'},
				{title: 'article03', content: '12345'},
				{title: 'article04', content: '12345'}
			]);

			limit = 3;
		});

		it('should return 400 if invalid data is passed', async () => {
			limit = 'aaa';

			const res = await exec();
			
			expect(res.status).toBe(400);
		});

		it('should return required number of articles', async () => {
			const res = await exec();
			
			expect(res.status).toBe(200);
			expect(res.body.length).toBe(3);
			expect(res.body.some(article => article.title === 'article04'))
				.toBeTruthy();
			expect(res.body.some(article => article.title === 'article03'))
				.toBeTruthy();
			expect(res.body.some(article => article.title === 'article02'))
				.toBeTruthy();
		});
	});

	describe('GET /search/:keyword', () => {
		let keyword;

		const exec = async () => {
			return await request(server)
				.get(`/api/articles/search/${keyword}`);
		};

		beforeEach(async () => {
			await Article.collection.insertMany([
				{title: 'articleKeyword01', content: '12345', description: '12345'},
				{title: 'article02', content: '12keYword345', description: '12345'},
				{title: 'article03', content: '12345', description: '12345'}
			]);

			keyword = 'keyword';
		});

		it('should return articles with keywords', async () => {
			const res = await exec();

			expect(res.status).toBe(200);
			expect(res.body.length).toBe(2);
			expect(res.body.some(article =>
				article.title === 'articleKeyword01')).toBeTruthy();
			expect(res.body.some(article =>
				article.content === '12keYword345')).toBeTruthy();
		});
	});

	describe('POST /', () => {
		let token, title, content, description, user, salt;

		const exec = async () => {
			return await request(server)
				.post('/api/articles')
				.set('x-auth-token', token)
				.send({title, content, description});
		};

		beforeEach(async () => {
			title = '12345';
			content = '12345';
			description = '12345';

			user = new User({
				_id: mongoose.Types.ObjectId(),
				name: 'user1',
				email: 'user1@mail.com',
				password: '12345'
			});
			salt = await bcrypt.genSalt(10);
			user.password = await bcrypt.hash(user.password, salt);
			await user.save();

			userId = user._id;

			token = user.generateAuthToken();
		});

		afterEach(async () => {
			await User.deleteMany({});
		});

		it('should return 401 if client is not logged in', async () => {
			token = '';
			
			const res = await exec();

			expect(res.status).toBe(401);
		});

		it('should return 400 if title is less than 5 characters', async () => {
			title = '1234';

			const res = await exec();

			expect(res.status).toBe(400);
		});

		it('should return 400 if content is less than 5 characters', async () => {
			content = '1234';

			const res = await exec();

			expect(res.status).toBe(400);
		});

		it('should return 400 if description is less than 5 characters', async () => {
			description = '1234';

			const res = await exec();

			expect(res.status).toBe(400);
		});

		it('should return 400 if title is more than 255 characters', async () => {
			title = new Array(257).join('a');

			const res = await exec();

			expect(res.status).toBe(400);
		});

		it('should return 400 if content is more than 10000 characters', async () => {
			content = new Array(10002).join('a');

			const res = await exec();

			expect(res.status).toBe(400);
		});

		it('should return 400 if description is more than 255 characters', async () => {
			description = new Array(257).join('a');

			const res = await exec();

			expect(res.status).toBe(400);
		});

		it('should save the article if it is valid', async () => {
			const res = await exec();

			const article = await Article.findById(res.body._id);
			
			expect(article).not.toBeNull();
		});

		it('should return the article if it is valid', async () => {
			const res = await exec();

			expect(res.body).toHaveProperty('_id');
			expect(res.body).toHaveProperty('title', '12345');
			expect(res.body).toHaveProperty('content', '12345');
			expect(res.body).toHaveProperty('description', '12345');
		});

		it('should allow to save data if description is not defined', async () => {
			description = null;

			const res = await exec();

			expect(res.body).toHaveProperty('_id');
			expect(res.body).toHaveProperty('title', '12345');
			expect(res.body).toHaveProperty('content', '12345');
			expect(res.body).toHaveProperty('description', null);
		});

		it('should increment articlesCount for current user', async () => {
			const res = await exec();

			user = await User.findById(user._id);

			expect(res.status).toBe(200);
			expect(user).toHaveProperty('articlesCount', 1);
		});
	});

	describe('PUT /:id', () => {
		let token, user, article, articleId, title, content,
			description, category, newCategory, oldCategory, tags,
				oldTag, newTag1, newTag2, article2, article2Id, visible;

		const exec = async () => {
			return await request(server)
				.put(`/api/articles/${articleId}`)
				.set('x-auth-token', token)
				.send({title, content, description,
					category, tags, visible});
		};

		beforeEach(async () => {
			user = new User({
				_id: mongoose.Types.ObjectId(),
				name: 'User1',
				isAdmin: true
			});

			token = user.generateAuthToken();

			articleId = mongoose.Types.ObjectId();
			article2Id = mongoose.Types.ObjectId();

			newCategory = new Category({
				_id: mongoose.Types.ObjectId(),
				title: 'category1',
				articles: []
			});

			oldCategory = new Category({
				_id: mongoose.Types.ObjectId(),
				title: 'category2',
				articles: [articleId]
			});

			oldTag = new Tag({
				_id: mongoose.Types.ObjectId(),
				title: 'tag01',
				articles: [articleId, article2Id]
			});

			newTag1 = new Tag({
				_id: mongoose.Types.ObjectId(),
				title: 'tag02',
				articles: []
			});

			newTag2 = new Tag({
				_id: mongoose.Types.ObjectId(),
				title: 'tag03',
				articles: []
			});

			article = new Article({
				_id: articleId,
				user: {
					_id: user._id,
					name: user.name
				},
				description: '12345',
				title: '12345',
				content: '12345',
				comments: [],
				tags: [oldTag],
				category: oldCategory
			});

			article2 = new Article({
				_id: article2Id,
				user: {
					_id: user._id,
					name: user.name
				},
				description: '54321',
				title: '54321',
				content: '54321',
				comments: [],
				tags: [oldTag],
				category: null
			});

			await oldTag.save();
			await newTag1.save();
			await newTag2.save();
			await article.save();
			await article2.save();
			await newCategory.save();
			await oldCategory.save();

			title = '54321';
			content = '54321';
			description = '54321';
			visible = true;
			tags = [newTag1._id, newTag2._id];
			category = newCategory._id;
		});

		afterEach(async () => {
			await Category.deleteMany({});
			await Tag.deleteMany({});
		});

		it('should return 401 if client is not logged in', async () => {
			token = '';

			const res = await exec();

			expect(res.status).toBe(401); 
		});

		it('should return 404 if invalid ID is passed', async () => {
			articleId = '1';

			const res = await exec();

			expect(res.status).toBe(404);
		});

		it('should return 404 if no article with the given ID exists', async () => {
			articleId = mongoose.Types.ObjectId();

			const res = await exec();

			expect(res.status).toBe(404);
		});

		it('should return 401 if user is not authorized', async () => {
			user = new User({
				_id: mongoose.Types.ObjectId(),
				isAdmin: false
			});

			token = user.generateAuthToken();
			
			const res = await exec();

			expect(res.status).toBe(401);
		});

		it('should return 400 if title is less than 5 characters', async () => {
			title = '1234';

			const res = await exec();

			expect(res.status).toBe(400);
		});

		it('should return 400 if content is less than 5 characters', async () => {
			content = '1234';

			const res = await exec();

			expect(res.status).toBe(400);
		});

		it('should return 400 if description is less than 5 characters', async () => {
			description = '1234';

			const res = await exec();

			expect(res.status).toBe(400);
		});

		it('should return 400 if title is more than 255 characters', async () => {
			title = new Array(257).join('a');

			const res = await exec();

			expect(res.status).toBe(400);
		});

		it('should return 400 if content is more than 10000 characters', async () => {
			content = new Array(10002).join('a');

			const res = await exec();

			expect(res.status).toBe(400);
		});

		it('should return 400 if description is more than 255 characters', async () => {
			description = new Array(257).join('a');

			const res = await exec();

			expect(res.status).toBe(400);
		});

		it('should return 404 if category ID is invalid', async () => {
			category = '1';

			const res = await exec();

			expect(res.status).toBe(404);
		});

		it('should return 404 if no category with given ID exists', async () => {
			category = mongoose.Types.ObjectId();

			const res = await exec();

			expect(res.status).toBe(404);
		});

		it('should return 404 if tag ID is invalid', async () => {
			tags = ['1'];

			const res = await exec();

			expect(res.status).toBe(404);
		});

		it('should return 404 if no tag with given ID exists', async () => {
			tags = [mongoose.Types.ObjectId()];

			const res = await exec();

			expect(res.status).toBe(404);
		});

		it('should update the article with category and tags if given data is valid', async () => {
			const res = await exec();

			article = await Article.findById(articleId);

			newCategory = await Category.findById(category);

			newTag1 = await Tag.findById(newTag1._id);
			newTag2 = await Tag.findById(newTag2._id);
			
			expect(res.status).toBe(200);
			expect(article).toHaveProperty('title', '54321');
			expect(article).toHaveProperty('content', '54321');
			expect(article).toHaveProperty('description', '54321');
			expect(article).toHaveProperty('visible', true);
			expect(article.last_update).not.toEqual(article.put_date);
			expect(article.category).toHaveProperty('_id', category);
			expect(article.tags)
				.toContainEqual(expect.objectContaining({title: 'tag02'}));
			expect(article.tags)
				.toContainEqual(expect.objectContaining({_id: newTag1._id}));
			expect(article.tags)
				.toContainEqual(expect.objectContaining({title: 'tag03'}));
			expect(article.tags)
				.toContainEqual(expect.objectContaining({_id: newTag2._id}));
			expect(newCategory.articles).toContainEqual(article._id);
			expect(newTag1.articles).toContainEqual(article._id);
			expect(newTag2.articles).toContainEqual(article._id);
		});

		it('should allow to set only visibility', async () => {
			title = null;
			content = null;
			description = null;
			category = null;
			tags = [];

			const res = await exec();

			article = await Article.findById(articleId);

			expect(res.status).toBe(200);
			expect(article).toHaveProperty('title', '12345');
			expect(article).toHaveProperty('content', '12345');
			expect(article).toHaveProperty('description', '12345');
			expect(article).toHaveProperty('visible', true);
		});

		it('should allow to set only title', async () => {
			visible = null;
			content = null;
			description = null;
			category = null;
			tags = [];

			const res = await exec();
			
			article = await Article.findById(articleId);

			expect(res.status).toBe(200);
			expect(article).toHaveProperty('title', '54321');
			expect(article).toHaveProperty('content', '12345');
			expect(article).toHaveProperty('description', '12345');
			expect(article).toHaveProperty('visible', false);
		});

		/*it('should not allow to set visibility if user is not admin', async () => {
			user.isAdmin = false;

			token = user.generateAuthToken();

			const res = await exec();

			expect(res.status).toBe(403);
		});*/

		it('should remove the article from old category', async () => {
			const res = await exec();

			article = await Article.findById(articleId);

			oldCategory = await Category.findById(oldCategory._id);

			expect(res.status).toBe(200);
			expect(article.category).not.toHaveProperty('_id', oldCategory._id);
			expect(oldCategory.articles).not.toContainEqual(article._id);
		});

		it('should remove the article from old tag', async () => {
			const res = await exec();

			article = await Article.findById(articleId);

			oldTag = await Tag.findById(oldTag._id);

			expect(res.status).toBe(200);
			expect(article.tags)
				.not.toContainEqual(expect.objectContaining({_id: oldTag._id}));
			expect(oldTag.articles).not.toContainEqual(article._id);
			expect(oldTag.articles).toContainEqual(article2Id);
		});

	});

	describe('DELETE /:id', () => {
		let user, token, article, articleId, comment1,
			comment2, category, categoryId, tag1, tag2, salt;

		const exec = async () => {
			return await request(server)
				.delete(`/api/articles/${articleId}`)
				.set('x-auth-token', token);
		};

		beforeEach(async () => {
			user = new User({
				_id: mongoose.Types.ObjectId(),
				name: 'User01',
				email: 'user01@mail.com',
				password: '12345',
				isAdmin: true,
				articlesCount: 1
			});
			salt = await bcrypt.genSalt(10);
			user.password = await bcrypt.hash(user.password, salt);
			await user.save();

			token = user.generateAuthToken();

			categoryId = mongoose.Types.ObjectId();

			category = new Category({
				_id: categoryId,
				title: 'category1',
				articles: []
			});

			tag1 = new Tag({
				_id: mongoose.Types.ObjectId(),
				title: 'tag01',
				articles: []
			});

			tag2 = new Tag({
				_id: mongoose.Types.ObjectId(),
				title: 'tag02',
				articles: []
			});

			article = new Article({
				_id: mongoose.Types.ObjectId(),
				user: {
					_id: user._id,
					name: user.name
				},
				description: '12345',
				title: '12345',
				content: '12345',
				comments: [],
				tags: [],
				category: {
					_id: categoryId,
					title: category.title
				}
			});

			tag1.articles.push(article._id);
			tag2.articles.push(article._id);
			tag1.articles.push(mongoose.Types.ObjectId());
			tag2.articles.push(mongoose.Types.ObjectId());
			await tag1.save();
			await tag2.save();

			category.articles.push(article._id);
			category.articles.push(mongoose.Types.ObjectId());
			await category.save();

			comment1 = new Comment({
				_id: mongoose.Types.ObjectId(),
				article: article._id,
				text: '12345'
			});
			comment2 = new Comment({
				_id: mongoose.Types.ObjectId(),
				article: article._id,
				text: '54321'
			});
			article.comments.push(comment1);
			article.comments.push(comment2);
			article.tags.push({_id: tag1._id, title: tag1.title});
			article.tags.push({_id: tag2._id, title: tag2.title});

			await comment1.save();
			await comment2.save();
			await article.save();

			articleId = article._id;
		});

		afterEach(async () => {
			await Comment.deleteMany({});
			await User.deleteMany({});
			await Category.deleteMany({});
			await Tag.deleteMany({});
		});

		it('should return 401 if client is not logged in', async () => {
			token = '';

			const res = await exec();

			expect(res.status).toBe(401); 
		});

		it('should return 401 if user is not authorized', async () => {
			user = new User({
				_id: mongoose.Types.ObjectId(),
				isAdmin: false
			});

			token = user.generateAuthToken();
			
			const res = await exec();

			expect(res.status).toBe(401);
		});

		it('should return 404 if invalid id is passed', async () => {
			articleId = '1';

			const res = await exec();

			expect(res.status).toBe(404);
		});

		it('should return 404 if no article with the given id exists', async () => {
			articleId = mongoose.Types.ObjectId();

			const res = await exec();

			expect(res.status).toBe(404);
		});

		it('should delete the article and related comments from db if id is valid', async () => {
			const res = await exec();

			article = await Article.findById(res.body._id);

			const comments = await Comment.find({article: res.body._id});

			expect(res.status).toBe(200);
			expect(article).toBeNull();
			expect(comments.length).toBe(0);
		});

		it('should delete article id from category and tags', async () => {
			const res = await exec();

			const tags = await Tag.find({articles: article._id});
			
			category = await Category.findOne({_id: categoryId});

			expect(res.status).toBe(200);
			expect(tags.length).toBe(0);
			expect(category).not.toHaveProperty('articles', articleId);
		});

		it('should return the removed article', async () => {
      const res = await exec();

      expect(res.body).toHaveProperty('_id', article._id.toHexString());
      expect(res.body).toHaveProperty('title', article.title);
      expect(res.body).toHaveProperty('content', article.content);
    });

    it('should decrement articlesCount for article owner', async () => {
			const res = await exec();

			user = await User.findById(user._id);

			expect(res.status).toBe(200);
			expect(user).toHaveProperty('articlesCount', 0);
		});
	});

});