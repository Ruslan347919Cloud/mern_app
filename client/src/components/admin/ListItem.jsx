import React from 'react';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';

class ListItem extends React.Component {

	deleteRecord = event => {
		event.preventDefault();
		
		const {route, deleteItem} = this.props;
		const {value} = event.currentTarget.querySelector('input');
		
		deleteItem(value, route);
	};

	changeVisibleStatus = event => {
		event.preventDefault();

		const {
			route,
			changeItemStatus,
			item: {
				_id
			}
		} = this.props;
		
		changeItemStatus(_id, route);
	};

	changeStatusButton() {
		const {_id, visible} = this.props.item;
		let icon = (Number(visible))
			? (<span className="mx-1">
					<i className="fa fa-ban" aria-hidden="true"></i>
				</span>)
			: (<span className="mx-1">
					<i className="fa fa-check-circle" aria-hidden="true"></i>
				</span>);
		
		return (
			<a href="#status" onClick={this.changeVisibleStatus}>
    		<input type="text" defaultValue={_id} hidden={true} />
    		{icon}
    	</a>
		);
	};

	shorten(value, maxLength) {
		value = String(value);

		return (value.length > maxLength)
			? value.slice(0, maxLength)+'...' : value;
	};

	getValues() {
		const {item} = this.props;
		const values = Object.values(item);

		return values.map(value => (
			<td key={Math.random()}>
				{this.shorten(value, 50)}
			</td>
		));
	};

	render() {
		const {_id, visible} = this.props.item;
		const {itemName} = this.props;

		return (
			<tbody>
				<tr>
			    {this.getValues()}
			    <td>
			    	{(typeof visible !== 'undefined') && this.changeStatusButton()}
			    	<Link
		    		 	className="mx-1"
			    		to={`/admin/${itemName}/view/${_id}`}
		    		>
			    		<i className="fa fa-eye" aria-hidden="true"></i>
		    		</Link>
		    		<Link
	    			 	className="mx-1"
		    			to={`/admin/${itemName}/form/${_id}`}
	    			>
			    		<i className="fa fa-wrench" aria-hidden="true"></i>
			    	</Link>
			    	<a href="#delete" className="mx-1" onClick={this.deleteRecord}>
			    		<input
			    			type="text"
			    			defaultValue={_id}
			    			hidden={true}
		    			/>
			    		<i className="fa fa-trash" aria-hidden="true"></i>
			    	</a>
			    </td>
		    </tr>
		  </tbody>
		);
	};
};

ListItem.propTypes = {
	item: PropTypes.object.isRequired,
	itemName: PropTypes.string.isRequired,
	route: PropTypes.string.isRequired,
	deleteItem: PropTypes.func.isRequired,
	changeItemStatus: PropTypes.func.isRequired,
};

export default ListItem;