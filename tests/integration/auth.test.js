const bcrypt = require('bcrypt');
const {User} = require('../../models/User');
const request = require('supertest');
const mongoose = require('mongoose');

let server;

describe('/api/auth', () => {
	beforeEach(() => {server = require('../../server');});
	afterEach(async () => {
		await User.deleteMany({});
		await server.close();
	});

	describe('POST /', () => {
		let email, password, user;

		const exec = async () => {
			return await request(server)
				.post('/api/auth')
				.send({email, password});
		};

		beforeEach(async () => {
			user = new User({
				_id: mongoose.Types.ObjectId(),
				name: '12345',
				email: 'user1@mail.com',
				password: '12345'
			});
			const salt = await bcrypt.genSalt(10);
			user.password = await bcrypt.hash(user.password, salt);
			await user.save();

			email = 'user1@mail.com';
			password = '12345';
		});

		it('should return 400 if email is invalid', async () => {
			email = '1';

			const res = await exec();

			expect(res.status).toBe(400); 
		});

		it('should return 400 if email is wrong', async () => {
			email = 'user2@mail.com';

			const res = await exec();

			expect(res.status).toBe(400); 
		});

		it('should return 400 if password is less than 5 characters', async () => {
			password = '1234';

			const res = await exec();

			expect(res.status).toBe(400); 
		});

		it('should return 400 if password is invalid', async () => {
			password = '54321';

			const res = await exec();

			expect(res.status).toBe(400); 
		});

		it('should return JWT if given data is valid', async () => {
			const res = await exec();

			expect(res.status).toBe(200);
			expect(Boolean(res.text)).toBeTruthy(); 
		});
	});
});