import React from 'react';
import {connect} from 'react-redux';
import CommentFormForUsersContent from './CommentFormForUsersContent';
import PropTypes from 'prop-types';
import {createComment} from '../../../../actions/CommentActions';

class CommentFormForUsers extends React.Component {
	submitHandler = formData => {
    const {articleId, createComment} = this.props;

    createComment(articleId, formData);
  };

	render() {
		return (
			<div className="col">
        <CommentFormForUsersContent
        	onSubmit={this.submitHandler}
        />
      </div>
		);
	};
};

CommentFormForUsers.propTypes = {
	createComment: PropTypes.func.isRequired
};

const mapDispatchToProps = dispatch => ({
  createComment: (id, data) =>
  	dispatch(createComment(id, data))
});

export default connect(
	null,
	mapDispatchToProps
)(CommentFormForUsers);