import axios from 'axios';
import {setAlert} from '../../AlertActions';
import {loadUser} from '../../AuthActions';
import {
	FIND_USER,
	UPDATE_USER,
	CLEAR_FORM
} from '../../Types';

export const findUser = id => async dispatch => {
	try {
		const res = await axios.get(
			`/api/users/${id}`);
		
		dispatch({
			type: FIND_USER,
			payload: res.data
		});
	} catch (err) {
		const {data: error, status, statusText} = err.response;
		
		console.log(statusText, status);
		console.log(error);
		
		if (error)
			dispatch(setAlert(error, 'danger'));
	}
};

export const updateUser = data => async dispatch => {
	const config = {
		headers: {
			'Content-Type': 'application/json'
		}
	};

	const body = JSON.stringify(data);

	try {
		const res = await axios.put(
			`/api/users`, body, config);
		
		dispatch({
			type: UPDATE_USER,
			payload: res.data
		});

		dispatch(loadUser());

	} catch (err) {
		const {data: error, status, statusText} = err.response;
		
		console.log(statusText, status);
		console.log(error);
		
		if (error)
			dispatch(setAlert(error, 'danger'));
	}
};

export const clearForm = function() {
	return {
		type: CLEAR_FORM,
		payload: {
			defaultValue: '',
			defaultItemData: {}
		}
	};
};