import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import Result from './Result';
import Pagination from '../Pagination/';
import {sortBy} from '../../actions/SortActions';

class Sort extends React.Component {

	constructor(props) {
		super(props);
		this.pageSize = 3;
	};

	componentDidMount() {
		const {sortBy} = this.props;
		const {model, id} = this.props.match.params;
		
		sortBy(model, id, this.getCurrentPage(), this.pageSize);
	};

	componentDidUpdate(prevProps, prevState, prevContext) {
		const {sortBy} = this.props;
		const {model, id, page} = this.props.match.params;
		const {
			model: modelPrev,
			id: idPrev,
			page: pagePrev
		} = prevProps.match.params;

		if (model !== modelPrev || id !== idPrev || page !== pagePrev)
			sortBy(model, id, this.getCurrentPage(), this.pageSize);
	};

	getArticles() {
		const {articles} = this.props;

		return articles.map(article => <Result
			key={article._id}
			article={article}
		/>);
	};

	resultsHeader() {
		const {articles} = this.props;

		return (typeof articles !== 'undefined' && articles.length > 0)
			? 'Result:' : 'No results.';
	};

	getCurrentPage() {
		return (typeof this.props.match.params.page !== 'undefined')
			? Number(this.props.match.params.page) : 1;
	};

	render() {
		const {pages} = this.props;
		const {model, id} = this.props.match.params;

		return (
			<div className="col-md-7 col-lg-8 articles">
				<h4 className="bg-primary rounded-top shadow text-white p-2">
					{this.resultsHeader()}
				</h4>
				{this.getArticles()}
				{(pages.length > 1) && (<Pagination
					route={`/sort/${model}/`}
					pages={pages}
					componentId={id}
					currentPage={this.getCurrentPage()}
				/>)}
			</div>
		);
	};
};

const mapStateToProps = store => {
  const {articles, pages} = store.sort;

  return {articles, pages}
};

const mapDispatchToProps = dispatch => ({
  sortBy: (model, id, page, size) => dispatch(
  	sortBy(model, id, page, size))
});

Sort.propTypes = {
	articles: PropTypes.array.isRequired,
	pages: PropTypes.array.isRequired,
	match: PropTypes.object.isRequired,
	sortBy: PropTypes.func.isRequired
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Sort);