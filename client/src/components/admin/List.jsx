import {connect} from 'react-redux';
import {compose} from 'redux';
import ListItem from './ListItem';
import {withList} from '../hoc/withList';
import {
  fetchItems,
  deleteItem,
  changeItemStatus
} from '../../actions/admin/ListActions';

const mapStateToProps = store => {
  const {items, pages, deletedItemId, changedItem} = store.list;

  return {items, pages, deletedItemId, changedItem};
};

const mapDispatchToProps = dispatch => ({
  fetchItems:(page, size, route) =>
    dispatch(fetchItems(page, size, route)),
	deleteItem: (id, route) =>
    dispatch(deleteItem(id, route)),
  changeItemStatus: (id, route) =>
    dispatch(changeItemStatus(id, route))
});

export default compose(
	connect(mapStateToProps, mapDispatchToProps),
	withList
)(ListItem);