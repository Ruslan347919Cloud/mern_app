import React from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {Redirect} from 'react-router-dom';
import SingleUser from './SingleUser';
import Pagination from '../../../Pagination/';
import {
	fetchUser,
	fetchUserArticles,
	deleteUser,
	clearUserData
}
	from '../../../../actions/admin/User/UserViewActions';

class UserView extends React.Component {
	constructor(props) {
		super(props);
		this.pageSize = 3;
	};

	componentDidMount() {
		const {id} = this.props.match.params;
		const {
			fetchUser,
			fetchUserArticles
		} = this.props;

		fetchUser(id);
		fetchUserArticles(id, this.getCurrentPage(), this.pageSize);
	};

	componentWillUnmount() {
		this.props.clearUserData();
	};

	componentDidUpdate(prevProps) {
		const {fetchUserArticles} = this.props;
		const {id, page} = this.props.match.params;
		const {id: idPrev, page: pagePrev} = prevProps.match.params;
		
		if (id !== idPrev || page !== pagePrev)
			fetchUserArticles(id, this.getCurrentPage(), this.pageSize);
	};

	getCurrentPage() {
		const {page} = this.props.match.params;

		return (typeof page !== 'undefined') ? Number(page) : 1;
	};

	render() {
		const {
			user,
			articles,
			pages,
			deleted,
			deleteUser
		} = this.props;
		const {id} = this.props.match.params;

		if (deleted)
			return (<Redirect to={`/`}/>);

		return (
			<div className="col-md-12 articles">
				<SingleUser
					user={user}
					articles={articles}
					deleteUser={deleteUser}
				/>
				{(pages) && (pages.length > 1) && (<Pagination
					route={`/admin/user/view/`}
					pages={pages}
					componentId={id}
					currentPage={this.getCurrentPage()}
				/>)}
			</div>
		);
	};
};

UserView.propTypes = {
	match: PropTypes.object,
	user: PropTypes.object.isRequired,
	articles: PropTypes.array.isRequired,
	pages: PropTypes.array.isRequired,
	deleted: PropTypes.bool.isRequired,
	fetchUser: PropTypes.func.isRequired,
	fetchUserArticles: PropTypes.func.isRequired,
	deleteUser: PropTypes.func.isRequired,
	clearUserData: PropTypes.func.isRequired
};

const mapStateToProps = store => {
  const {user, articles, pages, deleted} = store.userView;
  
  return {user, articles, pages, deleted};
};

const mapDispatchToProps = dispatch => ({
  fetchUser: userId => dispatch(fetchUser(userId)),
  fetchUserArticles: (userId, page, pageSize) =>
  	dispatch(fetchUserArticles(userId, page, pageSize)),
	deleteUser: userId => dispatch(deleteUser(userId)),
	clearUserData: () => dispatch(clearUserData())
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(UserView);