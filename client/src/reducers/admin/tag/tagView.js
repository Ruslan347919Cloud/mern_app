import {
	FETCH_SINGLE_TAG,
	FETCH_ARTICLES_BY_TAG
} from '../../../actions/Types';

const initialState = {
	tag: {},
	articles: [],
	pages: []
};

export const tagViewReducer = function(state = initialState, action) {
	const {type, payload} = action;

	switch (type) {
		case FETCH_SINGLE_TAG:
			return {
				...state,
				tag: payload
			};
		case FETCH_ARTICLES_BY_TAG:
			return {
				...state,
				...payload
			};
		default: return state;
	}
};