import axios from 'axios';
import {setAlert} from './AlertActions';
import {
	UPLOAD_FILE,
	SET_FILE,
	CLEAR_UPLOAD_DATA
} from './Types';

export const setFile = file => ({
	type: SET_FILE,
	payload: file
});

export const uploadFile = (file, route, id = '') => async dispatch => {
	const config = {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  };

  const formData = new FormData();
  formData.append('image', file);

  try {
    const res = await axios.post(
      `/api/${route}/uploadImage/${id}`, formData, config);

    dispatch({
			type: UPLOAD_FILE,
			payload: res.data
		});
  } catch (err) {
    console.log(err.message);

    if (err.response) {
    	const {data: error, status, statusText} = err.response;
    
	    console.log(statusText, status);
	    console.log(error);

	    if (error)
				dispatch(setAlert(error, 'danger'));
    }
  }
};

export const clearUploadData = () => ({
	type: CLEAR_UPLOAD_DATA,
	payload: {}
});