import {
	FETCH_SINGLE_COMMENT,
	CREATE_COMMENT,
	UPDATE_COMMENT,
	CLEAR_COMMENT_FORM
} from '../../../actions/Types';

const initialState = {
	currentCommentId: '',
	currentCommentText: '',
	currentCommentArticleId: '',
	newComment: {}
};

export const commentFormReducer = function(state = initialState, action) {
	const {type, payload} = action;

	switch (type) {
		case FETCH_SINGLE_COMMENT:
			return {
				...state,
				currentCommentId: payload._id,
				currentCommentText: payload.text,
				currentCommentArticleId: payload.article._id
			};
		case CREATE_COMMENT:
		case UPDATE_COMMENT:
			return {
				...state,
				newComment: payload
			};
		case CLEAR_COMMENT_FORM:
			return {
				...state,
				currentCommentId: payload.defaultValue,
				currentCommentText: payload.defaultValue,
				currentCommentArticleId: payload.defaultValue,
				newComment: payload.defaultData
			}
		default: return state;
	}
};