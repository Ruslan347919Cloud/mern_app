const request = require('supertest');
const mongoose = require('mongoose');
const {Comment} = require('../../models/Comment');
const {User} = require('../../models/User');
const {Article} = require('../../models/Article');

let server;

describe('/api/comments', () => {
	beforeEach(() => {server = require('../../server');});
	afterEach(async () => {
		await server.close();
		await Comment.deleteMany({});
	});

	describe('GET /page/:page/size/:pageSize', () => {
		let page, pageSize;

		const exec = async () => {
			return await request(server)
				.get(`/api/comments/page/${page}/size/${pageSize}`);
		};

		beforeEach(async () => {
			await Comment.collection.insertMany([
				{text: 'comment1'},
				{text: 'comment2'},
				{text: 'comment3'},
				{text: 'comment4'},
				{text: 'comment5'},
				{text: 'comment6'},
				{text: 'comment7'},
				{text: 'comment8'}
			]);

			page = 2;
			pageSize = 3;
		});

		it('should return all comments for current page', async () => {
			const res = await exec();

			const {comments} = res.body;

			expect(res.status).toBe(200);
			expect(comments.length).toBe(3);
			expect(comments.some(comment => comment.text === 'comment5'))
				.toBeTruthy();
			expect(comments.some(comment => comment.text === 'comment4'))
				.toBeTruthy();
			expect(comments.some(comment => comment.text === 'comment3'))
				.toBeTruthy();
		});

		it('should return first page if given page is less than 1', async () => {
			page = 0;

			const res = await exec();

			const {comments} = res.body;

			expect(res.status).toBe(200);
			expect(comments.length).toBe(3);
			expect(comments.some(comment => comment.text === 'comment8'))
				.toBeTruthy();
			expect(comments.some(comment => comment.text === 'comment7'))
				.toBeTruthy();
			expect(comments.some(comment => comment.text === 'comment6'))
				.toBeTruthy();
		});

		it('should return last page if given page is more then max page', async () => {
			page = 4;

			const res = await exec();

			const {comments} = res.body;

			expect(res.status).toBe(200);
			expect(comments.length).toBe(2);
			expect(comments.some(comment => comment.text === 'comment2'))
				.toBeTruthy();
			expect(comments.some(comment => comment.text === 'comment1'))
				.toBeTruthy();
		});

		it('should return information about pages', async () => {
			const res = await exec();

			const {pages} = res.body;

			expect(res.status).toBe(200);
			expect(pages.length).toBe(3);
			expect(pages.some(page => {
				page.number === 1 && page.active === false
			}));
			expect(pages.some(page => {
				page.number === 2 && page.active === true
			}));
			expect(pages.some(page => {
				page.number === 3 && page.active === false
			}));
		});
	});

	describe('GET /article/:id/page/:page/size/:pageSize', () => {
		let page, pageSize, article, articleId;

		const exec = async () => {
			return await request(server)
				.get(`/api/comments/article/${articleId}/page/${page}/size/${pageSize}`);
		};

		beforeEach(async () => {
			articleId = mongoose.Types.ObjectId();

			article = new Article({
				_id: articleId,
				title: 'article1',
				content: '12345'
			});
			await article.save();

			await Comment.collection.insertMany([
				{text: 'comment01', article: articleId},
				{text: 'comment02'},
				{text: 'comment03', article: articleId},
				{text: 'comment04'},
				{text: 'comment05', article: articleId},
				{text: 'comment06'},
				{text: 'comment07', article: articleId},
				{text: 'comment08'},
				{text: 'comment09', article: articleId},
				{text: 'comment10'},
				{text: 'comment11', article: articleId},
				{text: 'comment12'},
				{text: 'comment13', article: articleId},
				{text: 'comment14'},
				{text: 'comment15', article: articleId},
			]);

			page = 2;
			pageSize = 3;
		});

		afterEach(async () => {
			await Article.deleteMany({});
		});

		it('should return 404 if invalid ID is passed', async () => {
			articleId = '1';

			const res = await exec();

			expect(res.status).toBe(404);
		});

		it('should return 404 if no article with given ID exists', async () => {
			articleId = mongoose.Types.ObjectId();

			const res = await exec();

			expect(res.status).toBe(404);
		});

		it('should return all comments for current artile on current page', async () => {
			const res = await exec();

			const {comments} = res.body;

			expect(res.status).toBe(200);
			expect(comments.length).toBe(3);
			expect(comments.some(comment => comment.text === 'comment09'))
				.toBeTruthy();
			expect(comments.some(comment => comment.text === 'comment07'))
				.toBeTruthy();
			expect(comments.some(comment => comment.text === 'comment05'))
				.toBeTruthy();
		});

		it('should return first page if given page is less than 1', async () => {
			page = 0;

			const res = await exec();

			const {comments} = res.body;

			expect(res.status).toBe(200);
			expect(comments.length).toBe(3);
			expect(comments.some(comment => comment.text === 'comment15'))
				.toBeTruthy();
			expect(comments.some(comment => comment.text === 'comment13'))
				.toBeTruthy();
			expect(comments.some(comment => comment.text === 'comment11'))
				.toBeTruthy();
		});

		it('should return last page if given page is more then max page', async () => {
			page = 4;

			const res = await exec();

			const {comments} = res.body;

			expect(res.status).toBe(200);
			expect(comments.length).toBe(2);
			expect(comments.some(comment => comment.text === 'comment03'))
				.toBeTruthy();
			expect(comments.some(comment => comment.text === 'comment01'))
				.toBeTruthy();
		});

		it('should return information about pages', async () => {
			const res = await exec();

			const {pages} = res.body;

			expect(res.status).toBe(200);
			expect(pages.length).toBe(3);
			expect(pages.some(page => {
				page.number === 1 && page.active === false
			}));
			expect(pages.some(page => {
				page.number === 2 && page.active === true
			}));
			expect(pages.some(page => {
				page.number === 3 && page.active === false
			}));
		});
	});

	describe('GET /:id', () => {
		it('should return a single comment if valid ID is passed', async () => {
			const comment = new Comment({text: 'aaaaa'});
			await comment.save();

			const res = await request(
				server).get(`/api/comments/${comment._id}`);

			expect(res.status).toBe(200);
			expect(res.body).toHaveProperty('text', comment.text);
		});

		it('should return 404 if invalid ID is passed', async () => {
			const res = await request(
				server).get('/api/comments/1');

			expect(res.status).toBe(404);
		});

		it('should return 404 if no comment with the given ID exists', async () => {
			const id = mongoose.Types.ObjectId();
			const res = await request(
				server).get(`/api/comments/${id}`);

			expect(res.status).toBe(404);
		});
	});

	describe('GET /search/:keyword', () => {
		let keyword;

		const exec = async () => {
			return await request(server)
				.get(`/api/comments/search/${keyword}`);
		};

		beforeEach(async () => {
			await Comment.collection.insertMany([
				{text: 'commentKeyword01'},
				{text: 'comment02'},
				{text: 'commentKeyWord03'}
			]);

			keyword = 'keyword';
		});

		it('should return comments with keywords', async () => {
			const res = await exec();

			expect(res.status).toBe(200);
			expect(res.body.length).toBe(2);
			expect(res.body.some(comment =>
				comment.text === 'commentKeyword01')).toBeTruthy();
			expect(res.body.some(comment =>
				comment.text === 'commentKeyWord03')).toBeTruthy();
		});
	});

	describe('POST /article/:id', () => {
		let token, text, article, articleId;

		const exec = async () => {
			return await request(server)
				.post(`/api/comments/article/${articleId}`)
				.set('x-auth-token', token)
				.send({text});
		}

		beforeEach(async () => {
			token = new User().generateAuthToken();
			article = new Article({
				_id: mongoose.Types.ObjectId(),
				user: mongoose.Types.ObjectId(),
				description: '12345',
				title: '12345',
				content: '12345'
			});
			await article.save();
			articleId = article._id;
			text = '12345';
		});

		afterEach(async () => {
			await Article.deleteMany({});
		});

		it('should return 401 if client is not logged in', async () => {
			token = '';
			
			const res = await exec();

			expect(res.status).toBe(401);
		});

		it('should return 404 if invalid ID is passed', async () => {
			articleId = '1';

			const res = await exec();

			expect(res.status).toBe(404);
		});

		it('should return 404 if no article with given ID exists', async () => {
			articleId = mongoose.Types.ObjectId();

			const res = await exec();

			expect(res.status).toBe(404);
		});

		it('should return 400 if text is less than 5 characters', async () => {
			text = '1234';

			const res = await exec();

			expect(res.status).toBe(400);
		});

		it('should return 400 if text is more than 1250 characters', async () => {
			text = new Array(1252).join('a');

			const res = await exec();

			expect(res.status).toBe(400);
		});

		it('should save the comment if it is valid', async () => {
			const res = await exec();

			const comment = await Comment.findOne({text: '12345'});

			article = await Article.findById(res.body.article);
			
			expect(comment).not.toBeNull();
			expect(comment).toHaveProperty('article', articleId);
			expect(article.comments).toContainEqual(
				expect.objectContaining({_id: comment._id}));
		});

		it('should return the comment if it is valid', async () => {
			const res = await exec();

			expect(res.body).toHaveProperty('_id');
			expect(res.body).toHaveProperty('text', '12345');
		});
	});

	describe('PUT /:id', () => {
		let token, user, article, text,
			visible, comment, commentId;

		const exec = async () => {
			return await request(server)
				.put(`/api/comments/${commentId}`)
				.set('x-auth-token', token)
				.send({text, visible});
		};

		beforeEach(async () => {
			user = new User({
				_id: mongoose.Types.ObjectId(),
				isAdmin: true
			});

			token = user.generateAuthToken();

			article = new Article({
				_id: mongoose.Types.ObjectId(),
				user: mongoose.Types.ObjectId(),
				description: '12345',
				title: '12345',
				content: '12345',
				comments: []
			});

			comment = new Comment({
				_id: mongoose.Types.ObjectId(),
				text: '12345',
				article: article._id,
				user: user._id
			});

			article.comments.push(comment);

			await article.save();
			await comment.save();
			
			text = '54321';
			visible = true;
			commentId = comment._id;
		});

		afterEach(async () => {
			await Article.deleteMany({});
		});

		it('should return 401 if client is not logged in', async () => {
			token = '';
			
			const res = await exec();

			expect(res.status).toBe(401);
		});

		it('should return 404 if invalid id is passed', async () => {
			commentId = 1;

			const res = await exec();

			expect(res.status).toBe(404);
		});

		it('should return 404 if no comment with the given id exists', async () => {
			commentId = mongoose.Types.ObjectId();

			const res = await exec();

			expect(res.status).toBe(404);
		});

		it('should return 400 if comment is less than 5 characters', async () => {
			text = '1234';

			const res = await exec();

			expect(res.status).toBe(400);
		});

		it('should return 400 if comment is more than 1250 characters', async () => {
			text = new Array(1252).join('a');

			const res = await exec();

			expect(res.status).toBe(400);
		});

		it('should return 401 if user is not authorized', async () => {
			user = new User({
				_id: mongoose.Types.ObjectId(),
				isAdmin: false
			});

			token = user.generateAuthToken();

			const res = await exec();

			expect(res.status).toBe(401);
		});

		it('should update comment from db and article collection if id is valid', async () => {
			const res = await exec();

			comment = await Comment.findById(res.body._id);

			article = await Article.findById(res.body.article);

			expect(res.status).toBe(200);
			expect(comment.text).toBe('54321');
			expect(comment.visible).toBeTruthy();
			expect(article.comments).toContainEqual(
				expect.objectContaining({text: '54321'}));
			expect(article.comments).toContainEqual(
				expect.objectContaining({visible: true}));
		});

		it('should allow to set only visibility', async () => {
			text = null;

			const res = await exec();

			comment = await Comment.findById(res.body._id);

			article = await Article.findById(res.body.article);

			expect(res.status).toBe(200);
			expect(comment.text).toBe('12345');
			expect(comment.visible).toBeTruthy();
			expect(article.comments).toContainEqual(
				expect.objectContaining({text: '12345'}));
			expect(article.comments).toContainEqual(
				expect.objectContaining({visible: true}));
		});

		it('should allow to set only text', async () => {
			visible = null;

			const res = await exec();

			comment = await Comment.findById(res.body._id);

			article = await Article.findById(res.body.article);

			expect(res.status).toBe(200);
			expect(comment.text).toBe('54321');
			expect(comment.visible).toBeFalsy();
			expect(article.comments).toContainEqual(
				expect.objectContaining({text: '54321'}));
			expect(article.comments).toContainEqual(
				expect.objectContaining({visible: false}));
		});

		it('should not allow to set visibility if user is not admin', async () => {
			user.isAdmin = false;

			token = user.generateAuthToken();

			const res = await exec();

			expect(res.status).toBe(403);
		});

		it('should return the comment if it is valid', async () => {
			const res = await exec();

			expect(res.body).toHaveProperty('_id', comment._id.toHexString());
			expect(res.body).toHaveProperty('text', '54321');
		});
	});

	describe('DELETE /:id', () => {
		let token, user, article, comment, commentId;

		const exec = async () => {
			return await request(server)
				.delete(`/api/comments/${commentId}`)
				.set('x-auth-token', token);
		};

		beforeEach(async () => {
			user = new User({
				_id: mongoose.Types.ObjectId(),
				isAdmin: true
			});

			token = user.generateAuthToken();

			article = new Article({
				_id: mongoose.Types.ObjectId(),
				user: mongoose.Types.ObjectId(),
				description: '12345',
				title: '12345',
				content: '12345'
			});

			comment = new Comment({
				_id: mongoose.Types.ObjectId(),
				text: '12345',
				article: article._id,
				user: mongoose.Types.ObjectId()
			});

			article.comments.push(comment);

			await article.save();
			await comment.save();

			commentId = comment._id;
		});

		afterEach(async () => {
			await Article.deleteMany({});
		});

		it('should return 401 if client is not logged in', async () => {
			token = '';

			const res = await exec();

			expect(res.status).toBe(401); 
		});

		it('should return 404 if invalid id is passed', async () => {
			commentId = 1;

			const res = await exec();

			expect(res.status).toBe(404);
		});

		it('should return 404 if no comment with the given id exists', async () => {
			commentId = mongoose.Types.ObjectId();

			const res = await exec();

			expect(res.status).toBe(404);
		});

		it('should return 401 if user is not authorized', async () => {
			user = new User({
				_id: mongoose.Types.ObjectId(),
				isAdmin: false
			});

			token = user.generateAuthToken();

			const res = await exec();

			expect(res.status).toBe(401);
		});

		it('should delete comment from db and article collection if id is valid', async () => {
			const res = await exec();

			comment = await Comment.findById(res.body._id);

			article = await Article.findById(res.body.article);

			expect(res.status).toBe(200);
			expect(comment).toBeNull();
			expect(article.comments.length).toBe(0);
		});

		it('should return the removed comment', async () => {
      const res = await exec();

      expect(res.body).toHaveProperty('_id', comment._id.toHexString());
      expect(res.body).toHaveProperty('text', comment.text);
    });
	});

});