import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {NavLink} from 'react-router-dom';
import {changeState} from '../../actions/SearchActions';

class SearchForm extends React.Component {

	changeKeyword = event => {
		const {changeStateAction} = this.props;

		changeStateAction(event.currentTarget.value);
	};

	blockEmptyString = event => {
		const EMPTY_LINE = /^\s*$/;
		const {keyword} = this.props;

		return (EMPTY_LINE.test(keyword))
			&& (event.preventDefault());
	};

	render() {
		let {keyword} = this.props;

		return (
			<form className="form-inline">
	    	<div className="input-group">
		      <input
		      	className="form-control"
		      	type="search" placeholder="Search"
		      	aria-label="Search" value={keyword}
		      	onChange={this.changeKeyword}
	      	/>
		      <div className="input-group-append">
			      <NavLink
			      	className="btn btn-outline-light"
			      	to={`/search/${keyword.trim()}`}
			      	onClick={this.blockEmptyString}>
			      	<i className="fa fa-search" aria-hidden="true"></i>
			      </NavLink>
		    	</div>
	    	</div>
	    </form>
		);
	};
};

const mapStateToProps = store => {
  const {keyword} = store.search;
  
  return {keyword};
};

const mapDispatchToProps = dispatch => ({
  changeStateAction: keyword => dispatch(changeState(keyword))
});

SearchForm.propTypes = {
	keyword: PropTypes.string.isRequired,
	changeStateAction: PropTypes.func.isRequired
};

export default connect(
	mapStateToProps,
  mapDispatchToProps
)(SearchForm);