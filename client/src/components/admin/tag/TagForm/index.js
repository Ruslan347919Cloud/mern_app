import {connect} from 'react-redux';
import {compose} from 'redux';
import TagFormContent from './TagFormContent';
import {withForm} from '../../../hoc/withForm';
import {
  findItem,
  createItem,
  updateItem,
  clearForm
} from '../../../../actions/admin/FormActions';

const mapStateToProps = store => {
  const {
  	currentTagId,
  	currentTagTitle,
  	newTagData
  } = store.tagForm;
  
  return {
    route: 'tags',
    itemName: 'tag',
  	updatingItemId: currentTagId,
    newItemData: newTagData,
    initialValues: {
    	title: currentTagTitle
    }
  }
};

const mapDispatchToProps = dispatch => ({
	findItem: (id, route) => dispatch(findItem(id, route)),
  createItem: (data, route) => dispatch(createItem(data, route)),
  updateItem: (id, data, route) =>
    dispatch(updateItem(id, data, route)),
  clearForm: () => dispatch(clearForm())
});

export default compose(
	connect(mapStateToProps, mapDispatchToProps),
	withForm
)(TagFormContent);