import {
	FETCH_USER,
	FETCH_USER_ARTICLES,
	DELETE_USER,
	CLEAR_USER_DATA
} from '../../../actions/Types';

const initialState = {
	user: {},
	articles: [],
	pages: [],
	deleted: false
};

export const userViewReducer = function(state = initialState, action) {
	const {type, payload} = action;

	switch (type) {
		case FETCH_USER:
			return {
				...state,
				user: payload
			};
		case FETCH_USER_ARTICLES:
			return {
				...state,
				...payload
			};
		case DELETE_USER:
			return {
				...state,
				deleted: true
			};
		case CLEAR_USER_DATA:
			return {
				...state,
				...payload
			}
		default: return state;
	}
};