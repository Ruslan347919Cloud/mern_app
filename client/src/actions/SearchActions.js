import axios from 'axios';
import {
	SEARCH_CHANGE_STATE,
	SEARCH_SEND_QUERY
} from './Types';

export const changeState = keyword => ({
	type: SEARCH_CHANGE_STATE,
	payload: keyword
});

export const sendQuery = keyword => async dispatch => {
	keyword = keyword.trim();

	try {
		const resArticles = await axios.get(
			`/api/articles/search/${keyword}`);
		const resComments = await axios.get(
			`/api/comments/search/${keyword}`);
		const resUsers = await axios.get(
			`/api/users/search/${keyword}`);
		
		dispatch({
			type: SEARCH_SEND_QUERY,
			payload: {
				articles: resArticles.data,
				comments: resComments.data,
				users: resUsers.data
			}
		});
	} catch (err) {
		console.log(err.message);
	}
};