import React, {Fragment, useEffect} from 'react';
import {Route, Switch} from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.css';
import './styles/App.css';
import 'bootstrap/dist/js/bootstrap.min.js';
import '@fortawesome/fontawesome-free/css/all.css';
import {store} from './store/configureStore';
import {loadUser} from './actions/AuthActions';
import setAuthToken from './utils/setAuthToken';
import AdminDefault from './components/admin/AdminDefault';
import HeaderCms from './components/Header/HeaderCms';
import Header from './components/Header/';
import Footer from './components/Footer/';
import Articles from './components/Articles/';
import Register from './components/auth/Register';
import Login from './components/auth/Login';
import Alert from './components/Alert/';
import Sidebar from './components/Sidebar/';
import Single from './components/Single/';
import Sort from './components/Sort/';
import SearchResult from './components/Header/SearchResult/';
import FileUpload from './components/FileUpload/';
import List from './components/admin/List';
import UserView from './components/admin/user/UserView/';
import UserForm from './components/admin/user/UserForm/';
import SetAttributes from './components/admin/article/ArticleView/SetAttributes';
import ArticleView from './components/admin/article/ArticleView/';
import ArticleForm from './components/admin/article/ArticleForm/';
import CommentView from './components/admin/comment/CommentView/';
import CommentForm from './components/admin/comment/CommentForm/';
import CategoryView from './components/admin/category/CategoryView/';
import CategoryForm from './components/admin/category/CategoryForm/';
import TagView from './components/admin/tag/TagView/';
import TagForm from './components/admin/tag/TagForm/';

if (localStorage.token)
  setAuthToken(localStorage.token);

const App = () => {
  useEffect(() => {
    store.dispatch(loadUser());
  }, []);

  return (
    <Fragment>
    	<Switch>
        <Route path="/admin" component={HeaderCms} />
        <Route path="/" component={Header} />
      </Switch>
      <div className="wrapper">
        <div className="container-fluid content">
          <Alert/>
          <div className="row">
          	<Switch>
          		<Route exact path="/" render={() => <Articles/>} />
              <Route exact path="/login" component={Login} />
              <Route exact path="/register" component={Register} />
              <Route path="/articles/:page" component={Articles} />
              <Route path="/single/:id/:page?" component={Single} />
              <Route path="/sort/:model/:id/:page?" component={Sort} />
              <Route path="/search/:keyword" component={SearchResult} />
              <Route path="/upload/:route/:itemName/:id?" component={FileUpload} />
              <Route exact path="/admin" component={AdminDefault} />
              <Route path="/admin/:itemName/index/:page?" component={List} />
              <Route path="/admin/user/view/:id/:page?" component={UserView} />
              <Route path="/admin/user/form/:id?" component={UserForm} />
              <Route path="/admin/article/view/:id/:page?" component={ArticleView} />
              <Route path="/admin/article/form/:id?" component={ArticleForm} />
              <Route path="/admin/article/set/:attribute/:id" component={SetAttributes}/>
              <Route path="/admin/comment/view/:id/:page?" component={CommentView} />
              <Route path="/admin/comment/form/:id?" component={CommentForm} />
              <Route path="/admin/category/view/:id/:page?" component={CategoryView} />
              <Route path="/admin/category/form/:id?" component={CategoryForm} />
              <Route path="/admin/tag/view/:id/:page?" component={TagView} />
              <Route path="/admin/tag/form/:id?" component={TagForm} />
          	</Switch>
            <Route exact path={[
              '/',
              '/articles/:page',
              '/single/:id/:page?',
              '/sort/:model/:id/:page?',
              '/search/:keyword'
            ]} component={Sidebar}/>
          </div>
          <Alert/>
        </div>
        <Footer/>
      </div>
    </Fragment>
)};

export default App;
