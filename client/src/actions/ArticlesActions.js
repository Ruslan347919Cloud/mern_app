import axios from 'axios';
import {FETCH_ARTICLES} from './Types';

export const fetchArticles = (page, size) => async dispatch => {
	try {
		const res = await axios.get(
			`/api/articles/page/${page}/size/${size}`);
		
		dispatch({
			type: FETCH_ARTICLES,
			payload: res.data
		});
	} catch (err) {
		console.log(err.message);
	}
};