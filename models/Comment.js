const getCurrentDate = require('../helpers/getCurrentDate');
const mongoose = require('mongoose');
const Joi = require('@hapi/joi');

const validateComment = comment => {
	const schema = Joi.object({
		text: Joi.string().min(5).max(1250).required()
	});
	return schema.validate(comment);
};

const validateCommentUpdate = comment => {
	const schema = Joi.object({
		text: Joi.string().min(5).max(1250).allow(null),
		visible: Joi.boolean().allow(null)
	});
	return schema.validate(comment);
};

const commentSchema = new mongoose.Schema({
	user: {
		type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
	},
	article: {
		type: mongoose.Schema.Types.ObjectId,
    ref: 'Article'
	},
	text: {
		type: String,
		minlength: 5,
		maxlength: 1250,
		required: true,
		trim: true
	},
	visible: {
		type: Boolean,
		default: false
	},
	put_date: {
		type: String,
		default: getCurrentDate()
	}
});

commentSchema.methods.checkAuthorization = function(req) {
	return (!req.user.isAdmin && this.user.toString()
		!== req.user._id) ? false : true;
};

const Comment = new mongoose.model('Comment', commentSchema);

exports.Comment = Comment;
exports.commentSchema = commentSchema;
exports.validateComment = validateComment;
exports.validateCommentUpdate = validateCommentUpdate;