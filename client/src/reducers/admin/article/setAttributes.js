import {
	GET_ARTICLE_ATTRIBUTES,
	FETCH_ATTRIBUTES,
	CLEAR_ATTRIBUTES_DATA,
	SET_ATTRIBUTE
} from '../../../actions/Types';

const initialState = {
	attributesAll: [],
	category: {},
	tags: [],
	receivedData: {}
};

export const setAttributesReducer = function(state = initialState, action) {
	const {type, payload} = action;

	switch (type) {
		case GET_ARTICLE_ATTRIBUTES:
			return {
				...state,
				category: payload.category,
				tags: payload.tags
			};
		case FETCH_ATTRIBUTES:
			return {
				...state,
				attributesAll: payload
			};
		case CLEAR_ATTRIBUTES_DATA:
			return {
				...state,
				...payload
			};
		case SET_ATTRIBUTE:
			return {
				...state,
				receivedData: payload
			};
		default: return state;
	}
};
