import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

class SingleComment extends React.Component {

	deleteComment = event => {
		event.preventDefault();

		const {_id} = this.props.item;

		this.props.deleteItem(_id, 'comments');
	};

	changeVisibleStatus = event => {
		event.preventDefault();

		const {_id} = this.props.item;
		const {changeStatus} = this.props;

		changeStatus(_id);
	};

	render() {
		const {item} = this.props;

		return (
			<React.Fragment>
				<div className="list-group mb-3 shadow">
					<div className="list-group-item d-flex justify-content-between border">
				    <span>Id</span>
				    <span>{item._id}</span>
				  </div>
					<div className="list-group-item d-flex justify-content-between border">
				    <span>{item.text}</span>
				  </div>
				  <div className="list-group-item d-flex justify-content-between border">
				    <span>Author</span>
				    <span>{item.user && item.user.name}</span>
				  </div>
				  <div className="list-group-item d-flex justify-content-between border">
				    <span>Added</span>
				    <span>{item.put_date}</span>
				  </div>
				  <div className="list-group-item d-flex justify-content-between border">
				    <span>Visible</span>
				    <span>{String(item.visible)}</span>
				  </div>
				  <div className="list-group-item d-flex justify-content-between border">
				    <span>Article</span>
				    <span>{item.article && item.article.title}</span>
				  </div>
				</div>
				<div className="mb-3">
					<Link className="btn btn-primary btn mr-2" to={`/admin/comment/form/${item._id}`}>
						Update comment
					</Link>
					<button className="btn btn-primary btn mr-2" type="button" onClick={this.changeVisibleStatus}>
						Change status
					</button>
					<button className="btn btn-danger btn mr-2" type="button" onClick={this.deleteComment}>
						Delete comment
					</button>
				</div>
			</React.Fragment>
		);
	};
};

SingleComment.propTypes = {
	item: PropTypes.object.isRequired,
	deleteItem: PropTypes.func.isRequired,
	changeStatus: PropTypes.func.isRequired
};

export default SingleComment;