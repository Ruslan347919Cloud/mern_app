const {Article} = require('../../../models/Article');
const mongoose = require('mongoose');

describe('article.checkAuthorization', () => {
	let req;
	let article;

	beforeEach(() => {
		req = {
			user: {
				_id: new mongoose.Types.ObjectId().toHexString(),
				isAdmin: false
			}
		};

		article = new Article({
			title: 'aaaaa',
			content: 'aaaaa',
			user: {
				_id: new mongoose.Types.ObjectId().toHexString()
			},
			_id: new mongoose.Types.ObjectId().toHexString()
		});
	});

	it('should return false if user is not admin and not own this article', () => {
		const result = article.checkAuthorization(req);

		expect(result).toBeFalsy();
	});

	it('should return true if user is not admin but own this article', () => {
		article.user._id = req.user._id;

		const result = article.checkAuthorization(req);

		expect(result).toBeTruthy();
	});

	it('should return true if user is admin but not own this article', () => {
		req.user.isAdmin = true;

		const result = article.checkAuthorization(req);

		expect(result).toBeTruthy();
	});

	it('should return true if user is admin and own this article', () => {
		req.user.isAdmin = true;
		article.user._id = req.user._id;

		const result = article.checkAuthorization(req);

		expect(result).toBeTruthy();
	});
});