import React from 'react';
import {Redirect} from 'react-router-dom';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {uploadFile, setFile, clearUploadData}
  from '../../actions/FileUploadActions';

class FileUpload extends React.Component {
  componentWillUnmount() {
    this.props.clearUploadData();
  };

  onFileChange = event => {
    this.props.setFile(event.target.files[0]);
  };

  onSubmit = event => {
    event.preventDefault();

    const {route, id} = this.props.match.params;
    const {uploadFile, file} = this.props;

    uploadFile(file, route, id);
  };

  formIsSubmitted() {
    const {receivedData} = this.props;

    return ((Object.keys(receivedData).length !== 0)
      && (typeof receivedData._id !== 'undefined'));
  };

  render() {
    const {itemName} = this.props.match.params;
    const {_id} = this.props.receivedData;

    if (this.formIsSubmitted())
      return <Redirect to={`/admin/${itemName}/view/${_id}`}/>;
    else return (
      <div className="container">
        <div className="row">
          <form onSubmit={this.onSubmit}>
            <h3>File Upload</h3>
            <div className="form-group">
              <input type="file" onChange={this.onFileChange}/>
            </div>
            <div className="form-group">
              <button className="btn btn-primary" type="submit">
                Upload
              </button>
            </div>
          </form>
        </div>
      </div>
    );
  };
};

FileUpload.propTypes = {
  match: PropTypes.object,
  file: PropTypes.object.isRequired,
  receivedData: PropTypes.object.isRequired,
  uploadFile: PropTypes.func.isRequired,
  setFile: PropTypes.func.isRequired,
  clearUploadData: PropTypes.func.isRequired
};

const mapStateToProps = store => {
  const {file, receivedData} = store.fileUpload;
  return {file, receivedData};
};

const mapDispatchToProps = dispatch => ({
  uploadFile: (file, route, id) =>
    dispatch(uploadFile(file, route, id)),
  setFile: file => dispatch(setFile(file)),
  clearUploadData: () => dispatch(clearUploadData())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FileUpload);