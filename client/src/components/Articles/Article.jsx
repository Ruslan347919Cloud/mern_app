import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

class Article extends React.Component {
	render() {
		let {
			_id,
			title,
			content,
			last_update,
			image,
			viewed,
			user: {
				name
			}
		} = this.props.article;

		const defaultImage = `${window.location.origin}/no_image.jpg`;

		image = image ? image : defaultImage;
		//image = image ? `${window.location.origin}/uploads/${image}` : defaultImage;
		
		return (
			<div className="card mb-3 shadow">
				<div className="card-header">
					Author: {name}
				</div>
				<Link className="p-1" to={`/single/${_id}`}>
			  	<img
			  		className="card-img-top" alt="error"
			  		src={image}
		  		/>
				</Link>
			  <div className="card-body">
			    <h5 className="card-title">{title}</h5>
			    <p className="card-text">
						{content.slice(0, 300)+'...'}
			    </p>
			    <Link className="card-link" to={`/single/${_id}`}>Show all</Link>
			  </div>
			  <div className="card-footer text-muted d-flex justify-content-between">
				  <small>Last updated {last_update}</small>
				  <span>
				  	<i className="fa fa-eye" aria-hidden="true"></i>
				  	{' '}
				  	<span>{viewed}</span>
				  </span>
				</div>
			</div>
		);
	};
};

Article.propTypes = {
	article: PropTypes.object.isRequired
};

export default Article;