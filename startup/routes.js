const express = require('express');
const helmet = require('helmet');
const error = require('../middleware/error');
const auth = require('../routes/auth');
const articles = require('../routes/articles');
const categories = require('../routes/categories');
const tags = require('../routes/tags');
const users = require('../routes/users');
const comments = require('../routes/comments');
const cors = require('cors');
const compression = require('compression');

module.exports = app => {
	app.use(express.json());
	app.use(cors());
	app.use(compression());
	app.use(
		helmet({
			contentSecurityPolicy: false,
		})
	);
	app.use('/api/auth', auth);
	app.use('/api/articles', articles);
	app.use('/api/categories', categories);
	app.use('/api/tags', tags);
	app.use('/api/users', users);
	app.use('/api/comments', comments);
	app.use(error);
}