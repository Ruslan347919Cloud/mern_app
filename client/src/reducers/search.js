import {
	SEARCH_CHANGE_STATE,
	SEARCH_SEND_QUERY
} from '../actions/Types';

const initialState = {
	keyword: '',
	result: {}
};

export const searchReducer = function(state = initialState, action) {
	const {type, payload} = action;

	switch (type) {
		case SEARCH_CHANGE_STATE:
			return {...state, keyword: payload};
		case SEARCH_SEND_QUERY:
			return {...state, result: payload};
		default: return state;
	}
};