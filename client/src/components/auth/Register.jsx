import React, {Fragment, useState} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {Link, Redirect} from 'react-router-dom';
import {setAlert} from '../../actions/AlertActions';
import {register} from '../../actions/AuthActions';

const Register = ({setAlert, register, isAuthenticated}) => {
	const [formData, setFormData] = useState({
		name: '',
		email: '',
		password: '',
		passwordConfirm: ''
	});

	const {name, email, password, passwordConfirm} = formData;

	const onChange = event => {
		const {name, value} = event.target;
		setFormData({...formData, [name]: value});
	};

	const onSubmit = async event => {
		event.preventDefault();
		if (password !== passwordConfirm)
			setAlert('passwords do not match!', 'danger');
		else {
			register({name, email, password});
		};
	};

	if (isAuthenticated)
		return (<Redirect to="/" />);

	return (
		<Fragment>
			<form
				className="form-horizontal col-md-7 col-lg-8"
				onSubmit={event => onSubmit(event)}>
			  <fieldset>
			    <div id="legend">
			      <legend className="">Register</legend>
			    </div>
			    <div className="control-group">
			      <label className="control-label" htmlFor="name">
			      	Username
		      	</label>
			      <div className="controls">
			        <input
			        	type="text"
			        	id="name"
			        	name="name"
			        	className="input-xlarge"
			        	value={name}
			        	onChange={event => onChange(event)}
			        	required
		        	/>
			        <p className="help-block">
			        	Username can contain any letters or numbers, without spaces
		        	</p>
			      </div>
			    </div>
			 
			    <div className="control-group">
			      <label className="control-label" htmlFor="email">
			      	E-mail
		      	</label>
			      <div className="controls">
			        <input
			        	type="text"
			        	id="email"
			        	name="email"
			        	className="input-xlarge"
			        	value={email}
			        	onChange={event => onChange(event)}
			        	required
		        	/>
			        <p className="help-block">Please provide your E-mail</p>
			      </div>
			    </div>
			 
			    <div className="control-group">
			      <label className="control-label" htmlFor="password">
			      	Password
		      	</label>
			      <div className="controls">
			        <input
			        	type="password"
			        	id="password"
			        	name="password"
			        	className="input-xlarge"
			        	value={password}
			        	onChange={event => onChange(event)}
			        	required
		        	/>
			        <p className="help-block">
			        	Password should be at least 4 characters
		        	</p>
			      </div>
			    </div>
			 
			    <div className="control-group">
			      <label className="control-label" htmlFor="password_confirm">
			      	Password (Confirm)
		      	</label>
			      <div className="controls">
			        <input
			        	type="password"
			        	id="password_confirm"
			        	name="passwordConfirm"
			        	placeholder=""
			        	className="input-xlarge"
			        	value={passwordConfirm}
			        	onChange={event => onChange(event)}
			        	required
		        	/>
			        <p className="help-block">
			        	Please confirm password
		        	</p>
			      </div>
			    </div>
			 
			    <div className="control-group">
			      <div className="controls">
			        <button className="btn btn-success">
			        	Register
			        </button>
			      </div>
			    </div>

			    <div className="control-group my-2">
			    	<span className="mr-1">
			    		Already have an account?
			    	</span>
			    	<Link to={'/login'}>
			    		Sign In
			    	</Link>
			    </div>
			  </fieldset>
			</form>
		</Fragment>
	);
};

Register.propTypes = {
	setAlert: PropTypes.func.isRequired,
	register: PropTypes.func.isRequired,
	isAuthenticated: PropTypes.bool
};

const mapStateToProps = store => ({
	isAuthenticated: store.auth.isAuthenticated
});

export default connect(
	mapStateToProps,
	{setAlert, register}
)(Register);