import {
	FETCH_SINGLE_COMMENT,
	CHANGE_COMMENT_STATUS,
	DELETE_COMMENT
} from '../../../actions/Types';

const initialState = {
	comment: {},
	deletedCommentId: '',
	status: null
};

export const commentViewReducer = function(state = initialState, action) {
	const {type, payload} = action;

	switch (type) {
		case FETCH_SINGLE_COMMENT:
			return {
				...state,
				comment: payload
			};
		case CHANGE_COMMENT_STATUS:
			return {
				...state,
				status: payload
			};
		case DELETE_COMMENT:
			return {
				...state,
				deletedCommentId: payload
			};
		default: return state;
	}
};