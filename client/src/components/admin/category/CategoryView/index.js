import {connect} from 'react-redux';
import {compose} from 'redux';
import SingleCategory from './SingleCategory';
import {withView} from '../../../hoc/withView';
import {
  fetchSingleCategory,
  fetchArticlesByCategory
} from '../../../../actions/CategoryActions';
import {
  deleteItem
} from '../../../../actions/admin/ListActions';

const mapStateToProps = store => {
  const {category, articles, pages} = store.categoryView;
  const {deletedItemId} = store.list;

  return {
    itemName: 'category',
    item: category,
    junctionItems: articles,
    pages,
    deletedItemId
  };
};

const mapDispatchToProps = dispatch => ({
  fetchSingleItem: id => dispatch(fetchSingleCategory(id)),
  fetchJunctionItems: (id, page, size) =>
    dispatch(fetchArticlesByCategory(id, page, size)),
  deleteItem: (id, route) => dispatch(deleteItem(id, route))
});

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  withView
)(SingleCategory);