const getCurrentDate = require('../helpers/getCurrentDate');
const _ = require('lodash');
const {commentSchema} = require('../models/Comment');
const {Category, categorySchema} = require('../models/Category');
const {Tag} = require('../models/Tag');
const mongoose = require('mongoose');
const Joi = require('@hapi/joi');

const validateArticle = article => {
	const schema = Joi.object({
		title: Joi.string().min(5).max(255).required(),
		description: Joi.string().min(5).max(255).allow(null),
		content: Joi.string().min(5).max(10000).required()
	});
	return schema.validate(article);
};

const validateArticleUpdate = article => {
	const schema = Joi.object({
		title: Joi.string().min(5).max(255).allow(null),
		description: Joi.string().min(5).max(255).allow(null),
		content: Joi.string().min(5).max(10000).allow(null),
		category: Joi.string().max(255).allow(null),
		tags: Joi.array(),
		visible: Joi.boolean().allow(null)
	});
	return schema.validate(article);
};

const articleSchema = new mongoose.Schema({
	user: {
		type: Object,
		_id: mongoose.Schema.Types.ObjectId,
		name: String
	},
	title: {
		type: String,
		minlength: 5,
		maxlength: 255,
		trim: true,
		required: true
	},
	description: {
		type: String,
		minlength: 5,
		maxlength: 255,
		trim: true
	},
	content: {
		type: String,
		minlength: 5,
		maxlength: 10000,
		trim: true,
		required: true
	},
	category: {
		type: Object,
		_id: mongoose.Schema.Types.ObjectId,
		title: {
			type: String,
			required: true,
			minlength: 5,
			maxlength: 50,
			trim: true
		},
		default: null
	},
	tags: [{
		_id: mongoose.Schema.Types.ObjectId,
		title: {
			type: String,
			required: true,
			minlength: 5,
			maxlength: 50,
			trim: true
		}
	}],
	put_date: {
		type: String,
		default: getCurrentDate()
	},
	last_update: {
		type: String,
		default: getCurrentDate()
	},
	image: {
		type: String,
		default: null
	},
	viewed: {
		type: Number,
		default: 0
	},
	visible: {
		type: Boolean,
		default: false
	},
	comments: [commentSchema]
});

articleSchema.methods.getTagsIds = function() {
	return this.tags.map(singleTag => singleTag._id);
};

articleSchema.methods.checkAuthorization = function(req) {
	return (!req.user.isAdmin
		&& this.user._id !== req.user._id) ? false : true;
};

articleSchema.methods.canIncreaseViews = function(req) {
	const token = req.header('x-auth-token');
	const config = require('config');
	const jwt = require('jsonwebtoken');

	const loggedUser = (token) ?
		jwt.verify(token, config.get('jwtPrivateKey'))
		: null;

	return (!loggedUser || !loggedUser.isAdmin
		&& this.user._id !== loggedUser._id);
};

articleSchema.methods.setCategory = async function(category) {
	const idIsValid = mongoose.Types.ObjectId.isValid(category);
	if (!idIsValid)
		return {status: 404, result: 'Invalid category ID.'};

	let categoryInDb = await Category.findById(category);
	if (!categoryInDb)
		return {status: 404, result: 'Cannot find the category with given ID'};

	const oldCategory = await Category.findById(this.category);
	if (oldCategory && String(oldCategory._id) !== category)
		await oldCategory.removeArticle(this._id);

	await categoryInDb.addArticle(this._id);

	categoryInDb = _.pick(categoryInDb, ['_id', 'title']);

	return {status: 200, result: categoryInDb};
};

articleSchema.methods.setTags = async function(tags) {
	const idsIsValid = Tag.validateIds(tags);
	if (!idsIsValid)
		return {status: 404, result: 'Invalid tag ID'};

	let tagsInDb = await Tag.find({_id: {$in: tags}});
	if (tagsInDb.length !== tags.length)
		return {status: 404, result: 'Cannot find some of tags with given IDs'};

	const oldTags = await Tag.find({
		_id: {$in: this.getTagsIds()}
	});
	if (oldTags.length > 0) {
		oldTags.forEach(async oldTag => {
			if (!oldTag.coincides(tags))
				oldTag.removeArticle(this._id);
		});
	}
	
	tagsInDb.forEach(async tagInDb => {
		tagInDb.addArticle(this._id);
	});

	tagsInDb = tagsInDb.map(item =>
		_.pick(item, ['_id', 'title']));

	return {status: 200, result: tagsInDb};
};

const Article = new mongoose.model('Article', articleSchema);

exports.Article = Article;
exports.articleSchema = articleSchema;
exports.validateArticle = validateArticle;
exports.validateArticleUpdate = validateArticleUpdate;