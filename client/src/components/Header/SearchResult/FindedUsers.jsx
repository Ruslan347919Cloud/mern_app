import React from 'react';
import PropTypes from 'prop-types';

class FindedUsers extends React.Component {

	getUsers() {
		const {users} = this.props;

		return users.map(user => (
			<a href ="#1" className="list-group-item list-group-item-action" key={user._id}>
				{user.name}
			</a>
		));
	};

	render() {
		return (
			<div className="list-group shadow mb-3">
				<h4 className="list-group-item active">
					Users:
				</h4>
				{this.getUsers()}
			</div>
		);
	};
};

FindedUsers.propTypes = {
	users: PropTypes.array.isRequired
};

export default FindedUsers;