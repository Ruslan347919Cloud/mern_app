const debugDb = require('debug')('app:db');
const mongoose = require('mongoose');
const config = require('config');
const dbLink = config.get('db');
const dbPassword = config.get('db_password');

const db = dbLink.replace(
	'<password>',
	dbPassword
);

module.exports = async () => {
	try {
		await mongoose.connect(db, {
			useNewUrlParser: true,
			useUnifiedTopology: true,
			useFindAndModify: false,
			useCreateIndex: true
		});
		debugDb(`connected to ${db}`);
	} catch(err) {
		console.error(err.message);
		process.exit(1);
	}
}