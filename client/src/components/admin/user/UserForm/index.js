import React from 'react';
import PropTypes from 'prop-types';
import {Redirect} from 'react-router-dom';
import {connect} from 'react-redux';
import UserFormContent from './UserFormContent';
import {
  findUser,
  updateUser,
  clearForm
} from '../../../../actions/admin/User/UserFormActions';

class UserForm extends React.Component {

  componentDidMount() {
    const {id} = this.props.match.params;
    const {findUser} = this.props;

    if (typeof id !== 'undefined')
      findUser(id);
  };

  componentWillUnmount() {
    this.props.clearForm();
  };

  submitHandler = formData => {
    const {updateUser} = this.props;
    
    updateUser(formData);
  };

  formIsSubmitted() {
    const {newUserData} = this.props;

    return ((Object.keys(newUserData).length !== 0)
      && (typeof newUserData._id !== 'undefined'));
  };

  render() {
    const {newUserData, initialValues} = this.props;
    
    if (this.formIsSubmitted())
      return <Redirect to={`/admin/user/view/${newUserData._id}`}/>;
    else return (
      <div className="col-md-7 col-lg-8 articles">
        <UserFormContent
          onSubmit={this.submitHandler}
          initialValues={initialValues}
        />
      </div>
    );
  };
};

UserForm.propTypes = {
  match: PropTypes.object,
  newUserData: PropTypes.object.isRequired,
  initialValues: PropTypes.object.isRequired,
  findUser: PropTypes.func.isRequired,
  updateUser: PropTypes.func.isRequired,
  clearForm: PropTypes.func.isRequired
};

const mapStateToProps = store => {
  const {
    currentUserName,
    currentUserEmail,
    newUserData
  } = store.userForm;
  
  return {
    newUserData,
    initialValues: {
      name: currentUserName,
      email: currentUserEmail
    }
  };
};

const mapDispatchToProps = dispatch => ({
  findUser:
    (id, route) => dispatch(findUser(id)),
  updateUser:
    (data, route) => dispatch(updateUser(data)),
  clearForm: () => dispatch(clearForm())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserForm);