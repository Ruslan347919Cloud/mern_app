import {
	CREATE_ITEM,
	FIND_ITEM,
	UPDATE_ITEM,
	CLEAR_FORM
} from '../../../actions/Types';

const initialState = {
	currentCategoryId: '',
	currentCategoryTitle: '',
	newCategoryData: {}
};

export const categoryFormReducer = function(state = initialState, action) {
	const {type, payload} = action;

	switch (type) {
		case FIND_ITEM:
			return {
				...state,
				currentCategoryId: payload._id,
				currentCategoryTitle: payload.title
			};
		case CREATE_ITEM:
			return {
				...state,
				newCategoryData: payload
			};
		case UPDATE_ITEM:
			return {
				...state,
				newCategoryData: payload
			};
		case CLEAR_FORM:
			return {
				...state,
				currentCategoryId: payload.defaultValue,
				currentCategoryTitle: payload.defaultValue,
				newCategoryData: payload.defaultItemData
			};
		default: return state;
	}
};