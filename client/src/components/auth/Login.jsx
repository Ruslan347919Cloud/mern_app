import React, {Fragment, useState} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {Link, Redirect} from 'react-router-dom';
import {login} from '../../actions/AuthActions';

const Login = ({login, isAuthenticated}) => {
	const [formData, setFormData] = useState({
		email: '',
		password: ''
	});

	const {email, password} = formData;

	const onChange = event => {
		const {name, value} = event.target;
		setFormData({...formData, [name]: value});
	};

	const onSubmit = async event => {
		event.preventDefault();
		login(email, password);
	};

	if (isAuthenticated)
		return (<Redirect to="/" />);

	return (
		<Fragment>
			<form
				className="form-horizontal col-md-7 col-lg-8"
				onSubmit={event => onSubmit(event)}>
			  <fieldset>
			    <div id="legend">
			      <legend className="">Login</legend>
			    </div>
		
			    <div className="control-group">
			      <label className="control-label" htmlFor="email">
			      	E-mail
		      	</label>
			      <div className="controls">
			        <input
			        	type="text"
			        	id="email"
			        	name="email"
			        	className="input-xlarge"
			        	value={email}
			        	onChange={event => onChange(event)}
			        	required
		        	/>
			        <p className="help-block">
			        	Enter your e-mail
		        	</p>
			      </div>
			    </div>
			 
			    <div className="control-group">
			      <label className="control-label" htmlFor="password">
			      	Password
		      	</label>
			      <div className="controls">
			        <input
			        	type="password"
			        	id="password"
			        	name="password"
			        	className="input-xlarge"
			        	value={password}
			        	onChange={event => onChange(event)}
			        	required
		        	/>
			        <p className="help-block">
			        	Enter your password
		        	</p>
			      </div>
			    </div>
			 
			    <div className="control-group">
			      <div className="controls">
			        <button className="btn btn-success">
			        	Login
			        </button>
			      </div>
			    </div>

			    <div className="control-group my-2">
			    	<span className="mr-1">
			    		Do not have an account?
			    	</span>
			    	<Link to={'/register'}>
			    		Sign Up
			    	</Link>
			    </div>
			  </fieldset>
			</form>
		</Fragment>
	);
};

Login.propTypes = {
	login: PropTypes.func.isRequired,
	isAuthenticated: PropTypes.bool
};

const mapStateToProps = store => ({
	isAuthenticated: store.auth.isAuthenticated
});

export default connect(mapStateToProps, {login})(Login);