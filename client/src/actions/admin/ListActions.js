import axios from 'axios';
import {setAlert} from '../AlertActions';
import {
	FETCH_ITEMS,
	DELETE_ITEM,
	CHANGE_ITEM_STATUS
} from '../Types';

export const fetchItems = (page, size, route) => async dispatch => {
	try {
		const res = await axios.get(
			`/api/${route}/page/${page}/size/${size}`);
		
		dispatch({
			type: FETCH_ITEMS,
			payload: {
				items: res.data.[route],
				pages: res.data.pages
			}
		});
	} catch (err) {
		const {data: error, status, statusText} = err.response;
		
		console.log(statusText, status);
		console.log(error);
		
		if (error)
			dispatch(setAlert(error, 'danger'));
	}
};

export const changeItemStatus = (id, route) => async dispatch => {
	const config = {
		headers: {
			'Content-Type': 'application/json'
		}
	};

	const body = JSON.stringify({visible: true});

	try {
		const res = await axios.put(`/api/${route}/${id}`, body, config);
		
		dispatch({
			type: CHANGE_ITEM_STATUS,
			payload: res.data
		});

	} catch (err) {
		const {data: error, status, statusText} = err.response;
		
		console.log(statusText, status);
		console.log(error);
		
		if (error)
			dispatch(setAlert(error, 'danger'));
	}
};

export const deleteItem = (id, route) => async dispatch => {
	try {
		const res = await axios.delete(`/api/${route}/${id}`);
		
		dispatch({
			type: DELETE_ITEM,
			payload: res.data._id
		});
	} catch (err) {
		const {data: error, status, statusText} = err.response;
		
		console.log(statusText, status);
		console.log(error);
		
		if (error)
			dispatch(setAlert(error, 'danger'));
	}
};