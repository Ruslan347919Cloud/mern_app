import React from 'react';
import PropTypes from 'prop-types';
import {Redirect} from 'react-router-dom';
import {connect} from 'react-redux';
import CommentFormContent from './CommentFormContent';
import {
  fetchSingleComment,
  updateComment,
  clearCommentForm
} from '../../../../actions/CommentActions';

class CommentForm extends React.Component {

  componentDidMount() {
    const {id} = this.props.match.params;
    const {fetchSingleComment} = this.props;

    if (typeof id !== 'undefined')
      fetchSingleComment(id);
  };

  componentWillUnmount() {
    this.props.clearCommentForm();
  };

  submitHandler = formData => {
    const {id} = this.props.match.params;
    const {updateComment} = this.props;

    updateComment(id, formData);
  };

  formIsSubmitted() {
    const {newComment} = this.props;

    return ((Object.keys(newComment).length !== 0)
      && (typeof newComment._id !== 'undefined'));
  };

  render() {
    const {newComment, initialValues} = this.props;
    
    if (this.formIsSubmitted())
      return <Redirect to={`/admin/comment/view/${newComment._id}`}/>;
    else return (
      <div className="col-md-7 col-lg-8">
        <CommentFormContent
          onSubmit={this.submitHandler}
          initialValues={initialValues}
        />
      </div>
    );
  };
};

CommentForm.propTypes = {
  currentCommentId: PropTypes.string.isRequired,
  newComment: PropTypes.object.isRequired,
  initialValues: PropTypes.object.isRequired,
  fetchSingleComment: PropTypes.func.isRequired,
  updateComment: PropTypes.func.isRequired,
  clearCommentForm: PropTypes.func.isRequired
};

const mapStateToProps = store => {
  const {
    currentCommentId,
    currentCommentText,
    currentCommentArticleId,
    newComment
  } = store.commentForm;
  
  return {
    currentCommentId,
    newComment,
    currentCommentArticleId,
    initialValues: {
      text: currentCommentText
    }
  }
};

const mapDispatchToProps = dispatch => ({
  fetchSingleComment: id => dispatch(fetchSingleComment(id)),
  updateComment: (id, data) =>
    dispatch(updateComment(id, data)),
  clearCommentForm: () => dispatch(clearCommentForm())
});

export default connect(
  mapStateToProps, mapDispatchToProps
)(CommentForm);