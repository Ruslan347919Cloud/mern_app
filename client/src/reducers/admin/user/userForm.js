import {
	FIND_USER,
	UPDATE_USER,
	CLEAR_FORM
} from '../../../actions/Types';

const initialState = {
	currentUserName: '',
	currentUserEmail: '',
	newUserData: {}
};

export const userFormReducer = function(state = initialState, action) {
	const {type, payload} = action;

	switch (type) {
		case FIND_USER:
			return {
				...state,
				currentUserName: payload.name,
				currentUserEmail: payload.email
			};
		case UPDATE_USER:
			return {
				...state,
				newUserData: payload
			};
		case CLEAR_FORM:
			return {
				...state,
				currentUserName: payload.defaultValue,
				currentUserEmail: payload.defaultValue,
				newUserData: payload.defaultItemData
			}
		default: return state;
	}
};