import {
	FETCH_LATEST_ARTICLES,
	FETCH_LATEST_AUTHORS,
	FETCH_CATEGORIES,
	FETCH_TAGS
} from '../actions/Types';

const initialState = {
	latestArticles: [],
	categories: [],
	authors: [],
	tags: []
};

export const sidebarReducer = function(state = initialState, action) {
	const {type, payload} = action;

	switch (type) {
		case FETCH_LATEST_ARTICLES:
			return {
				...state,
				latestArticles: payload
			};
		case FETCH_LATEST_AUTHORS:
			return {
				...state,
				authors: payload
			};
		case FETCH_CATEGORIES:
			return {
				...state,
				categories: payload
			};
		case FETCH_TAGS:
			return {
				...state,
				tags: payload
			};
		default: return state;
	}
};