const mongoose = require('mongoose');
const Joi = require('@hapi/joi');

const validateTag = tag => {
	const schema = Joi.object({
		title: Joi.string().min(5).max(50).required()
	});
	return schema.validate(tag);
}

const tagSchema = new mongoose.Schema({
	title: {
		type: String,
		required: true,
		minlength: 5,
		maxlength: 50,
		trim: true
	},
	articles: [{
		type: mongoose.Schema.Types.ObjectId,
    ref: 'Article'
	}]
});

tagSchema.methods.coincides = function(tags) {
	return tags.some(tag => tag === String(this._id));
};

tagSchema.methods.removeArticle = async function(articleId) {
	const removeIndex = this.articles
		.findIndex(item => String(item) === String(articleId));
	this.articles.splice(removeIndex, 1);
	await this.save();
};

tagSchema.methods.hasArticle = function(articleId) {
	return this.articles.some(item =>
		String(item) === String(articleId));
};

tagSchema.methods.addArticle = async function(articleId) {
	if (!this.hasArticle(articleId)) {
		this.articles.push(articleId);
		await this.save();
	}
};

tagSchema.statics.validateIds = function(tags) {
	return tags.every(tagId =>
		mongoose.Types.ObjectId.isValid(tagId));
};

const Tag = mongoose.model('Tag', tagSchema);

exports.Tag = Tag;
exports.tagSchema = tagSchema;
exports.validateTag = validateTag;