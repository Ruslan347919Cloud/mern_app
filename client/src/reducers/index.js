import {combineReducers} from 'redux';
import {alertReducer} from './alert';
import {authReducer} from './auth';
import {articlesReducer} from './articles';
import {sidebarReducer} from './sidebar';
import {singleArticleReducer} from './singleArticle';
import {sortReducer} from './sort';
import {searchReducer} from './search';
import {fileUploadReducer} from './fileUpload';
import {listReducer} from './admin/list';
import {userViewReducer} from './admin/user/userView';
import {userFormReducer} from './admin/user/userForm';
import {setAttributesReducer} from './admin/article/setAttributes';
import {articleFormReducer} from './admin/article/articleForm';
import {articleViewReducer} from './admin/article/articleView';
import {commentFormReducer} from './admin/comment/commentForm';
import {commentViewReducer} from './admin/comment/commentView';
import {categoryViewReducer} from './admin/category/categoryView';
import {categoryFormReducer} from './admin/category/categoryForm';
import {tagViewReducer} from './admin/tag/tagView';
import {tagFormReducer} from './admin/tag/tagForm';
import {reducer as formReducer} from 'redux-form';

export const rootReducer = combineReducers({
	alert: alertReducer,
	auth: authReducer,
	articles: articlesReducer,
	sidebar: sidebarReducer,
	singleArticle: singleArticleReducer,
	fileUpload: fileUploadReducer,
	list: listReducer,
	sort: sortReducer,
	search: searchReducer,
	userView: userViewReducer,
	userForm: userFormReducer,
	setAttributes: setAttributesReducer,
	articleForm: articleFormReducer,
	articleView: articleViewReducer,
	commentForm: commentFormReducer,
	commentView: commentViewReducer,
	categoryView: categoryViewReducer,
	categoryForm: categoryFormReducer,
	tagView: tagViewReducer,
	tagForm: tagFormReducer,
	form: formReducer
});