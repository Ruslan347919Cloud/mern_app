import React from 'react';
import PropTypes from 'prop-types';
import {Link, Redirect} from 'react-router-dom';
import Pagination from '../Pagination/';
const _ = require('lodash');

export const withList = Component => {

	class ListComponent extends React.Component {

		constructor(props) {
			super(props);
			this.pageSize = 3;
		};

		componentDidMount() {
			const {fetchItems} = this.props;
			const {route} = this.getSpecificValues();
			const page = this.getCurrentPage();

			fetchItems(page, this.pageSize, route);
		};

		componentDidUpdate(prevProps) {
			const {itemName} = this.props.match.params;
			const {route} = this.getSpecificValues();
			const {
				itemName: itemNamePrev
			} = prevProps.match.params;
			const {
				deletedItemId,
				changedItem,
				fetchItems
			} = this.props;
			const {
				deletedItemId: deletedItemIdPrev,
				changedItem: changedItemPrev
			} = prevProps;
			const page = this.getCurrentPage();
			const pagePrev = (typeof prevProps.match.params.page !== 'undefined')
				? (Number(prevProps.match.params.page)) : 1;

			if (page !== pagePrev ||
				deletedItemId !== deletedItemIdPrev ||
					changedItem !== changedItemPrev ||
						itemName !== itemNamePrev)
						fetchItems(page, this.pageSize, route);
		};

		getComponentsList() {
			const {itemName} = this.props.match.params;
			const {itemsSmall, route} = this.getSpecificValues();
			const {
				deleteItem,
				changeItemStatus
			} = this.props;

			return itemsSmall.map(item => (
				<Component
					itemName={itemName}
					route={route}
					item={item}
					key={item._id}
					deleteItem={deleteItem}
					changeItemStatus={changeItemStatus}
				/>
			));
		};

		getHeader() {
			const {itemsSmall} = this.getSpecificValues();
			const itemKeys = (itemsSmall.length > 0)
				? Object.keys(itemsSmall[0]) : null;
			const headerFields = (itemKeys !== null)
				? (itemKeys.map(item => (
					<th scope="col" key={itemKeys.indexOf(item)}>
						{item}
					</th>
				))) : null ;

			return (
				<thead className="thead-dark">
					<tr>
						{headerFields}
						<th scope="col">actions</th>
					</tr>
				</thead>
			);
		};

		getCurrentPage() {
			const {page} = this.props.match.params;

			return (typeof page !== 'undefined')
				? Number(page) : 1;
		};

		firstLetterToUppercase(word) {
			return word.charAt(0).toUpperCase() + word.slice(1);
		};

		getSpecificValues() {
			const {itemName} = this.props.match.params;
			const {items} = this.props;
			let route, itemsSmall;

			switch(itemName) {
				case 'article':
					route = 'articles';
					itemsSmall = items.map(item =>
						_.pick(item, ['_id', 'title', 'visible']));
				break;
				case 'category':
					route = 'categories';
					itemsSmall = items.map(item =>
						_.pick(item, ['_id', 'title']));
				break;
				case 'comment':
					route = 'comments';
					itemsSmall = items.map(item =>
						_.pick(item,['_id', 'text', 'visible']));
				break;
				case 'tag':
					route = 'tags';
					itemsSmall = items.map(item =>
						_.pick(item,['_id', 'title']));
				break;
				case 'user':
					route = 'users';
					itemsSmall = items.map(item =>
						_.pick(item, ['_id', 'name', 'email']));
				break;
				default: return null;
			}

			return {
				route,
				itemsSmall
			};
		};

		render() {
			const {itemName, page} = this.props.match.params;
			const {pages} = this.props;

			if (pages.length > 0 && page > pages.length)
				return <Redirect to={`/admin/${itemName}/index/${pages.length}`}/>;
			else return (
				<div className="col-md-12">
					<h2>{this.firstLetterToUppercase(itemName)}</h2>
					{(itemName !== 'comment') && <Link
						className="btn btn-success btn-lg mb-2"
						to={`/admin/${itemName}/form`}
					>
						Create {itemName}
					</Link>}
					<table className="table table-bordered mb-4 shadow rounded">
						{this.getHeader()}
					  {this.getComponentsList()}
					</table>
					{(pages.length > 1) && (<Pagination
				  	pages={pages}
						currentPage={this.getCurrentPage()}
						route={`/admin/${itemName}/index/`}
				  />)}
				</div>
			);
		};
	};

	ListComponent.propTypes = {
		match: PropTypes.object,
		items: PropTypes.array.isRequired,
		pages: PropTypes.array.isRequired,
		deletedItemId: PropTypes.string.isRequired,
		changedItem: PropTypes.object.isRequired,
		fetchItems: PropTypes.func.isRequired,
		deleteItem: PropTypes.func.isRequired,
		changeItemStatus: PropTypes.func
	};

	return ListComponent;
};